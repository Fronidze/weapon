<?php

namespace backend\request;

use common\attributes\GenerateValueInterface;
use common\attributes\LabelInterface;
use common\attributes\ValidateAttributeInterface;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class BaseModel extends Model
{

    public function rules(): array
    {
        return ArrayHelper::merge($this->buildRules(), $this->customRules());
    }

    public function customRules(): array
    {
        return [];
    }

    public function buildRules(): array
    {
        $rules = [];
        $reflection = new \ReflectionClass($this);
        $properties = $reflection->getProperties();
        foreach ($properties as  $property) {
            $attributes = $property->getAttributes(ValidateAttributeInterface::class, flags: \ReflectionAttribute::IS_INSTANCEOF);
            foreach ($attributes as $attribute) {
                /** @var ValidateAttributeInterface $instance */
                $instance = $attribute->newInstance();
                $validatorName = $instance->getValidatorClass();

                $attributeRules = [
                    $property->getName(),
                    $validatorName
                ];

                if ($instance instanceof GenerateValueInterface) {
                    $attributeRules['value'] = $instance->getValue();
                }

                $rules[] = $attributeRules;
            }
        }

        return $rules;
    }

    public function attributeLabels(): array
    {
        $labels = [];
        $reflection = new \ReflectionClass($this);
        $properties = $reflection->getProperties();
        foreach ($properties as  $property) {
            $attributes = $property->getAttributes(LabelInterface::class, flags: \ReflectionAttribute::IS_INSTANCEOF);
            foreach ($attributes as $attribute) {
                /** @var LabelInterface $instance */
                $instance = $attribute->newInstance();
                $labels[$property->getName()] = $instance->getAttributeLabel();
            }
        }

        return $labels;
    }

    public function loadPostRequest(): bool
    {
        return $this->load(\Yii::$app->request->post()) && $this->validate();
    }

    public function loadQueryRequest(): bool
    {
        return $this->load(\Yii::$app->request->get()) && $this->validate();
    }
}