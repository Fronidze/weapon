<?php

namespace backend\request;

use common\modules\directory\validation\StorageValidator;
use common\modules\directory\validation\TimestampValidator;
use common\weapon\entity\enum\Gender;
use common\weapon\entity\enum\ShooterExperience;
use Ramsey\Uuid\Uuid;
use yii\base\Model;
use yii\web\UploadedFile;

class ClientRequest extends Model
{
    public ?string $id = null;
    public string $lastName = '';
    public string $firstName = '';
    public ?string $middleName = null;
    public string $photo_id = '';
    public string $gender = '';
    public string $birthday_at = '';
    public string $email = '';
    public string $phone = '';
    public string $notice = '';
    public int $isLefty = 0;
    public string $passport_id = '';
    public string $shooterExperience = '';
    public string $declaration_id = '';


    public function rules(): array
    {
        return [
            [['id'], 'default', 'value' => Uuid::uuid4()->toString()],
            [['gender'], 'default', 'value' => Gender::Male->value],
            [['shooterExperience'], 'default', 'value' => ShooterExperience::Junior->value],
            [['birthday_at'], TimestampValidator::class, 'format' => 'Y-m-d'],
            [['photo_id', 'passport_id', 'declaration_id'], StorageValidator::class],
            [['isLefty'], 'boolean'],
            [
                [
                    'lastName', 'firstName', 'gender', 'birthday_at', 'email', 'phone',
                    'shooterExperience'
                ],
                'required'
            ],
            [
                [
                    'lastName', 'firstName', 'middleName', 'photo_id', 'gender', 'birthday_at',
                    'email', 'phone', 'notice', 'passport_id', 'shooterExperience', 'declaration_id'
                ],
                'string'
            ],
        ];
    }
    public function attributeLabels()
    {
        return [
            'id' => 'Индетификатор',
            'lastName' => 'Фамилия',
            'firstName' => 'Имя',
            'middleName' => 'Отчество',
            'photo_id' => 'Фотография',
            'gender' => 'Пол',
            'birthday_at' => 'Дата рождения',
            'email' => 'Электронный ящик',
            'phone' => 'Номер телефона',
            'notice' => 'Дополнительные сведения',
            'isLefty' => 'Левша?',
            'passport_id' => 'Скан паспорта',
            'shooterExperience' => 'Стрелковый опыт',
            'declaration_id' => 'Скан заявления',
        ];
    }
    public function isPostLoad(): bool
    {
        return $this->load(\Yii::$app->request->post()) && $this->validate();
    }
    public function listGender(): array
    {
        return [
            Gender::Male->value => Gender::Male->getLabel(),
            Gender::Female->value => Gender::Female->getLabel(),
        ];
    }
    public function listShooterExperience(): array
    {
        return [
            ShooterExperience::Junior->value => ShooterExperience::Junior->getLabel(),
            ShooterExperience::Middle->value => ShooterExperience::Middle->getLabel(),
            ShooterExperience::Senior->value => ShooterExperience::Senior->getLabel(),
        ];
    }

}