<?php

namespace backend\request;

use common\attributes\ModelLabelAttribute;
use common\attributes\RequiredValidateAttribute;
use common\attributes\StringValidateAttribute;
use common\attributes\UuidValidateAttribute;
use common\models\Employee;
use common\models\Settings;
use common\weapon\repository\EmployeeRepository;

class SettingsRequest extends BaseModel
{
    #[UuidValidateAttribute, StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Индетификатор')]
    public string|null $id = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Название компании')]
    public string|null $title = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Адресс')]
    public string|null $address = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Электронный адрес')]
    public string|null $email = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Контактный номер телефона')]
    public string|null $phone = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Материально ответсвенное лицо')]
    public string|null $material_responsible_id = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Главный бухгалтер')]
    public string|null $accountant_id = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Руководитель клуба')]
    public string|null $director_id = null;

    public function employeeList(): array
    {
        $employees = (new EmployeeRepository())->list();

        $list = [];
        /** @var Employee $employee */
        foreach ($employees as $employee) {
            $list[$employee->id] = implode(' ', [
                $employee->last_name,
                $employee->first_name,
                $employee->middle_name
            ]);
        }

        return $list;
    }

    public static function default(): static
    {
        $settings = new static();
        $record = Settings::find()->one();
        if ($record instanceof Settings === false) {
            return $settings;
        }

        foreach ($settings->getAttributes() as $attribute => $value) {
            $settings->{$attribute} = $record->getAttribute($attribute);
        }
        return $settings;
    }
}