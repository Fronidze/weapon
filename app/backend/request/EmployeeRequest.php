<?php

namespace backend\request;

use common\attributes\DefaultValidateAttribute;
use common\attributes\ModelLabelAttribute;
use common\attributes\RequiredValidateAttribute;
use common\attributes\StringValidateAttribute;
use common\attributes\UuidValidateAttribute;
use common\weapon\entity\enum\EmployeeStatus;

class EmployeeRequest extends BaseModel
{
    #[UuidValidateAttribute, StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Индетификатор')]
    public string|null $id = null;

    #[StringValidateAttribute]
    #[ModelLabelAttribute('Индетификатор учетной записи')]
    public string|null $account_id = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Фамилия')]
    public string|null $last_name = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Имя')]
    public string|null $first_name = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Отчество')]
    public string|null $middle_name = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Электроный ящик')]
    public string|null $email = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Номер телефона')]
    public string|null $phone = null;

    #[StringValidateAttribute]
    #[ModelLabelAttribute('Индетификатор должности')]
    public string|null $position_id = null;

    #[StringValidateAttribute]
    #[ModelLabelAttribute('Дата трудоустройства')]
    public string|null $employment_at = null;

    #[StringValidateAttribute]
    #[ModelLabelAttribute('Статус сотрудника')]
    public string|null $status = null;

}