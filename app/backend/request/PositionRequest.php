<?php

namespace backend\request;

use common\models\Positions;
use common\weapon\entity\enum\Permission;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class PositionRequest extends Model
{
    public string $id = '';
    public string $title = '';
    public array $roles = [];

    public function init()
    {
        parent::init();
    }

    public function rules(): array
    {
        return [
            [['id'], 'default', 'value' => \Ramsey\Uuid\Uuid::uuid4()->toString()],
            [['id', 'title'], 'string'],
            [['title'], 'required'],
            [['roles'], function ($attribute) {
                $selectRoles = array_filter($this->{$attribute}, function ($value) {
                    return (boolean)$value;
                });

                if (empty($selectRoles) === true) {
                    $this->addError($attribute, 'Выберите одну из ролей');
                }

            }, 'skipOnEmpty' => false],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'Индетификатор',
            'title' => 'Название должности',
            'roles' => 'Разрешения'
        ];
    }

    public static function find(string $id): static
    {
        $position = Positions::find()
            ->with(['positionRoles'])
            ->where(['=', 'id', $id])
            ->one();

        if ($position instanceof Positions === false) {
            throw new NotFoundHttpException('Должность не найдена');
        }

        $roles = [];
        $selectPermissions = ArrayHelper::getColumn($position->positionRoles, 'items_id');
        $permissions = Permission::getList();

        foreach ($permissions as $permission) {
            $roles[$permission] = (integer)in_array($permission, $selectPermissions);
        }

        return new static([
            'id' => $position->id,
            'title' => $position->title,
            'roles' => $roles,
        ]);
    }

}