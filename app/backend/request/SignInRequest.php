<?php

namespace backend\request;

use common\models\Accounts;
use common\weapon\repository\AccountRepository;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;
use yii\web\NotFoundHttpException;

class SignInRequest extends Model
{
    public string $email = '';
    public string $password = '';
    public int $isRemember = 0;

    private Accounts|null $account = null;

    public function rules(): array
    {
        return [
            [['email', 'password'], 'required'],
            [['isRemember'], 'boolean'],
            [['email', 'password'], 'string'],
            [['email'], 'email'],
            [['email'], function (string $attribute) {
                $isExists = (new Query())
                    ->from(Accounts::tableName())
                    ->where(['=', new Expression('lower(email)'), strtolower($this->{$attribute})])
                    ->exists();

                if ($isExists === false) {
                    $this->addError($attribute, 'Пользователь не найден');
                }
            }],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'email' => 'Электронная почта',
            'password' => 'Пароль',
            'isRemember' => 'Запонить меня?',
        ];
    }

    protected function getAccount(): Accounts|null
    {
        if ($this->account instanceof Accounts) {
            return $this->account;
        }

        $repository = new AccountRepository();
        return $repository->findByEmail($this->email);
    }

    public function login(): bool
    {
        if ($this->validate() === false) {
            return false;
        }

        $account = $this->getAccount();
        if ($account === null) {
            throw new NotFoundHttpException("Пользователь не найден");
        }
        return \Yii::$app->user->login($account, $this->isRemember ? 3600 * 24 * 30 : 0);
    }
}