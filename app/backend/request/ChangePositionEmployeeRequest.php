<?php

namespace backend\request;

use common\attributes\ModelLabelAttribute;
use common\attributes\RequiredValidateAttribute;
use common\attributes\StringValidateAttribute;
use common\models\Positions;
use JetBrains\PhpStorm\ArrayShape;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class ChangePositionEmployeeRequest extends BaseModel
{
    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Сотрудник')]
    public string|null $employee_id = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Должность')]
    public string|null $position_id = null;

    #[ArrayShape(['integer' => 'string'])]
    public function getPositions(): array
    {
        $positions = (new Query())
            ->select(['id', 'title'])
            ->from(Positions::tableName())
            ->orderBy([])
            ->all();

        return ArrayHelper::map($positions, 'id', 'title');
    }

}