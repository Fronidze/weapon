<?php

namespace backend\request;

use common\models\Accounts;
use common\modules\directory\service\OrganizationService;
use common\weapon\repository\AccountRepository;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;

class SignUpRequest extends Model
{
    public string $email = '';
    public string $inn = '';
    public string $password = '';
    public string $passwordConfirmation = '';
    private ?OrganizationService $organizationService;

    public function init()
    {
        $this->organizationService = new OrganizationService();
        parent::init();
    }

    public function rules(): array
    {
        return [
            [['email', 'inn', 'password', 'passwordConfirmation'], 'required'],
            [['email', 'inn', 'password', 'passwordConfirmation'], 'string'],
            [['email'], 'email'],
            [['email'], 'filter', 'filter' => function (string $email) {
                return strtolower($email);
            }],
            [['inn'], 'innValidate', 'skipOnEmpty' => false],
            [['email'], function (string $attribute) {
                $isExist = (new Query())
                    ->from(Accounts::tableName())
                    ->where(['=', new Expression('lower(email)'), strtolower($this->{$attribute})])
                    ->exists();

                if ($isExist === true) {
                    $this->addError($attribute, 'Пользователь с таким Email уже существует');
                }
            }],
            [['password'], function (string $attribute) {
                if ($this->{$attribute} !== $this->passwordConfirmation) {
                    $this->addError($attribute, 'Пароли не совпадают');
                }
            }],
            [['passwordConfirmation'], function (string $attribute) {
                if ($this->{$attribute} !== $this->password) {
                    $this->addError($attribute, 'Пароли не совпадают');
                }
            }],
        ];
    }

    public function innValidate(string $attribute, ?array $params = null)
    {   
        try {
            $organization = $this->organizationService->findByInn($this->{$attribute});
        } catch (\Throwable $exception) {
            $this->addError($attribute, 'Не валидный ИНН');
            return;
        }

        if ($organization->isActive() === false) {
            $this->addError($attribute, 'Организация с данным ИНН не действующая');
            return;
        }
    }

    public function attributeLabels(): array
    {
        return [
            'email' => 'Электронная почта',
            'inn' => 'ИНН организации',
            'password' => 'Пароль',
            'passwordConfirmation' => 'Повторите пароль',
        ];
    }
}