<?php

namespace backend\assets;

use yii\web\AssetBundle;

class CustomAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/custom.css',
    ];

    public $js = [
        'js/request/modal.js',
        'js/custom.js',
    ];

    public $depends = [
        MainAsset::class,
        Select2Assets::class,
        AirDatepickerAssets::class,
        iCheckAsset::class,
        ToastAssets::class
    ];
}