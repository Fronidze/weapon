<?php

namespace backend\assets;

use yii\web\AssetBundle;

class AirDatepickerAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/plugins/air-datepicker/air-datepicker.css',
    ];
    public $js = [
        'js/plugins/air-datepicker/air-datepicker.js',
    ];
    public $depends = [
        MainAsset::class
    ];

}