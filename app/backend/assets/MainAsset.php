<?php

namespace backend\assets;

use yii\web\AssetBundle;

class MainAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'font-awesome/css/font-awesome.css',
        'css/plugins/datapicker/datepicker3.css',
        'css/animate.css',
        'css/style.css',
    ];
    public $js = [
        'js/jquery-3.1.1.min.js',
        'js/popper.min.js',
        'js/bootstrap.js',
        'js/plugins/metisMenu/jquery.metisMenu.js',
        'js/plugins/slimscroll/jquery.slimscroll.min.js',
        'js/inspinia.js',
        'js/plugins/pace/pace.min.js',
        'js/plugins/jquery-ui/jquery-ui.min.js',
        'js/plugins/bs-custom-file/bs-custom-file-input.min.js',
        'js/plugins/datapicker/bootstrap-datepicker.js',
        'js/plugins/fullcalendar/moment.min.js',
        'js/main.js',
    ];
    public $depends = [
    ];
}
