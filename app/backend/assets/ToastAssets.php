<?php


namespace backend\assets;

use yii\web\AssetBundle;

class ToastAssets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "css/plugins/toastr/toastr.min.css"
    ];

    public $js = [
        "js/plugins/toastr/toastr.min.js"
    ];

    public $depends = [
        MainAsset::class
    ];

}