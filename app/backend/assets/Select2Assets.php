<?php

namespace backend\assets;

use yii\web\AssetBundle;

class Select2Assets extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "css/plugins/select2/select2.min.css",
        "css/plugins/select2/select2-bootstrap4.min.css"
    ];
    public $js = [
        'js/plugins/select2/select2.full.min.js',
    ];
    public $depends = [
        MainAsset::class
    ];

}