<?php


namespace backend\assets;

use yii\web\AssetBundle;

class iCheckAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/plugins/iCheck/custom.css',
    ];

    public $js = [
        'js/plugins/iCheck/icheck.min.js',
    ];

    public $depends = [
        MainAsset::class
    ];
}