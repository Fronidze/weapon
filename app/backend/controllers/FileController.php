<?php

namespace backend\controllers;

use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use common\weapon\service\storage\FileService;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class FileController extends BaseAccessController
{

    private FileService $fileService;

    public function init()
    {
        $this->fileService = new FileService();
        parent::init();
    }

    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [
                Role::Admin->getRoleName(),
                Permission::Document->getRoleName()
            ]]
        ];
    }

    public function actionDownload(string $id): Response
    {
        $filePath = $this->fileService->getDownloadPath($id);
        if (file_exists($filePath) === false) {
            throw new NotFoundHttpException('Путь до файла не найден');
        }

        return \Yii::$app->response->sendFile($filePath);
    }
}