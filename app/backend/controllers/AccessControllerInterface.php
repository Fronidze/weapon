<?php

namespace backend\controllers;

interface AccessControllerInterface
{
    public function rules(): array;
    public function except(): array;
}