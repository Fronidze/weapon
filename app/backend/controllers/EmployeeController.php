<?php

namespace backend\controllers;

use backend\request\ChangePositionEmployeeRequest;
use backend\request\EmployeeRequest;
use backend\request\PositionRequest;
use common\models\Accounts;
use common\models\Employee;
use common\models\Positions;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use common\weapon\service\company\PositionService;
use common\weapon\service\profile\EmployeeService;
use common\widgets\Alert;
use yii\db\Query;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class EmployeeController extends BaseAccessController
{

    private EmployeeService $employeeService;

    public function init()
    {
        $this->employeeService = new EmployeeService();
        parent::init();
    }

    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [Permission::Directory->getRoleName()]]
        ];
    }

    public function actionIndex(): string
    {
        $employees = Employee::find()
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->render('index', compact('employees'));
    }

    public function actionCreate()
    {
        $request = new EmployeeRequest();
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $employee = $this->employeeService->create($request);
            } catch (\Exception $exception) {
                Alert::error($exception->getMessage());
                $transaction->rollBack();
            } catch (\Throwable $throwable) {
                Alert::throwable($throwable);
                $transaction->rollBack();
            }

            $fullName = "{$employee->last_name} {$employee->first_name} {$employee->middle_name}";
            Alert::success("Сотрудник <b>{$fullName}</b> успешно создан");
            $transaction->commit();
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('request'));
    }

    public function actionChangePosition(string $id)
    {
        $employee = Employee::find()
            ->where(['=', 'id', $id])
            ->one();

        if ($employee instanceof Employee === false) {
            throw new NotFoundHttpException('Сотрудник не найден');
        }

        $request = new ChangePositionEmployeeRequest([
            'employee_id' => $employee->id,
            'position_id' => $employee->position_id
        ]);

        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $this->employeeService->hire($request->employee_id, $request->position_id);
            return $this->redirect(['index']);
        }

        return $this->render('inner/change-position', compact('request'));
    }

    public function actionCreateForAccount(string $id)
    {
        $account = Accounts::find()
            ->where(['=', 'id', $id])
            ->one();

        if ($account instanceof Accounts === false) {
            throw new NotFoundHttpException("Учетная запись не найдена");
        }

        $request = new EmployeeRequest([
            'account_id' => $account->id,
            'email' => $account->email,
        ]);
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $this->employeeService->create($request);
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('request'));
    }
}