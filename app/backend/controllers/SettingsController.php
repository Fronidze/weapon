<?php

namespace backend\controllers;

use backend\request\SettingsRequest;
use common\weapon\entity\enum\Permission;
use common\weapon\repository\SettingsRepository;
use common\weapon\service\company\SettingService;
use common\widgets\Alert;
use yii\web\Response;

class SettingsController extends BaseAccessController
{

    private SettingService $settingsService;

    public function init()
    {
        $this->settingsService = new SettingService();
        parent::init();
    }

    public function rules(): array
    {
        return [
            [
                'allow' => true,
                'roles' => [Permission::Directory->getRoleName()]
            ]
        ];

    }

    public function actionIndex(): Response|string
    {
        $request = SettingsRequest::default();
        if ($request->loadPostRequest() === true) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $this->settingsService->create($request);
                Alert::success("Настройки успешно изменены");
                $transaction->commit();
            } catch (\Exception $exception) {
                Alert::error($exception->getMessage());
                $transaction->rollBack();
            } catch (\Throwable $throwable) {
                Alert::throwable($throwable);
                $transaction->rollBack();
            }
            return $this->redirect(['index']);
        }
        return $this->render('index', compact('request'));
    }


}