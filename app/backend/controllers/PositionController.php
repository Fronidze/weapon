<?php

namespace backend\controllers;

use backend\request\ChangePositionEmployeeRequest;
use backend\request\EmployeeRequest;
use backend\request\PositionRequest;
use common\models\Accounts;
use common\models\Employee;
use common\models\Positions;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use common\weapon\exception\HaveRelationRecordsException;
use common\weapon\repository\PositionRepository;
use common\weapon\service\company\PositionService;
use common\weapon\service\profile\EmployeeService;
use common\widgets\Alert;
use yii\db\Query;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class PositionController extends BaseAccessController
{

    private PositionRepository $positionRepository;
    private PositionService $positionService;

    public function init()
    {
        $this->positionRepository = new PositionRepository();
        $this->positionService = new PositionService();
        parent::init();
    }

    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [Permission::Directory->getRoleName()]]
        ];
    }

    public function actionIndex(): string
    {
        $positions = $this->positionRepository->list();
        return $this->render('index', compact('positions'));
    }

    public function actionCreatePosition(): Response|string
    {
        $request = new PositionRequest();
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $position = $this->positionService->create($request);
            } catch (\Exception $exception) {
                Alert::error($exception->getMessage());
                $transaction->rollBack();
            } catch (\Throwable $exception){
                Alert::throwable($exception);
                $transaction->rollBack();
            }

            Alert::success("Должность <b>{$position->title}</b> успешно создана");
            $transaction->commit();
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('request'));
    }

    public function actionUpdatePosition(string $id): Response|string
    {
        $request = PositionRequest::find($id);
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {
                $position = $this->positionService->update($request);
            } catch (\Exception $exception) {
                Alert::error($exception->getMessage());
                $transaction->rollBack();
            } catch (\Throwable $throwable) {
                Alert::throwable($throwable);
                $transaction->rollBack();
            }

            Alert::success("Должность <b>{$position->title}</b> успешно обновлена");
            $transaction->commit();
            return $this->redirect(['index']);
        }

        return $this->render('update', compact('request'));
    }

    public function actionRemovePosition(string $id): Response
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $this->positionRepository->remove($id);
        } catch (HaveRelationRecordsException $exception) {
            Alert::warning($exception->getMessage());
            $transaction->rollBack();
        } catch (\Throwable $exception) {
            Alert::throwable($exception);
            $transaction->rollBack();
        }

        Alert::success("Должность успешно удалена");
        $transaction->commit();
        return $this->redirect(['index']);
    }
}