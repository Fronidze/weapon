<?php

namespace backend\controllers;

use backend\request\SignInRequest;
use backend\request\SignUpRequest;
use common\modules\directory\service\OrganizationService;
use common\weapon\entity\enum\Role;
use common\weapon\service\profile\AccountService;
use JetBrains\PhpStorm\ArrayShape;
use Yii;
use yii\filters\AccessControl;
use yii\web\ErrorAction;
use yii\web\Response;

class SiteController extends BaseAccessController
{

    private AccountService $accountService;
    private OrganizationService $organizationService;

    public function init()
    {
        $this->accountService = new AccountService();
        $this->organizationService = new OrganizationService();
        parent::init();
    }

    public function beforeAction($action)
    {
        if ($action->id === 'info') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [
                Role::Client->getRoleName(),
                Role::Employee->getRoleName(),
                Role::Admin->getRoleName(),
            ]]
        ];
    }

    public function except(): array
    {
        return ['signin', 'signup', 'error', 'signout', 'info'];
    }

    #[ArrayShape(['error' => "string[]"])]
    public function actions(): array
    {
        return [
            'error' => ['class' => ErrorAction::class],
        ];
    }

    public function actionIndex(): Response
    {
        return $this->redirect(match (true) {
            \Yii::$app->user->can(Role::Client->getRoleName()) => ['client/request'],
            \Yii::$app->user->can(Role::Employee->getRoleName()) => ['client/index'],
            \Yii::$app->user->can(Role::Admin->getRoleName()) => ['settings/index'],
        });
    }

    #[ArrayShape(['token' => "int|null|string"])]
    public function actionInfo(): array
    {
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return [
            'token' => \Yii::$app->user->getId()
        ];
    }

    public function actionSignin(): Response|string
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $request = new SignInRequest();
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $request->login();
            \Yii::$app->session->setFlash('success', 'Пользователь успешно вошел');
            return $this->goBack();
        }

        $request->password = '';
        $this->layout = 'blank';
        return $this->render('login', compact('request'));
    }

    public function actionSignup(): Response|string
    {
        $request = new SignUpRequest();
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $transaction = \Yii::$app->db->beginTransaction();
            try {

                $account = $this->accountService->create($request->email, $request->password,  Role::Admin->getRoleName());

                $organizationDto = $this->organizationService->findByInn($request->inn);
                $organizationDto->setAccountId($account->id);
                $this->organizationService->create($organizationDto->buildRequest());

                \Yii::$app->user->login($account);
                \Yii::$app->session->setFlash('success', 'Пользователь успешно зарегистрировался');
                $transaction->commit();
                return $this->goHome();
            } catch (\Throwable $exception) {
                $transaction->rollBack();
            }
        }

        $request->password = '';
        $request->passwordConfirmation = '';
        $this->layout = 'blank';
        return $this->render('signup', compact('request'));
    }

    public function actionSignout()
    {
        Yii::$app->user->logout();
        return $this->goHome();
    }
}
