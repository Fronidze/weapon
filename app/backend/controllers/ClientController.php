<?php

namespace backend\controllers;

use backend\request\ClientRequest;
use common\models\Clients;
use common\modules\directory\models\GallerySchedule;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use common\weapon\repository\GalleryRepository;
use common\weapon\repository\GalleryScheduleRepository;
use common\weapon\service\profile\ClientService;
use JetBrains\PhpStorm\ArrayShape;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class ClientController extends BaseAccessController
{
    private ClientService $clientService;
    private GalleryRepository $galleryRepository;
    private GalleryScheduleRepository $scheduleRepository;

    public function init()
    {
        $this->clientService = new ClientService();
        $this->galleryRepository = new GalleryRepository();
        $this->scheduleRepository = new GalleryScheduleRepository();
        
        parent::init();
    }

    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [Permission::Reception->getRoleName()]]
        ];
    }

    public function actionIndex(): string
    {
        $clients = Clients::find()
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->render('index', compact('clients'));
    }

    public function actionCreate(): Response|string
    {
        $request = new ClientRequest();
        if ($request->isPostLoad() === true) {
            $this->clientService->create($request);
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('request'));
    }

    public function actionEvents(): string
    {
        $galleries = $this->galleryRepository->list();
        $currentGallery = current($galleries);
        $selectGallery = \Yii::$app->request->getQueryParam('gallery', $currentGallery->id);

        return $this->render('events', compact('galleries', 'selectGallery'));
    }

    public function actionEventDetail(string $schedule_id)
    {
        $scheduleGallery = GallerySchedule::find()
            ->with(['gallery'])
            ->where(['=', 'id', $schedule_id])
            ->one();

        return $this->renderAjax('detail/schedule', compact('scheduleGallery'));
    }
}