<?php

namespace backend\controllers;

use yii\filters\AccessControl;
use yii\web\Controller;

abstract class BaseAccessController extends Controller implements AccessControllerInterface
{
    public function getAccessBehaviorKey(): string
    {
        return 'AccessControl';
    }


    public function behaviors(): array
    {
        return [
            $this->getAccessBehaviorKey() => [
                'class' => AccessControl::class,
                'except' => $this->except(),
                'rules' => $this->rules(),
            ],
        ];
    }

    public function except(): array
    {
        return [];
    }
}