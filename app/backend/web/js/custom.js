
let webSocketConnection = null;
window.onunload = event => {
    if (webSocketConnection !== null) {
        webSocketConnection.close();
    }
};


$(function () {
    bsCustomFileInput.init();

    let select2Selector = $('[data-select]');
    if (select2Selector.length > 0) {
        select2Selector.select2({
            theme: 'bootstrap4',
            dropdownPosition: 'below'
        });
    }

    new AirDatepicker('[data-datepicker]', {
        dateFormat: "yyyy-MM-dd"
    });

    $('.i-checks').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });

    fetch('/me')
        .then(response => response.json())
        .then(content => {
            webSocketConnection = new WeaponSocket(content.token);
            webSocketConnection.init();
        });

    $('[data-modal-trigger]').on('click', event => {
        let target = event.target.closest('[data-modal-trigger]');
        let request = target.dataset['modalRequest'];

        let modal = new CustomModal();
        modal.fetch(request);

    });

});

class WeaponSocket {
    userId = null;
    interval = null;
    constructor(token) {
        let type = document.location.protocol === 'https:' ? 'wss:' : 'ws:';
        this.socket = new WebSocket(type +'//'+document.location.host+'/ws?token='+token);
    }
    init() {
        this.socket.onopen = (event) => {
            let pingMessage = new WeaponSocketMessage('ping', 'system').json();
            this.interval = setInterval(() => {
                this.socket.send(pingMessage)
            }, 30000);
        }

        this.socket.onmessage = (event) => {
            let response = JSON.parse(event.data);

            switch (response.type) {
                case "message":
                    new Information(response.body).show();
                    break;
                case "command":
                    eval(response.body);
                    break;
                case "debug":
                    console.log(response.body);
            }


        }

        this.socket.onclose = (event) => {
            clearInterval(this.interval);
        }
    }
    close() {
        this.socket.close();
        clearInterval(this.interval);
    }
}

class WeaponSocketMessage {
    constructor(
        message,
        type = 'message',
        sender = null,
        recipient = null,
    ) {
        this.message = message
        this.type = type
        this.sender = sender;
        this.recipient = recipient;

    }
    json() {
        return JSON.stringify({
            type: this.type,
            sender: this.sender,
            recipient: this.recipient,
            body: this.message
        });
    }
}

class Information {
    constructor(message, type = 'success') {
        this.message = message;
        this.type = type;
        this.init();
    }
    init() {
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }
    }
    show() {
        switch (this.type) {
            case "success":
                toastr.success(this.message, 'Новое сообщение');
                break;
            case "error":
                toastr.error(this.message, 'Новое сообщение');
                break;
            default:
                toastr.info(this.message, 'Новое сообщение');
        }
    }
}