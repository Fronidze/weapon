class CustomModal {
    constructor() {
        this.modal = document.getElementById('customModal');
    }

    fetch(request) {
        fetch(request)
            .then(response => {
                let contentType = response.headers.get('Content-Type').split(';')[0];
                console.log(contentType);
                if (contentType === 'application/json') {
                    return response.json();
                }
                return response.text()
            })
            .then(content => {
                this.parseResponse(content);
            });
    }

    parseResponse(response) {
        if (response.type !== undefined) {
            this.setHeaderContent(response.content);
            $(this.modal).modal('show');
        }
        console.log(response);
    }

    setHeaderContent(header) {
        this.modal.querySelector('[data-modal-header]').innerHTML = header;
    }
}