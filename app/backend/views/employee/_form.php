<?php

use backend\request\EmployeeRequest;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * @var $this View
 * @var $request EmployeeRequest
 */

?>

<?= Html::beginForm(); ?>
<?= Html::activeHiddenInput($request, 'id'); ?>

<div class="row">
    <div class="col-lg-4 form-group">
        <?php
        echo Html::activeLabel($request, 'last_name');
        echo Html::activeTextInput($request, 'last_name', ['class' => ['form-control']]);
        echo Html::error($request, 'last_name', ['class' => ['text-danger']]);
        ?>
    </div>
    <div class="col-lg-4 form-group">
        <?php
        echo Html::activeLabel($request, 'first_name');
        echo Html::activeTextInput($request, 'first_name', ['class' => ['form-control']]);
        echo Html::error($request, 'first_name', ['class' => ['text-danger']]);
        ?>
    </div>
    <div class="col-lg-4 form-group">
        <?php
        echo Html::activeLabel($request, 'middle_name');
        echo Html::activeTextInput($request, 'middle_name', ['class' => ['form-control']]);
        echo Html::error($request, 'middle_name', ['class' => ['text-danger']]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 form-group">
        <?php
        echo Html::activeLabel($request, 'email');
        echo Html::activeTextInput($request, 'email', ['class' => ['form-control']]);
        echo Html::error($request, 'email', ['class' => ['text-danger']]);
        ?>
    </div>
    <div class="col-lg-4 form-group">
        <?php
        echo Html::activeLabel($request, 'phone');
        echo Html::activeTextInput($request, 'phone', ['class' => ['form-control']]);
        echo Html::error($request, 'phone', ['class' => ['text-danger']]);
        ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-12 text-left" style="margin-bottom: 0;">
        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']]) ?>
        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>

<?= Html::endForm(); ?>
