<?php

use common\models\Employee;
use common\weapon\entity\enum\EmployeeStatus;
use yii\helpers\Url;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * @var $this View
 * @var $employees  Employee[]
 */

$this->title = 'Персонал';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection($this->title);
?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Список сотрудников</h2>
                <p>All clients need to be verified before you can send email and set a project.</p>
                <div class="input-group">
                    <?= Html::a('Добавить сотрудника', ['create'], ['class' => ['btn btn-primary']]) ?>
                </div>
                <div class="clients-list">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">

                            <?php foreach ($employees as $employee): ?>
                                <tr>
                                    <td style="width: 100px;" class="text-center">
                                        <?= EmployeeStatus::from($employee->status)->renderLabel(); ?>
                                    </td>
                                    <td>
                                        <?= $employee->last_name . ' ' . $employee->first_name . ' ' . $employee->middle_name ?>
                                        <br>
                                        <small class="text-muted">
                                            <?= $employee?->position?->title ?? '(без должности)'; ?>
                                        </small>
                                    </td>
                                    <td class="contact-type text-navy">
                                        <i class="fa fa-at"></i>
                                        <i class="fa fa-phone"></i>
                                    </td>
                                    <td>
                                        <small><?= $employee->email ?></small><br>
                                        <small><?= $employee->phone ?></small>
                                    </td>
                                    <td class="contact-type text-navy"><i class="fa fa-calendar"></i></td>
                                    <td>
                                        <small><?= \Yii::$app->formatter->asDate($employee->employment_at) ?></small>
                                    </td>
                                    <td class="text-center">
                                        <small>Создан:</small>
                                        <small class="text-muted">
                                            <?= \Yii::$app->formatter->asDate($employee->created_at) ?>
                                        </small>
                                        <br>
                                        <small>Обновлен:</small>
                                        <small class="text-muted">
                                            <?= \Yii::$app->formatter->asDate($employee->created_at) ?>
                                        </small>
                                    </td>
                                    <td class="text-right">
                                        <div class="btn-group">
                                            <button data-toggle="dropdown"
                                                    class="btn btn-default btn-xs dropdown-toggle">
                                                Действия
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a class="dropdown-item"
                                                       href="<?= Url::to(['change-position', 'id' => $employee->id]) ?>">
                                                        Перевод на должность
                                                    </a>
                                                </li>
                                                <li><a class="dropdown-item" href="#">Отправить в отпуск</a>
                                                </li>
                                                <li class="dropdown-divider"></li>
                                                <li><a class="dropdown-item text-danger" href="#">Уволить</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

