<?php

use backend\request\ChangePositionEmployeeRequest;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * @var $this View
 * @var $request ChangePositionEmployeeRequest
 */

$this->title = 'Персонал';

\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Список сотрудников', ['index'])
    ->setSection('Смена должноси');

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Смена должности сотрудника</h5>
            </div>
            <div class="ibox-content">

                <?= Html::beginForm(); ?>
                <?= Html::activeHiddenInput($request, 'employee_id'); ?>

                <div class="row">
                    <div class="col-lg-4 form-group">

                        <?php
                        echo Html::activeLabel($request, 'position_id');
                        echo Html::activeDropDownList($request, 'position_id', $request->getPositions(), [
                            'class' => 'form-control',
                            'prompt' => '-- выберите должность --',
                            'data' => ['select' => true],
                        ]);
                        echo Html::error($request, 'position_id', ['class' => ['text-danger']]);
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-lg-12 text-left" style="margin-bottom: 0;">
                        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']]) ?>
                        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
                    </div>
                </div>

                <?= Html::endForm(); ?>

            </div>
        </div>
    </div>
</div>

