<?php

use backend\request\EmployeeRequest;
use yii\web\View;

/**
 * @var $this View
 * @var $request EmployeeRequest
 */

$this->title = 'Персонал';

\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Список сотрудников', ['index'])
    ->setSection('Новый сотрудник');

?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Создание нового сотрудника</h5>
            </div>
            <div class="ibox-content">
                <?= $this->render('_form', compact('request')) ?>
            </div>
        </div>
    </div>
</div>



