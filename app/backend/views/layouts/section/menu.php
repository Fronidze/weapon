<?php

use common\weapon\entity\enum\Permission;
use common\widgets\Menu;

echo Menu::widget([
    'items' => [
        [
            'label' => 'Ресепшн',
            'url' => ['/client/shooting/index'],
            'icon' => 'fa fa-bell',
            'visible' => \Yii::$app->user->can(Permission::Reception->getRoleName()),
            'items' => [
                [
                    'label' => 'Заявки',
                    'url' => ['/client/events'],
                ],
                [
                    'label' => 'Клиенты',
                    'url' => ['/client/index'],
                ],
            ]
        ],
        [
            'label' => 'Персонал',
            'url' => ['/employee/index'],
            'icon' => 'fa fa-user',
            'visible' => \Yii::$app->user->can(Permission::Directory->getRoleName()),
            'items' => [
                [
                    'label' => 'Сотрудники',
                    'url' => ['/employee/index'],
                ],
                [
                    'label' => 'Должности',
                    'url' => ['/position/index'],
                ],
            ]
        ],
        [
            'label' => 'Склад',
            'url' => ['/directory/storage/index'],
            'icon' => 'fa fa-inbox',
            'visible' => \Yii::$app->user->can(Permission::Storage->getRoleName()),
            'items' => [
                [
                    'label' => 'Вооружение',
                    'url' => ['/directory/weapon/index'],
                ],
                [
                    'label' => 'Амуниция',
                    'url' => ['/directory/ammunition/index'],
                ],
            ]
        ],
        [
            'label' => 'Справочники',
            'url' => ['/directory/main'],
            'icon' => 'fa fa-inbox',
            'visible' => \Yii::$app->user->can(Permission::Directory->getRoleName()),
            'items' => [
                [
                    'label' => 'Виды вооружения',
                    'url' => ['/directory/weapon-view/index'],
                ],
                [
                    'label' => 'Типы вооружения',
                    'url' => ['/directory/weapon-type/index'],
                ],
                [
                    'label' => 'Учетный записи',
                    'url' => ['/directory/account/index'],
                ],
                [
                    'label' => 'Склады',
                    'url' => ['/directory/storage/index'],
                ],
                [
                    'label' => 'Галлереи',
                    'url' => ['/directory/gallery/index'],
                ],
            ],
        ],
        [
            'label' => 'Записаться',
            'url' => ['/client/write'],
            'icon' => 'fa fa-pencil',
            //'visible' => \Yii::$app->user->can(Permission::Client->getRoleName())
            'visible' => false,
        ],
        [
            'label' => 'История посещений',
            'url' => ['/client/history'],
            'icon' => 'fa fa-history',
            //'visible' => \Yii::$app->user->can(Permission::Client->getRoleName())
            'visible' => false,
        ],
        [
            'label' => 'Документы',
            'url' => ['/document/main/index'],
            'icon' => 'fa fa-file-text',
            'visible' => \Yii::$app->user->can(Permission::Document->getRoleName()),
            'items' => [
                [
                    'label' => 'Приказы на стрельбы',
                    'url' => ['/document/shooter/index']
                ]
            ],
        ],
        [
            'label' => 'Организация',
            'url' => ['/directory/organization/index'],
            'icon' => 'fa fa-building-o',
            'visible' => \Yii::$app->user->can(Permission::Directory->getRoleName())
        ],
        [
            'label' => 'Настройки',
            'url' => ['/settings/index'],
            'icon' => 'fa fa-gear',
            'visible' => \Yii::$app->user->can(Permission::Directory->getRoleName())
        ],
        [
            'label' => 'Выход',
            'url' => ['/site/signout'],
            'icon' => 'fa fa-sign-out',
        ],
    ]
]);

?>