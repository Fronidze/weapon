<?php

use yii\web\View;

/**
 * @var $this View
 * @var $content string
 */

?>

<div class="modal inmodal" id="customModal" tabindex="-1" role="dialog" aria-hidden="true" data-modal-wrapper>
    <div class="modal-dialog">
        <div class="modal-content animated fadeIn" data-modal-conteiner>

            <div class="modal-header">

                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>

                <div class="header_wrapper" data-modal-header>
                    <h4 class="modal-title">Modal title</h4>
                    <small class="font-bold">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</small>
                </div>
            </div>
            <div class="modal-body" data-modal-body></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>

        </div>
    </div>
</div>
