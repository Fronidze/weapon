<?php
/**
 * @var $this \yii\web\View
 * @var $content string
 */

use backend\assets\CustomAssets;
use common\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

CustomAssets::register($this);
?>

<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body>
    <?php $this->beginBody(); ?>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <?= $this->render('section/menu'); ?>
            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2><?= $this->title; ?></h2>
                    <?= Breadcrumbs::widget([
                        'tag' => 'ol',
                        'itemTemplate' => "<li class='breadcrumb-item'>{link}</li>\n",
                        'activeItemTemplate' => "<li class='breadcrumb-item active'><strong>{link}</strong></li>\n",
                        'links' => $this->params['breadcrumbs'] ?? []
                    ]); ?>
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">
                <?php
                echo Alert::widget();
                echo $content;
                ?>
            </div>

            <div class="footer">
                <?= $this->render('section/footer') ?>
            </div>
        </div>
    </div>

    <?= $this->render('section/modal')?>

    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage();
