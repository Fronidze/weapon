<?php

/**
 * @var $this \yii\web\View
 * @var $positions \common\models\Positions[]
 */

use common\models\PositionRoles;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use yii\helpers\Html,
    yii\helpers\ArrayHelper;

$this->title = 'Доступные должности';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection($this->title);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Список доступных должностей</h2>
                <p>All clients need to be verified before you can send email and set a project.</p>
                <div class="input-group">
                    <?= Html::a('Новая должность', ['create-position'], ['class' => ['btn btn-primary']]) ?>
                </div>
                <div class="clients-list">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <tr>
                                <th class="col-lg-5">Название должности</th>
                                <th class="col-lg-4">Разрешения для должности</th>
                                <th class="col-lg-3 text-center"></th>
                            </tr>
                            <?php foreach ($positions as $position): ?>
                                <tr>
                                    <td>
                                        <?= $position->title ?> <br>
                                        <small class="text-muted"><?= $position->id ?></small>
                                    </td>
                                    <td style="width: 500px;">
                                        <?php
                                        $roles = ArrayHelper::getValue($position, 'positionRoles', []);
                                        /** @var PositionRoles $role */
                                        foreach ($roles as $role) {
                                            $label = Permission::from($role->items_id)->getLabelRoleName();
                                            echo Html::tag('span', $label, [
                                                'class' => ['label', 'label-primary'],
                                                'style' => ['margin-right' => '5px']
                                            ]);
                                        }
                                        ?>
                                    </td>
                                    <td class="text-right">
                                        <div class="btn-group">

                                            <?php
                                            $icon = Html::tag('i', null, ['class' => ['fa', 'fa-pencil']]);
                                            echo Html::a($icon, ['update-position', 'id' => $position->id], ['class' => ['btn', 'btn-sm', 'btn-white']]);
                                            ?>

                                            <?php
                                            $icon = Html::tag('i', null, ['class' => ['fa', 'fa-trash']]);
                                            echo Html::a($icon, ['remove-position', 'id' => $position->id], ['class' => ['btn', 'btn-sm', 'btn-danger']]);
                                            ?>

                                        </div>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

