<?php

/**
 * @var $this \yii\web\View
 * @var $request \backend\request\PositionRequest
 */

use common\weapon\helper\Breadcrumbs;

$this->title = "Обновление должности";
Breadcrumbs::instance($this)
    ->setSection('Должности', ['index'])
    ->setSection($this->title);

?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Обновление должности</h5>
            </div>
            <div class="ibox-content">
                <?= $this->render('_form', compact('request')) ?>
            </div>
        </div>
    </div>
</div>

