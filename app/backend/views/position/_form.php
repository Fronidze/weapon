<?php

use common\weapon\entity\enum\Permission;
use yii\web\View,
    yii\helpers\Html;

/**
 * @var $this View
 * @var $request \backend\request\PositionRequest
 */

?>

<?= Html::beginForm(); ?>
<?= Html::activeHiddenInput($request, 'id'); ?>

<div class="row">
    <div class="form-group col-lg-12">
        <?php
        echo Html::activeLabel($request, 'title');
        echo Html::activeTextInput($request, 'title', ['class' => ['form-control']]);
        echo Html::error($request, 'title', ['class' => ['text-danger']]);
        ?>
    </div>
</div>

<div class="row">
    <?php foreach (Permission::getList() as $permissionName): ?>
        <div class="form-group col-lg-12">
            <div class="i-checks">
                <label>
                    <?= Html::activeCheckbox($request, "roles[{$permissionName}]", ['label' => false]); ?>
                    <i></i><?= Permission::from($permissionName)->getLabelRoleName() ?>
                </label>
            </div>
        </div>
    <?php endforeach; ?>

    <div class="col-lg-12">
        <?= Html::error($request, 'roles', ['class' => 'text-danger']) ?>
    </div>

</div>


<div class="row">
    <div class="form-group col-lg-12 text-left">
        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']]) ?>
        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>

<?= Html::endForm(); ?>
