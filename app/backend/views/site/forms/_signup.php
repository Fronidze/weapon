<?php

/**
 * @var $this \yii\web\View
 * @var $request \backend\request\SignUpRequest
 */


use yii\helpers\Html;

echo Html::beginForm(); ?>

<div class="form-group text-left">
    <?= Html::activeTextInput($request, 'email',
        ['class' => 'form-control', 'placeholder' => $request->getAttributeLabel('email')]
    );?>
    <?= Html::error($request, 'email', ['class' => 'text-danger'])?>
</div>

<div class="form-group text-left">
    <?= Html::activeTextInput($request, 'inn',
        ['class' => 'form-control', 'placeholder' => $request->getAttributeLabel('inn')]
    );?>
    <?= Html::error($request, 'inn', ['class' => 'text-danger'])?>
</div>

<div class="form-group text-left">
    <?= Html::activePasswordInput($request, 'password',
        ['class' => 'form-control', 'placeholder' => $request->getAttributeLabel('password')]
    );?>
    <?= Html::error($request, 'password', ['class' => 'text-danger'])?>
</div>

<div class="form-group text-left">
    <?= Html::activePasswordInput($request, 'passwordConfirmation',
        ['class' => 'form-control', 'placeholder' => $request->getAttributeLabel('passwordConfirmation')]
    );?>
    <?= Html::error($request, 'passwordConfirmation', ['class' => 'text-danger'])?>
</div>

<?= Html::submitButton('Регистрация', ['class' => 'btn btn-primary block full-width m-b'])?>
<hr>
<?= Html::a('Войти', ['site/signin'], ['class' => 'btn btn-sm btn-white btn-block'])?>
<?= Html::endForm(); ?>
