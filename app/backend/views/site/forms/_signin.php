<?php

/**
 * @var $this \yii\web\View
 * @var $request \backend\request\SignInRequest
 */


use yii\helpers\Html;

echo Html::beginForm(); ?>

<div class="form-group text-left">
    <?= Html::activeTextInput($request, 'email',
        ['class' => 'form-control', 'placeholder' => $request->getAttributeLabel('email')]
    ); ?>
    <?= Html::error($request, 'email', ['class' => 'text-danger']) ?>
</div>
<div class="form-group text-left">
    <?= Html::activePasswordInput($request, 'password',
        ['class' => 'form-control', 'placeholder' => $request->getAttributeLabel('password')]
    ); ?>
    <?= Html::error($request, 'password', ['class' => 'text-danger']) ?>
</div>
<div class="form-group text-left">
    <?= Html::activeCheckbox($request, 'isRemember'); ?>
</div>
<?= Html::submitButton('Войти', ['class' => 'btn btn-primary block full-width m-b']) ?>
<hr>
<?= Html::a('Регистрация', ['site/signup'], ['class' => 'btn btn-sm btn-white btn-block']) ?>
<?= Html::endForm(); ?>
