<?php

/**
 * @var $this \yii\web\View
 * @var $request \backend\request\SignInRequest
 */

$this->title = 'Авторизация';
?>

<div>
    <div><h1 class="logo-name">IN+</h1></div>
    <h3>Вход в систему</h3>
    <?= $this->render('forms/_signin', compact('request'));?>
    <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
</div>

