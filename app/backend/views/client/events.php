<?php

use common\modules\directory\entities\enums\ScheduleStatus;
use common\modules\directory\models\Galleries;
use common\modules\directory\models\GallerySchedule;
use common\weapon\helper\Breadcrumbs;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var $this View
 * @var $galleries Galleries[]
 * @var $schedules GallerySchedule[]
 * @var $selectGallery string
 */

$this->title = 'Добро пожаловать в систему учета и контроля';

Breadcrumbs::instance($this)
    ->setSection($this->title);


$listGallery = ArrayHelper::map($galleries, 'id', 'title');
?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Список событий</h2>
                <p class="m-0">тут текст описание что тут будем говорить про события что у нас на сегодня или на
                    завтра</p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content clients-list">
                <h2>Рассписание для галлереи</h2>
                <p>Данное рассписание ознакомительное. И Желательно его согласовывать с сотрудниками после проведения
                    стрельб</p>
                <div class="tabs-container">
                    <ul class="nav nav-tabs">
                        <?php foreach ($galleries as $index => $gallery): ?>
                            <li>
                                <a class="nav-link <?php if ($gallery->id === $selectGallery): ?>active<?php endif; ?>"
                                   data-toggle="tab" href="#g<?= $index ?>"><?= $gallery->title ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>

                    <div class="tab-content">
                        <?php foreach ($galleries as $index => $gallery): ?>
                            <div id="g<?= $index ?>"
                                 style="height: auto;"
                                 class="tab-pane <?php if ($gallery->id === $selectGallery): ?>active<?php endif; ?>">
                                <div class="panel-body">
                                    <?php
                                    $schedules = [];
                                    foreach ($gallery->gallerySchedules as $schedule) {
                                        $schedules[$schedule->event_at][] = $schedule;
                                    }
                                    ?>
                                    <?php foreach ($schedules as $date => $schedule): ?>
                                        <p>Рассписание сеансов на <?= $date ?></p>
                                        <table class="table table-hover">
                                            <?php foreach ($schedule as $scheduleDays): ?>
                                                <tr>
                                                    <td style="width: 50px">
                                                        <small>
                                                            <button
                                                                    data-modal-trigger
                                                                    data-modal-request="<?= Url::to(['event-detail', 'schedule_id' => $scheduleDays->id]) ?>"
                                                                    class="btn btn-xs btn-white"
                                                            >
                                                                <i class="fa fa-eye"></i>
                                                            </button>
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <small>
                                                            <?php
                                                            echo \Yii::$app->formatter->asDate($scheduleDays->event_at)
                                                            ?>
                                                        </small>
                                                    </td>

                                                    <td>
                                                        <small>
                                                            <?php
                                                            $begin = new DateTime($scheduleDays->event_at . ' ' . $scheduleDays->begin_at);
                                                            $end = new DateTime($scheduleDays->event_at . ' ' . $scheduleDays->end_at);
                                                            echo $begin->format('H:i') . ' - ' . $end->format('H:i');
                                                            ?>
                                                        </small>
                                                    </td>

                                                    <td class="text-center" style="width: 100px;">
                                                        <?php
                                                        $status = ScheduleStatus::from($scheduleDays->status);
                                                        echo $status->renderLabel();
                                                        ?>
                                                    </td>
                                                </tr>
                                            <?php endforeach; ?>
                                        </table>
                                    <?php endforeach; ?>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
