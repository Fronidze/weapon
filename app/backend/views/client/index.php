<?php

/**
 * @var $this \yii\web\View
 * @var $clients \common\models\Clients[]
 */

use common\weapon\entity\enum\ShooterExperience;
use common\weapon\service\storage\FileService;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Список сотрудников";
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection($this->title);

?>

<div class="row">
    <div class="col-lg-4 form-group">
        <?= Html::a('Добавить нового клиента', ['create'], ['class' => 'btn btn-primary']) ?>
    </div>
</div>

<div class="row">
    <?php foreach ($clients as $client): ?>
        <div class="col-lg-4">
            <div class="contact-box">
                <div class="row">
                    <div class="col-4 text-center">
                        <p>
                            <?php
                            echo Html::img(
                                FileService::getWebPath($client->photo_id),
                                [
                                    'class' => 'rounded-circle img-fluid',
                                    'style' => 'width: 128px; height: 128px; object-fit: cover; box-shadow: 0 2px 4px rgba(0, 0, 0, .2);'
                                ]
                            )
                            ?>
                        </p>
                        <p><?= Html::a('Подробнее', ['detail', 'id' => $client->id], ['class' => 'btn btn-primary btn-sm']) ?></p>
                    </div>
                    <div class="col-8">
                        <h3>
                            <strong>
                                <?= ArrayHelper::getValue($client, 'lastName') ?>
                                <?= ArrayHelper::getValue($client, 'firstName') ?>
                                <?= ArrayHelper::getValue($client, 'middleName') ?>
                            </strong>
                        </h3>
                        <p>
                            <i class="fa fa-flask text-navy"></i>
                            <?php
                            $experience = $client->shooterExperience;
                            echo ShooterExperience::from($experience)->getLabel();
                            ?>
                        </p>
                        <address>
                            <i class="fa fa-phone text-navy"></i>
                            <?= $client->email ?><br>

                            <i class="fa fa-at text-navy"></i>
                            <?= $client->phone ?><br>
                        </address>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>


