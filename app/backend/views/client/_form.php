<?php

/**
 * @var $this View
 * @var $request ClientRequest
 */

use backend\request\ClientRequest;
use yii\helpers\Html;
use yii\web\View;

?>

<?= Html::beginForm(options: ['enctype' => 'multipart/form-data']); ?>
<?= Html::activeHiddenInput($request, 'id'); ?>

<div class="row">
    <div class="form-group col-lg-4">
        <?php
        echo Html::activeLabel($request, 'lastName');
        echo Html::activeTextInput($request, 'lastName', ['class' => 'form-control']);
        echo Html::error($request, 'lastName', ['class' => 'text-danger']);
        ?>
    </div>
    <div class="form-group col-lg-4">
        <?php
        echo Html::activeLabel($request, 'firstName');
        echo Html::activeTextInput($request, 'firstName', ['class' => 'form-control']);
        echo Html::error($request, 'firstName', ['class' => 'text-danger']);
        ?>
    </div>
    <div class="form-group col-lg-4">
        <?php
        echo Html::activeLabel($request, 'middleName');
        echo Html::activeTextInput($request, 'middleName', ['class' => 'form-control']);
        echo Html::error($request, 'middleName', ['class' => 'text-danger']);
        ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-4">
        <?php
        echo Html::activeLabel($request, 'gender');
        echo Html::activeDropDownList($request, 'gender', $request->listGender(), [
            'class' => 'form-control',
            'prompt' => '-- пол клиента --',
            'data' => ['select' => true],
        ]);
        echo Html::error($request, 'gender', ['class' => 'text-danger']);
        ?>
    </div>

    <div class="form-group col-lg-4">
        <?php
        echo Html::activeLabel($request, 'email');
        echo Html::activeTextInput($request, 'email', ['class' => 'form-control']);
        echo Html::error($request, 'email', ['class' => 'text-danger']);
        ?>
    </div>

    <div class="form-group col-lg-4">
        <?php
        echo Html::activeLabel($request, 'phone');
        echo Html::activeTextInput($request, 'phone', ['class' => 'form-control']);
        echo Html::error($request, 'phone', ['class' => 'text-danger']);
        ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-4">
        <?php
        echo Html::activeLabel($request, 'shooterExperience');
        echo Html::activeDropDownList($request, 'shooterExperience', $request->listShooterExperience(), [
            'class' => 'form-control',
            'prompt' => '-- опыт клиента --',
            'data' => ['select' => true],
        ]);
        echo Html::error($request, 'shooterExperience', ['class' => 'text-danger']);
        ?>
    </div>
    <div class="form-group col-lg-4">
        <?php
        echo Html::activeLabel($request, 'isLefty');
        echo Html::activeDropDownList($request, 'isLefty', [1 => 'Да', 0 => 'Нет'], [
            'class' => 'form-control',
            'data' => ['select' => true],
        ]);
        echo Html::error($request, 'isLefty', ['class' => 'text-danger']);
        ?>
    </div>
    <div class="form-group col-lg-4">
        <?php
        echo Html::activeLabel($request, 'birthday_at');
        echo Html::activeTextInput($request, 'birthday_at', [
            'class' => 'form-control',
            'data' => ['datepicker' => true]
        ]);
        echo Html::error($request, 'birthday_at', ['class' => ['text-danger']]);
        ?>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-12 form-group">
                <div class="input-group">
                    <div class="custom-file">
                        <?= Html::activeFileInput($request, 'photo_id', ['class' => 'custom-file-input']) ?>
                        <?= Html::activeLabel($request, 'photo_id', ['class' => 'custom-file-label']) ?>
                    </div>
                </div>
                <?= Html::error($request, 'photo_id', ['class' => ['text-danger']]); ?>
            </div>
            <div class="col-lg-12 form-group">
                <div class="input-group">
                    <div class="custom-file">
                        <?= Html::activeFileInput($request, 'passport_id', ['class' => 'custom-file-input']) ?>
                        <?= Html::activeLabel($request, 'passport_id', ['class' => 'custom-file-label']) ?>
                    </div>
                </div>
                <?= Html::error($request, 'passport_id', ['class' => ['text-danger']]); ?>
            </div>
            <div class="col-lg-12 form-group">
                <div class="input-group">
                    <div class="custom-file">
                        <?= Html::activeFileInput($request, 'declaration_id', ['class' => 'custom-file-input']) ?>
                        <?= Html::activeLabel($request, 'declaration_id', ['class' => 'custom-file-label']) ?>
                    </div>
                </div>
                <?= Html::error($request, 'declaration_id', ['class' => ['text-danger']]); ?>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <?php
        echo Html::activeLabel($request, 'notice');
        echo Html::activeTextarea($request, 'notice', ['class' => 'form-control', 'rows' => 4]);
        echo Html::error($request, 'notice', ['class' => 'text-danger']);
        ?>
    </div>
</div>


<div class="row">
    <div class="form-group col-lg-12">
        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']]) ?>
        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>
<?= Html::endForm(); ?>
