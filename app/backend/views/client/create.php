<?php

/**
 * @var $this View
 * @var $request ClientRequest
 */

use backend\request\ClientRequest;
use yii\web\View;

\backend\assets\Select2Assets::register($this);
\backend\assets\AirDatepickerAssets::register($this);

$this->title = 'Добавление нового клиента';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Клиенты', ['index'])
    ->setSection($this->title);

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Форма создания нового клиента</h5>
                </div>
                <div class="ibox-content">
                    <?= $this->render('_form', ['request' => $request])?>
                </div>
            </div>
        </div>
    </div>
</div>
