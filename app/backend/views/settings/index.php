<?php

/**
 * @var $this View
 * @var $request SettingsRequest
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use backend\request\SettingsRequest;
use yii\web\View;

$this->title = 'Настройки клуба';

\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection($this->title);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Настройки клуба</h5>
            </div>
            <div class="ibox-content">
                <?= Html::beginForm(); ?>
                <?= Html::activeHiddenInput($request, 'id'); ?>

                <div class="form-group row">
                    <?= Html::activeLabel($request, 'title', ['class' => ['col-lg-2', 'col-form-label']]); ?>
                    <div class="col-lg-10">
                        <?php
                        echo Html::activeTextInput($request, 'title', ['class' => 'form-control']);
                        echo Html::error($request, 'title', ['class' => ['text-danger']]);
                        ?>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group row">
                    <?= Html::activeLabel($request, 'address', ['class' => ['col-lg-2', 'col-form-label']]); ?>
                    <div class="col-lg-10">
                        <?php
                        echo Html::activeTextInput($request, 'address', ['class' => 'form-control']);
                        echo Html::error($request, 'address', ['class' => ['text-danger']]);
                        ?>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group row">
                    <?= Html::activeLabel($request, 'email', ['class' => ['col-lg-2', 'col-form-label']]); ?>
                    <div class="col-lg-10">
                        <?php
                        echo Html::activeTextInput($request, 'email', ['class' => 'form-control']);
                        echo Html::error($request, 'email', ['class' => ['text-danger']]);
                        ?>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group row">
                    <?= Html::activeLabel($request, 'phone', ['class' => ['col-lg-2', 'col-form-label']]); ?>
                    <div class="col-lg-10">
                        <?php
                        echo Html::activeTextInput($request, 'phone', ['class' => 'form-control']);
                        echo Html::error($request, 'phone', ['class' => ['text-danger']]);
                        ?>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group row">
                    <?= Html::activeLabel($request, 'material_responsible_id', ['class' => ['col-lg-2', 'col-form-label']]); ?>
                    <div class="col-lg-10">
                        <?php
                        echo Html::activeDropDownList($request, 'material_responsible_id', $request->employeeList(), [
                            'class' => 'form-control',
                            'prompt' => '--выберите тип--',
                            'data' => ['select' => true]
                        ]);
                        echo Html::error($request, 'material_responsible_id', ['class' => ['text-danger']]);
                        ?>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group row">
                    <?= Html::activeLabel($request, 'accountant_id', ['class' => ['col-lg-2', 'col-form-label']]); ?>
                    <div class="col-lg-10">
                        <?php
                        echo Html::activeDropDownList($request, 'accountant_id', $request->employeeList(), [
                            'class' => 'form-control',
                            'prompt' => '--выберите тип--',
                            'data' => ['select' => true]
                        ]);
                        echo Html::error($request, 'accountant_id', ['class' => ['text-danger']]);
                        ?>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="form-group row">
                    <?= Html::activeLabel($request, 'director_id', ['class' => ['col-lg-2', 'col-form-label']]); ?>
                    <div class="col-lg-10">
                        <?php
                        echo Html::activeDropDownList($request, 'director_id', $request->employeeList(), [
                            'class' => 'form-control',
                            'prompt' => '--выберите тип--',
                            'data' => ['select' => true]
                        ]);
                        echo Html::error($request, 'director_id', ['class' => ['text-danger']]);
                        ?>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="row">
                    <div class="form-group col-lg-12 text-left" style="margin-bottom: 0;">
                        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
                    </div>
                </div>

                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>
</div>
