<?php

namespace common\weapon\repository;

use common\models\Employee;
use common\models\PositionRoles;
use common\models\Positions;
use common\weapon\exception\HaveRelationRecordsException;
use common\weapon\exception\RepositoryRuntimeException;
use common\weapon\exception\ValidateException;
use yii\db\Query;
use yii\web\NotFoundHttpException;

class PositionRepository
{
    public function list(): array
    {
        return Positions::find()
            ->with(['positionRoles'])
            ->all();
    }

    public function find(string $position_id): Positions|null
    {
        $position = Positions::find()
            ->with(['positionRoles'])
            ->where(['=', 'id', $position_id])
            ->one();

        if ($position instanceof Positions === false) {
            throw new NotFoundHttpException('Должность не найдена');
        }

        return $position;
    }

    public function remove(string $position_id): bool
    {
        $position = $this->find($position_id);
        if ($position === null) {
            throw new NotFoundHttpException("Должность не найдена");
        }

        $count = (new Query())
            ->from(Employee::tableName())
            ->where(['=', 'position_id', $position_id])
            ->count();

        if ($count !== 0) {
            throw new HaveRelationRecordsException('На должности есть сотрудники');
        }

        PositionRoles::deleteAll(['=', 'positions_id', $position_id]);
        return (boolean)$position->delete();
    }

    public function save(Positions $positions): Positions
    {
        if ($positions->validate() === false) {
            throw new ValidateException($positions);
        }

        $positions->save();
        $positions->refresh();

        return $positions;
    }

    public function setPositionPermission(string $position_id, array $permissions = []): Positions
    {
        $position = $this->find($position_id);
        if ($position === null) {
            throw new NotFoundHttpException("Должность не найдена");
        }

        PositionRoles::deleteAll(['=', 'positions_id', $position_id]);
        foreach ($permissions as $permission) {
            \Yii::$app->db->createCommand()
                ->insert(PositionRoles::tableName(), [
                    'positions_id' => $position_id,
                    'items_id' => $permission
                ])->execute();
        }

        return $position;
    }

}