<?php

namespace common\weapon\repository;

use common\modules\directory\models\GallerySchedule;
use common\weapon\exception\ValidateException;
use yii\web\NotFoundHttpException;

class GalleryScheduleRepository
{

    public function list(string|null $gallery_id = null): array
    {
        $query = GallerySchedule::find()
            ->with('gallery');

        if ($gallery_id !== null) {
            $query->where(['=', 'gallery_id', $gallery_id])
                ->orderBy(['begin_at' => SORT_ASC]);
        } else {
            $query->orderBy(['event_id' => SORT_ASC]);
        }

        return $query->all();
    }

    public function findByIdentifier(string $schedule_id): GallerySchedule|null
    {
        $schedule = GallerySchedule::find()
            ->with('gallery')
            ->where(['=', 'id', $schedule_id])
            ->one();

        if ($schedule instanceof GallerySchedule === false) {
            return null;
        }

        return $schedule;
    }

    public function save(GallerySchedule $schedule): GallerySchedule
    {
        if ($schedule->validate() === false) {
            throw new ValidateException($schedule);
        }

        $schedule->save(false);
        $schedule->refresh();

        return $schedule;
    }

    public function remove(string $schedule_id): bool
    {
        $schedule = $this->findByIdentifier($schedule_id);
        if ($schedule === null) {
            throw new NotFoundHttpException('Дата/Время не найдено в рассписании');
        }

        return (boolean) $schedule->delete();
    }

}