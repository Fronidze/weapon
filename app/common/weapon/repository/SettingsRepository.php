<?php

namespace common\weapon\repository;

use common\models\Settings;
use common\weapon\exception\ValidateException;
use yii\web\NotFoundHttpException;

class SettingsRepository
{
    public function list(): array
    {
        return Settings::find()
            ->with(['accountant', 'materialResponsible'])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
    }

    public function find(string $id): Settings|null
    {
        $settings = Settings::find()
            ->with(['accountant', 'materialResponsible'])
            ->where(['=', 'id', $id])
            ->one();

        if ($settings instanceof Settings === false) {
            return null;
        }

        return $settings;
    }

    public function findByName(string $name): Settings|null
    {
        $settings = Settings::find()
            ->with(['accountant', 'materialResponsible'])
            ->where(['like', 'lower(title)', strtolower($name)])
            ->one();

        if ($settings instanceof Settings === false) {
            return null;
        }

        return $settings;
    }

    public function save(Settings $settings): Settings
    {
        if ($settings->validate() === false) {
            throw new ValidateException($settings);
        }

        $settings->save(false);
        $settings->refresh();

        return $settings;
    }

    public function remove(string $id): bool
    {
        $settings = $this->find($id);
        if ($settings === null) {
            throw new NotFoundHttpException("Настройки не найдены");
        }

        return (boolean)$settings->delete();
    }

}