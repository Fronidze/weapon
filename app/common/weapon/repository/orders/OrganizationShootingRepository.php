<?php

namespace common\weapon\repository\orders;

use common\models\OrganizationShootingOrders;
use common\weapon\exception\ValidateException;
use yii\db\Expression;
use yii\web\NotFoundHttpException;

class OrganizationShootingRepository
{
    public function list(): array
    {
        return OrganizationShootingOrders::find()
            ->with(['directorEmployee', 'headEmployee'])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
    }

    public function findByIdentifier(string $id): OrganizationShootingOrders|null
    {
        $order = OrganizationShootingOrders::find()
            ->with(['directorEmployee', 'headEmployee'])
            ->where(['=', 'id', $id])
            ->one();

        if ($order instanceof OrganizationShootingOrders === false) {
            return null;
        }

        return $order;
    }

    public function findByCreatedDate(\DateTime $datetime): OrganizationShootingOrders|null
    {
        $order = OrganizationShootingOrders::find()
            ->with(['directorEmployee', 'headEmployee'])
            ->where(['=', new Expression('date(created_at)'), $datetime->format('Y-m-d')])
            ->one();

        if ($order instanceof OrganizationShootingOrders === false) {
            return null;
        }

        return $order;
    }

    public function save(OrganizationShootingOrders $order): OrganizationShootingOrders
    {
        if ($order->validate() === false) {
            throw new ValidateException($order);
        }

        $order->save(false);
        $order->refresh();

        return $order;
    }

    public function remove(string $id): bool
    {
        $order = $this->findByIdentifier($id);
        if ($order === null) {
            throw new NotFoundHttpException('Приказ на организацию стрельб не найден');
        }

        return (boolean) $order->delete();
    }
}