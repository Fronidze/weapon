<?php

namespace common\weapon\repository;

use common\models\Organization;
use common\weapon\exception\ValidateException;

class OrganizationRepository
{

    public function save(Organization $organization): Organization
    {
        if ($organization->validate() === false) {
            throw new ValidateException($organization);
        }

        $organization->save(false);
        $organization->refresh();

        return $organization;
    }

}