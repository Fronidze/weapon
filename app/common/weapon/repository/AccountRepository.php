<?php

namespace common\weapon\repository;

use common\models\Accounts;
use common\weapon\exception\ValidateException;
use yii\db\Expression;
use yii\web\NotFoundHttpException;

class AccountRepository
{
    private EmployeeRepository $employeeRepository;

    public function __construct()
    {
        $this->employeeRepository = new EmployeeRepository();
    }

    public function find(string $id): Accounts|null
    {
        /** @var Accounts $record */
        $record = Accounts::find()
            ->with(['employee'])
            ->where(['=', 'id', $id])
            ->one();

        if ($record === null) {
            throw new NotFoundHttpException('Пользователь не найден');
        }

        return $record;
    }
    public function findByEmail(string $email): Accounts|null
    {
        /** @var Accounts $account */
        $account = Accounts::find()
            ->with(['employee'])
            ->where(['=', new Expression('lower(email)'), strtolower($email)])
            ->one();

        if ($account === null) {
            return null;
        }

        return $account;
    }
    public function save(Accounts $accounts): Accounts
    {
        if ($accounts->validate() === false) {
            throw new ValidateException($accounts);
        }

        $accounts->save(false);
        $accounts->refresh();

        return $accounts;
    }
    public function remove(string $account_id): bool
    {
        $account = $this->find($account_id);
        if ($account->employee !== null) {
            $this->employeeRepository->remove($account->employee->id);
        }

        \Yii::$app->authManager->revokeAll($account->id);
        return (bool) $account->delete();
    }
}