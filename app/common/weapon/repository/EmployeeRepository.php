<?php

namespace common\weapon\repository;

use common\models\Employee;
use common\weapon\exception\ValidateException;
use yii\web\NotFoundHttpException;

class EmployeeRepository
{

    public function list(): array
    {
        return Employee::find()
            ->with(['account', 'position'])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
    }

    public function find(string $employee_id): Employee|null
    {
        $employee = Employee::find()
            ->with(['account', 'position'])
            ->where(['=', 'id', $employee_id])
            ->one();

        if ($employee instanceof Employee === false) {
            return null;
        }

        return $employee;
    }

    public function findByAccount(string $account_id): Employee|null
    {
        $employee = Employee::find()
            ->with(['account', 'position'])
            ->where(['=', 'account_id', $account_id])
            ->one();

        if ($employee instanceof Employee === false) {
            return null;
        }

        return $employee;
    }

    public function save(Employee $employee): Employee
    {
        if ($employee->validate() === false) {
            throw new ValidateException($employee);
        }

        $employee->save(false);
        $employee->refresh();

        return $employee;
    }

    public function remove(string $employee_id): bool
    {
        $employee = Employee::find()
            ->where(['=', 'id', $employee_id])
            ->one();

        if ($employee instanceof Employee === false) {
            throw new NotFoundHttpException("Сотрудник не найден");
        }
        
        return (boolean)$employee->delete();
    }
}