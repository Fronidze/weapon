<?php

namespace common\weapon\repository;

use common\modules\directory\models\Galleries;
use common\weapon\exception\ValidateException;
use yii\db\ActiveQuery;
use yii\web\NotFoundHttpException;

class GalleryRepository
{

    public function list(): array
    {
        return Galleries::find()
            ->joinWith([
                'gallerySchedules' => function (ActiveQuery $relation) {
                    $relation->orderBy([
                        'event_at' => SORT_ASC,
                        'begin_at' => SORT_ASC
                    ]);
                }
            ])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();
    }

    public function findByIdentifier(string $id): Galleries|null
    {
        $gallery = Galleries::find()
            ->with(['gallerySchedules'])
            ->where(['=', 'id', $id])
            ->one();

        if ($gallery instanceof Galleries === false) {
            return null;
        }

        return $gallery;
    }

    public function save(Galleries $gallery): Galleries
    {
        if ($gallery->validate() === false) {
            throw new ValidateException($gallery);
        }

        $gallery->save(false);
        $gallery->refresh();

        return $gallery;
    }

    public function remove(string $id): bool
    {
        $gallery = $this->findByIdentifier($id);
        if ($gallery === null) {
            throw new NotFoundHttpException('Галлерея не найдена');
        }

        return (boolean)$gallery->delete();
    }

}