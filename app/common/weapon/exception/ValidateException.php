<?php

namespace common\weapon\exception;

use Throwable;
use yii\db\BaseActiveRecord;

class ValidateException extends \DomainException
{
    private array $errors = [];

    public function __construct(BaseActiveRecord $record, $message = null, $code = 0, Throwable $previous = null)
    {
        $this->errors = $record->getErrorSummary(true);
        $message = $message ?? 'Ошибка валидации модели: ' . $record::class;

        \Yii::error([
            'exception' => __CLASS__,
            'file' => $this->getFile().":".$this->getLine(),
            'class' => $record::class,
            'message' => $message,
            'errors' => $this->errors,
        ], __CLASS__);

        parent::__construct($message, $code, $previous);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }
}