<?php

namespace common\weapon\exception;

use Throwable;
use yii\web\HttpException;

class HaveRelationRecordsException extends \DomainException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        \Yii::error([
            'exception' => __CLASS__,
            'file' => $this->getFile().":".$this->getLine(),
            'message' => $message
        ], __CLASS__);
        parent::__construct($message, $code, $previous);
    }
}