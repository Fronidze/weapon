<?php

namespace common\weapon\exception;

use Throwable;

class ServiceCreateException extends \DomainException
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}