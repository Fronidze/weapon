<?php

namespace common\weapon\helper;

use yii\base\View;

class Breadcrumbs
{
    private string $key = 'breadcrumbs';

    public function __construct(private View $view)
    {
    }

    public static function instance(View $view): static
    {
        return new static($view);
    }

    public function setSection(string $title, ?array $link = null): static
    {

        if (array_key_exists($this->key, $this->view->params) === false) {
            $this->view->params[$this->key][] = [
                'label' => $title,
                'url' => $link
            ];
        } else {
            array_push($this->view->params[$this->key], [
                'label' => $title,
                'url' => $link
            ]);
        }

        return $this;
    }
}