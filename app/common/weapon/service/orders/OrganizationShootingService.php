<?php

namespace common\weapon\service\orders;

use common\jobs\orders\OrganizationShootingJobs;
use common\models\Files;
use common\models\OrganizationShootingOrders;
use common\modules\document\requests\OrganizationShootingOrderRequest;
use common\weapon\repository\orders\OrganizationShootingRepository;

class OrganizationShootingService
{
    private OrganizationShootingRepository $repository;

    public function __construct()
    {
        $this->repository = new OrganizationShootingRepository();
    }

    public function checkExists(): void
    {
        $datetime = new \DateTime();
        $currentOrders = $this->repository->findByCreatedDate($datetime);
        if ($currentOrders !== null) {
            $message = 'Приказ на ' . \Yii::$app->formatter->asDate($datetime->format(DATE_ATOM), 'full') . ' уже создан';
            throw new \DomainException($message);
        }
    }

    public function create(OrganizationShootingOrderRequest $request): OrganizationShootingOrders
    {
        $order = $this->repository->save(
            new OrganizationShootingOrders($request->getAttributes())
        );

        \Yii::$app->get('queue')->push(new OrganizationShootingJobs([
            'order' => $order
        ]));

        return $order;
    }

    public function attachFile(OrganizationShootingOrders $orders, Files $files): OrganizationShootingOrders
    {
        $orders->file_id = $files->id;
        return $this->repository->save($orders);
    }
}