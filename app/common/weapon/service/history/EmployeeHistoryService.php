<?php

namespace common\weapon\service\history;

use common\models\EmployeeHistory;
use common\weapon\entity\enum\EmployeeActions;
use common\weapon\exception\ValidateException;
use Ramsey\Uuid\Uuid;

class EmployeeHistoryService
{

    public static function write(
        EmployeeActions $action,
        string $description,
        string|null $target
    )
    {
        $record = new EmployeeHistory();
        $record->id = Uuid::uuid4()->toString();
        $record->action = $action->value;
        $record->source = \Yii::$app->user->getId();
        $record->target = $target;
        $record->description = $description;

        if ($record->validate() === false) {
            throw new ValidateException($record);
        }

        $record->save(false);
        $record->refresh();
    }

}