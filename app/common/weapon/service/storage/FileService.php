<?php

namespace common\weapon\service\storage;

use common\models\Files;
use common\weapon\exception\ValidateException;
use Ramsey\Uuid\Uuid;
use yii\base\ErrorException;
use yii\base\Model;
use yii\helpers\FileHelper;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;

class FileService
{
    public function saveFile(Model $request, string $attribute, string $path = ''): ?Files
    {
        $uploader = UploadedFile::getInstance($request, $attribute);
        if ($uploader === null) {
            return null;
        }

        $folder = $this->createDirectory($this->makeFolderPath($path));
        $uploader->saveAs("$folder/{$uploader->getBaseName()}.{$uploader->getExtension()}");

        return $this->createFileRecord(
            $uploader->getBaseName().'.'.$uploader->getExtension(),
            $path,
            $uploader->type,
            $uploader->size
        );
    }

    public function makeFolderPath(string $folder): string
    {
        $folder = \Yii::getAlias("@storage/{$folder}");
        return $this->createDirectory($folder);
    }

    protected function createDirectory(string $folder): string
    {
        if (FileHelper::createDirectory($folder, 0777, true) === false) {
            throw new ErrorException("Не смогли создать папку: {$folder}");
        }

        return $folder;
    }

    public function createFileRecord(string $filename, string $path, string $type, int $size): Files
    {
        $files = new Files([
            'id' => Uuid::uuid4()->toString(),
            'name' => $filename,
            'path' => $path,
            'type' => $type,
            'size' => $size,
        ]);

        if ($files->validate() === false) {
            throw new ValidateException($files);
        }

        $files->save(false);
        $files->refresh();

        return $files;
    }

    public function getDownloadPath(string $id)
    {
        /** @var Files $files */
        $files = Files::find()->where(['=', 'id', $id])->one();
        if ($files === null) {
            throw new NotFoundHttpException();
        }

        return \Yii::getAlias("@storage/{$files->path}/{$files->name}");
    }
    public static function getWebPath(string $id): string
    {
        /** @var Files $files */
        $files = Files::find()->where(['=', 'id', $id])->one();
        if ($files === null) {
            throw new NotFoundHttpException();
        }

        return "/storage/{$files->path}/{$files->name}";
    }

}