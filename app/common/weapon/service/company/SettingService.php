<?php

namespace common\weapon\service\company;

use backend\request\SettingsRequest;
use common\models\Settings;
use common\weapon\repository\SettingsRepository;
use yii\web\NotFoundHttpException;

class SettingService
{
    private SettingsRepository $settingRepository;

    public function __construct()
    {
        $this->settingRepository = new SettingsRepository();
    }

    public function current(): Settings
    {
        $settings = $this->settingRepository->list();
        if (empty($settings) === true) {
            throw new NotFoundHttpException("Настройки не найдены");
        }

        $current = current($settings);
        if ($current instanceof Settings === false) {
            throw new \LogicException('Не удалось определить текущие настройки клуба');
        }
        return $current;
    }

    public function create(SettingsRequest $request): Settings
    {
        $settings = $this->settingRepository->find($request->id);
        if ($settings === null) {
            $settings = new Settings();
        }

        $settings->setAttributes($request->getAttributes());
        return $this->settingRepository->save($settings);
    }
}