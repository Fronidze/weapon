<?php

namespace common\weapon\service\company;

use backend\request\PositionRequest;
use common\models\PositionRoles;
use common\models\Positions;
use common\weapon\exception\ServiceCreateException;
use common\weapon\exception\ValidateException;
use common\weapon\repository\PositionRepository;
use common\weapon\service\profile\AccountService;
use yii\web\NotFoundHttpException;

class PositionService
{
    private PositionRepository $positionRepository;
    private AccountService $accountService;

    public function __construct()
    {
        $this->positionRepository = new PositionRepository();
        $this->accountService = new AccountService();
    }

    public function create(PositionRequest $request): Positions
    {
        $positionModel = new Positions(['id' => $request->id, 'title' => $request->title]);
        $position = $this->positionRepository->save($positionModel);

        $permissions = array_keys(array_filter($request->roles, function ($value) {
            return (bool)$value;
        }));
        $this->positionRepository->setPositionPermission($position->id, $permissions);
        return $position;
    }

    public function update(PositionRequest $request): Positions
    {
        $positionModel = $this->positionRepository->find($request->id);
        if ($positionModel === null) {
            throw new NotFoundHttpException("Должность не найдена");
        }
        $positionModel->title = $request->title;
        $position = $this->positionRepository->save($positionModel);
        $permission = array_keys(array_filter($request->roles, function ($value) {
            return (bool)$value;
        }));
        // Установка новых разрешений для должности
        $this->positionRepository->setPositionPermission($position->id, $permission);

        // Изменение у сотрудников на этой должности разрешений
        $this->accountService->changePermissionForPosition($position->id);

        return $position;
    }

}