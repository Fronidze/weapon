<?php

namespace common\weapon\service\profile;

use backend\request\EmployeeRequest;
use common\jobs\account\PositionChangeJob;
use common\models\Employee;
use common\weapon\entity\enum\EmployeeActions;
use common\weapon\entity\enum\EmployeeStatus;
use common\weapon\entity\enum\Role;
use common\weapon\exception\ValidateException;
use common\weapon\repository\AccountRepository;
use common\weapon\service\history\EmployeeHistoryService;
use yii\web\NotFoundHttpException;

class EmployeeService
{
    private AccountRepository $accountRepository;
    private AccountService $accountService;

    public function __construct()
    {
        $this->accountRepository = new AccountRepository();
        $this->accountService = new AccountService();
    }

    public function create(EmployeeRequest $request): Employee
    {
        $employee = new Employee($request->getAttributes());
        if ($employee->account_id === null) {
            $account = $this->accountRepository->findByEmail($employee->email);
            if ($account === null) {
                $account = $this->accountService->create(
                    $employee->email,
                    \Yii::$app->security->generateRandomString(10),
                    Role::Employee->value
                );
            }
            $employee->account_id = $account->id;
        }

        $employee->status = EmployeeStatus::Draft->value;
        $employee->save(false);
        $employee->refresh();

        EmployeeHistoryService::write(
            EmployeeActions::Create,
            'Создание нового пользователя',
            $employee->id
        );

        return $employee;
    }

    public function hire(string $id, string $position_id): Employee
    {
        $employee = Employee::find()
            ->where(['=', 'id', $id])
            ->one();

        if ($employee instanceof Employee === false) {
            throw new NotFoundHttpException('Сотрудник не найден.');
        }

        $employee->position_id = $position_id;
        $employee->employment_at = (new \DateTime())->format(DATE_ATOM);
        $employee->status = EmployeeStatus::Hired->value;
        if ($employee->validate() === false) {
            throw new ValidateException($employee);
        }

        $employee->update();
        $employee->refresh();

        (new AccountService())
            ->setPermissionForPosition($employee->account_id, $employee->position_id);

        \Yii::$app->get('queue')
            ->push(new PositionChangeJob([
                'email' => $employee->email,
                'position' => $employee?->position?->title ?? 'не определена'
            ]));

        return $employee;
    }

    public function sendOnHoliday(string $id)
    {

    }

    public function fire(string $id)
    {

    }
}