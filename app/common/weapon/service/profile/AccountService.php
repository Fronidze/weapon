<?php

namespace common\weapon\service\profile;

use common\jobs\account\RegisterJob;
use common\jobs\account\RoleAssignJob;
use common\models\Accounts;
use common\models\Employee;
use common\models\Positions;
use common\weapon\entity\enum\Role;
use common\weapon\entity\enum\Status;
use common\weapon\repository\AccountRepository;
use Ramsey\Uuid\Uuid;
use yii\helpers\ArrayHelper;
use yii\rbac\Item;
use yii\web\NotFoundHttpException;

class AccountService
{
    private AccountRepository $repository;

    public function __construct()
    {
        $this->repository = new AccountRepository();
    }

    public function create(string $email, string $password, string|null $role = null): Accounts
    {
        $account = new Accounts([
            'id' => Uuid::uuid4()->toString(),
            'email' => $email,
            'password' => \Yii::$app->security->generatePasswordHash($password),
            'authentication_key' => \Yii::$app->security->generateRandomString(),
        ]);

        $authManager = \Yii::$app->authManager;
        $role = $authManager->getRole($role ?? Role::Client->getRoleName());
        $authManager->assign($role, $account->id);

        \Yii::$app->get('queue')
            ->push(new RegisterJob(['login' => $email, 'password' => $password]));

        return $this->repository->save($account);
    }

    public function activate(Accounts $account): Accounts
    {
        $account->status = Status::active->value;
        return $this->repository->save($account);
    }

    public function remove(Accounts $account): Accounts
    {
        $account->status = Status::remove->value;
        return $this->repository->save($account);
    }

    public function changeRole(string $email, string $roleName)
    {
        $account = $this->repository->findByEmail($email);
        if ($account === null) {
            throw new NotFoundHttpException("Пользователь не найден");
        }

        $assignRole = Role::from($roleName);
        $authManager = \Yii::$app->authManager;
        $roles = $authManager->getRolesByUser($account->id);
        foreach ($roles as $role) {
            $authManager->revoke($role, $account->id);
        }

        $role = $authManager->getRole($assignRole->getRoleName());
        $authManager->assign($role, $account->id);

        \Yii::$app->get('queue')
            ->push(new RoleAssignJob([
                'email' => $email,
                'role' => $assignRole->getLabelRoleName()
            ]));
    }

    public function setPermissionForPosition(string $account_id, string $position_id)
    {
        $authManager = \Yii::$app->authManager;
        $permissions = $authManager->getPermissionsByUser($account_id);
        foreach ($permissions as $permission) {
            $authManager->revoke($permission, $account_id);
        }

        $position = Positions::find()
            ->with(['positionRoles'])
            ->where(['=', 'id', $position_id])
            ->one();
        if ($position instanceof Positions === false) {
            throw new NotFoundHttpException("Должность не найдена");
        }

        $permissions = ArrayHelper::getColumn($position->positionRoles, 'items_id');
        foreach ($permissions as $permission) {
            $permissionItem = $authManager->getPermission($permission);
            if ($permissionItem instanceof Item === false) {
                continue;
            }

            $authManager->assign($permissionItem, $account_id);
        }
    }

    public function changePermissionForPosition(string $position_id)
    {
        $employees = Employee::find()
            ->where(['=', 'position_id', $position_id])
            ->all();

        foreach ($employees as $employee) {
            if ($employee instanceof Employee === false) {
                continue;
            }

            $this->setPermissionForPosition($employee->account_id, $position_id);
        }
    }
}