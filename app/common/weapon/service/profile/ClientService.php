<?php

namespace common\weapon\service\profile;

use backend\request\ClientRequest;
use common\models\Clients;
use common\weapon\exception\ValidateException;
use common\weapon\service\storage\FileService;

class ClientService
{
    public function create(ClientRequest $request): Clients
    {
        $fileService = new FileService();
        $transaction = \Yii::$app->db->beginTransaction();
        try {

            $photoFile = $fileService->saveFile($request, 'photo_id', "client/{$request->id}/photo");
            $passportFile = $fileService->saveFile($request, 'passport_id', "client/{$request->id}/passport");
            $declarationFile = $fileService->saveFile($request, 'declaration_id', "client/{$request->id}/declaration");

            $request->photo_id = $photoFile->id;
            $request->passport_id = $passportFile->id;
            $request->declaration_id = $declarationFile->id;
            
            $record = new Clients($request->getAttributes());
            if ($record->validate() === false) {
                throw new ValidateException($record);
            }

            $record->save(false);
            $record->refresh();

            $transaction->commit();
            return $record;
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw new $exception;
        }
    }
}