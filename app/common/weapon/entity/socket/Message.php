<?php

namespace common\weapon\entity\socket;

class Message implements \Stringable
{

    public function __construct(
        public string $body,
        public string $type = 'message',
        public ?string $sender = null,
        public ?string $recipient = null,
    )
    {
    }

    public function toJson(): string
    {
        return json_encode([
            'type' => $this->type,
            'sender' => $this->sender,
            'recipient' => $this->recipient,
            'body' => $this->body,
        ]);
    }

    public function __toString(): string
    {
        return $this->toJson();
    }

}