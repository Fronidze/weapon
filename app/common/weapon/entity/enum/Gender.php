<?php

namespace common\weapon\entity\enum;

enum Gender: string
{
    case Male = 'male';
    case Female = 'female';

    public function getLabel(): string {
        return match ($this) {
            Gender::Male => 'мужской',
            Gender::Female => 'женский',
        };
    }
}