<?php

namespace common\weapon\entity\enum;

use yii\helpers\Html;

enum EmployeeStatus: string
{
    case Draft = 'draft';
    case Hired = 'hired';
    case InHoliday = 'in_holiday';
    case Fired = 'fired';

    public function renderLabel(): string
    {
        return Html::tag('span', $this->getLabelText(), ['class' => ['label', $this->getClassLabel()]]);
    }

    protected function getClassLabel(): string
    {
        return match($this) {
            static::Draft => '',
            static::Hired => 'label-primary',
            static::InHoliday => 'label-info',
            static::Fired => 'label-danger',
        };
    }

    protected function getLabelText()
    {
        return match($this) {
            static::Draft => 'Черновик',
            static::Hired => 'Принят',
            static::InHoliday => 'В отпуске',
            static::Fired => 'Уволен',
        };
    }
}