<?php

namespace common\weapon\entity\enum;

enum Status: string
{
    case create = 'create';
    case active = 'active';
    case block = 'block';
    case remove = 'remove';
}