<?php

namespace common\weapon\entity\enum;

enum Role: string
{
    case Admin = 'admin';
    case Employee = 'employee';
    case Client = 'client';

    public function getRoleName(): string
    {
        return $this->value;
    }

    public function getLabelRoleName(): string
    {
        return match ($this) {
            Role::Admin => 'Администратор',
            Role::Employee => 'Сотрудник',
            Role::Client => 'Клиент',
        };
    }

    public static function getList(): array
    {
        return [
            self::Admin->getRoleName(),
            self::Employee->getRoleName(),
            self::Client->getRoleName(),
        ];
    }
}