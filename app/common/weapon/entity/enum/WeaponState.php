<?php

namespace common\weapon\entity\enum;

enum WeaponState: string
{
    case Bought = 'bought';
    case Storage = 'storage';
    case Gallery = 'gallery';
    case Repair = 'repair';
    case Disposal = 'disposal';

    public function getState(): string
    {
        return $this->value;
    }

    public function getLabel(): string
    {
        return match (true) {
            $this->name === 'Bought' => 'Куплен',
            $this->name === 'Storage' => 'На складе',
            $this->name === 'Gallery' => 'В галерее',
            $this->name === 'Repair' => 'В ремонте',
            $this->name === 'Disposal' => 'Списан',
        };
    }
}