<?php

namespace common\weapon\entity\enum;

use JetBrains\PhpStorm\Pure;

enum Permission: string
{
    case Client = 'accessClient';
    case Reception = 'accessReception';
    case Directory = 'accessDirectory';
    case Document = 'accessDocument';
    case Weapon = 'accessWeapon';
    case Storage = 'accessStorage';

    public function getRoleName(): string
    {
        return $this->value;
    }

    public function getLabelRoleName(): string
    {
        return match ($this) {
            Permission::Client => 'Запись и История посещений (Как клиент)',
            Permission::Reception => 'Работа с Ресепшн',
            Permission::Directory => 'Изменение и создание Справочников',
            Permission::Document => 'Просмотр, Скачивание документации',
            Permission::Weapon => 'Перемещение оружия',
            Permission::Storage => 'Выдача оружия в галерею, Принятие из галереи, Просмотр склада',
        };
    }

    #[Pure]
    public static function getList(): array
    {
        return [
            self::Client->getRoleName(),
            self::Reception->getRoleName(),
            self::Directory->getRoleName(),
            self::Document->getRoleName(),
            self::Weapon->getRoleName(),
            self::Storage->getRoleName(),
        ];
    }
}