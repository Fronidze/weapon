<?php

namespace common\weapon\entity\enum;

enum ShooterExperience: string
{
    case Junior = 'junior';
    case Middle = 'middle';
    case Senior = 'senior';

    public function getLabel(): string {
        return match ($this) {
            ShooterExperience::Junior => 'начинающий',
            ShooterExperience::Middle => 'продвинутый',
            ShooterExperience::Senior => 'професионал',
        };
    }
}