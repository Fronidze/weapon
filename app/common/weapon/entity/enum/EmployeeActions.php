<?php

namespace common\weapon\entity\enum;

enum EmployeeActions: string
{
    case Create = 'create';
    case AttachRole = 'attach-role';
    case AttachPosition = 'attach-position';
    case ChangeStatus = 'change-status';
}