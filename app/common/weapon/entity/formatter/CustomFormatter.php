<?php

namespace common\weapon\entity\formatter;

use common\models\Employee;
use yii\i18n\Formatter;

class CustomFormatter extends Formatter
{
    public function asEmployeeName(Employee $employee, string $type = 'full')
    {
        $lastName = $employee->last_name;
        $firstName = $employee->first_name;
        $middleName = $employee->middle_name;

        if($type === 'short') {
            $firstName = mb_substr($firstName, 0, 1) . ".";
            $middleName = mb_substr($middleName, 0, 1) . ".";
        }

        return implode(' ', [$lastName, $firstName, $middleName]);
    }
}