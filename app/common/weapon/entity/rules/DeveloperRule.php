<?php

namespace common\weapon\entity\rules;

use yii\rbac\Rule;

class DeveloperRule extends Rule
{
    public $name = 'developerRule';

    public function execute($user, $item, $params): bool
    {
        return $user === 1;
    }
}