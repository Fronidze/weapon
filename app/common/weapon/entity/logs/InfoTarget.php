<?php

namespace common\weapon\entity\logs;

use yii\helpers\VarDumper;
use yii\log\FileTarget;

class InfoTarget extends FileTarget
{
    public function formatMessage($message)
    {
        list($text, $level, $category, $timestamp) = $message;
        if (!is_string($text)) {
            if ($text instanceof \Throwable || $text instanceof \Exception) {
                $text = (string)$text;
            } else {
                $text = VarDumper::export($text);
            }
        }

        $separate = '========================================================';
        return $this->getTime($timestamp) . "\n$text\n{$separate}";
    }
}