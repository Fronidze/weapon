<?php

namespace common\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;


class Alert extends Widget
{

    public array $alertTypes = [
        'error' => 'alert-danger',
        'danger' => 'alert-danger',
        'success' => 'alert-success',
        'info' => 'alert-info',
        'warning' => 'alert-warning'
    ];

    public function run()
    {
        $session = Yii::$app->session;
        $flashes = $session->getAllFlashes();

        foreach ($flashes as $type => $flash) {
            if (!isset($this->alertTypes[$type])) {
                continue;
            }

            echo Html::beginTag('div', ['class' => ['alert', $this->alertTypes[$type], 'alert-dismissable']]);
                echo Html::button('×', [
                    'class' => 'close',
                    'data' => ['dismiss' => 'alert']
                ]);
                echo $flash;
            echo Html::endTag('div');

//            <div class="alert alert-success alert-dismissable">
//                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
//            A wonderful serenity has taken possession. <a class="alert-link" href="#">Alert Link</a>.
//                            </div>

            $session->removeFlash($type);
        }
    }

    public static function success(string $message)
    {
        \Yii::$app->session->setFlash('success', $message);
    }

    public static function error(string $message)
    {
        \Yii::$app->session->setFlash('error', $message);
    }

    public static function danger(string $message)
    {
        \Yii::$app->session->setFlash('danger', $message);
    }

    public static function info(string $message)
    {
        \Yii::$app->session->setFlash('info', $message);
    }

    public static function warning(string $message)
    {
        \Yii::$app->session->setFlash('warning', $message);
    }

    public static function throwable(\Throwable $throwable)
    {
        static::error('Системная ошибка: Обратитесь к администратору');
    }
}
