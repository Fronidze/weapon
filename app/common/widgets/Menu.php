<?php

namespace common\widgets;

use common\models\Accounts;
use common\weapon\entity\enum\Role;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class Menu extends \yii\widgets\Menu
{
    public $encodeLabels = false;
    public $activateParents = true;
    public $options = [
        'id' => 'side-menu',
        'class' => ['nav', 'metismenu']
    ];
    public $labelTemplate = "<span class='nav-label'>{label}</span>";
    public $linkTemplate = "<a href='{url}'>{icon}<span class='nav-label'>{label}</span>{arrow}</a>";
    public $submenuTemplate = "\n<ul class='nav nav-second-level'>\n{items}\n</ul>\n";

    public function init()
    {
        /** @var Accounts $account */
        $account = \Yii::$app->user->getIdentity();

        $email = $account?->email;
        $roleName = null;

        $roles = array_keys(\Yii::$app->authManager->getRolesByUser($account->id));
        if (empty($roles) === false) {
            $roleName = Role::from(current($roles))->getLabelRoleName();
        }
        array_unshift($this->items, [
            'options' => ['class' => 'nav-header'],
            'label' => '<div class="profile-element">
                        <img alt="image" class="rounded-circle" src="/img/profile_small.jpg"/>
                        <a href="#">
                            <span class="block m-t-xs font-bold">' . $email . '</span>
                            <span class="text-muted text-xs block">' . $roleName . '</span>
                        </a>
                    </div>'
        ]);
        parent::init();
    }

    protected function renderItem($item)
    {
        $icon = null;
        $iconArrowDown = null;
        if (array_key_exists('icon', $item)) {
            $icon = Html::tag('i', null, ['class' => $item['icon']]);
        }

        if (array_key_exists('items', $item) && empty($item['items']) === false) {
            $iconArrowDown = Html::tag('span', null, ['class' => 'fa arrow']);
        }

        if (isset($item['url'])) {
            $template = ArrayHelper::getValue($item, 'template', $this->linkTemplate);
            return strtr($template, [
                '{url}' => Html::encode(Url::to($item['url'])),
                '{label}' => $item['label'],
                '{icon}' => $icon,
                '{arrow}' => $iconArrowDown,
            ]);
        }

        $template = ArrayHelper::getValue($item, 'template', $this->labelTemplate);
        return strtr($template, [
            '{label}' => $item['label'],
        ]);
    }
}