<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@storage', dirname(dirname(__DIR__)) . '/storage');

$envFile = __DIR__ . '../../../.env';
if (file_exists($envFile)) {
    (\Dotenv\Dotenv::createUnsafeImmutable(dirname(dirname(__DIR__))))
        ->load();
}