<?php

use common\weapon\entity\enum\Role;
use common\weapon\entity\logs\InfoTarget;
use common\weapon\exception\HaveRelationRecordsException;
use common\weapon\exception\RepositoryRuntimeException;
use common\weapon\exception\ServiceCreateException;
use common\weapon\exception\ValidateException;
use yii\filters\AccessControl;
use yii\swiftmailer\Mailer;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => [
        \common\weapon\entity\config\Setup::class,
        'queue',
    ],
    'language' => 'ru_RU',
    'components' => [
        'session' => [
            'class' => \yii\redis\Session::class
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'db' => [
            'class' => \yii\db\Connection::class,
            'dsn' => 'pgsql:host=' . getenv('DATABASE_HOST') . ';port=' . getenv('DATABASE_PORT') . ';dbname=' . getenv('DATABASE_NAME'),
            'username' => getenv("DATABASE_USER"),
            'password' => getenv("DATABASE_PASSWORD"),
            'charset' => 'utf8',
        ],
        'authManager' => [
            'class' => \yii\rbac\DbManager::class
        ],
        'queue' => [
            'class' => \yii\queue\redis\Queue::class,
            'redis' => 'redis',
            'channel' => 'queue'
        ],
        'redis' => [
            'class' => \yii\redis\Connection::class,
            'hostname' => 'redis',
            'password' => '405968'
        ],
        'log' => [
            'targets' => [
                [
                    'class' => InfoTarget::class,
                    'logFile' => '@common/runtime/logs/info.log',
                    'levels' => ['error'],
                    'categories' => [
                        HaveRelationRecordsException::class,
                        RepositoryRuntimeException::class,
                        ValidateException::class,
                        ServiceCreateException::class,
                    ],
                    'logVars' => [],
                ],
            ],
        ],
        'mailer' => [
            'class' => Mailer::class,
            'viewPath' => '@common/mail',
            'useFileTransport' => false,
            'messageConfig' => [
                'charset' => 'UTF-8',
                'from' => [getenv('SMTP_USERNAME') => 'Сервис SmartTir'],
            ],
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => getenv('SMTP_HOST'),
                'username' => getenv('SMTP_USERNAME'),
                'password' => getenv('SMTP_PASSWORD'),
                'port' => getenv('SMTP_PORT'),
                'encryption' => getenv('SMTP_ENCRYPTION'),
            ],
        ],
    ],
    'modules' => [
        'builder' => ['class' => \common\modules\builder\Module::class],
        'directory' => ['class' => \common\modules\directory\Module::class],
        'document' => ['class' => \common\modules\document\Module::class],
    ],
    'controllerMap' => [
        'migrate' => [
            'class' => \yii\console\controllers\MigrateController::class,
            'migrationPath' => [
                '@console/migrations',
                '@yii/rbac/migrations',
            ],
//            'migrationNamespaces' => [
//                'yii\queue\db\migrations'
//            ],
        ],
    ],
];
