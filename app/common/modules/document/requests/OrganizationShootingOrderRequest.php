<?php

namespace common\modules\document\requests;

use backend\request\BaseModel;
use common\attributes\ModelLabelAttribute;
use common\attributes\RequiredValidateAttribute;
use common\attributes\StringValidateAttribute;
use common\attributes\UuidValidateAttribute;
use common\models\Employee;
use common\weapon\repository\EmployeeRepository;

class OrganizationShootingOrderRequest extends BaseModel
{
    #[StringValidateAttribute, UuidValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Индетификатор')]
    public string|null $id = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Название организации')]
    public string|null $organization_title = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Руководитель стрельб')]
    public string|null $head_employee_id = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Директор клуба')]
    public string|null $director_employee_id = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Дата приказа')]
    public string|null $created_at = null;

    public function listEmployee(): array
    {
        $list = [];
        $employees = (new EmployeeRepository())->list();
        /** @var Employee $employee */
        foreach ($employees as $employee) {
            $list[$employee->id] = implode(' ', [
                $employee->last_name,
                $employee->first_name,
                $employee->middle_name
            ]);
        }

        return $list;
    }
}