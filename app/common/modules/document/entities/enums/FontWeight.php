<?php

namespace common\modules\document\entities\enums;

enum FontWeight: string
{
    case Bold = 'bold';
    case Italic = 'italic';
    case Normal = 'normal';
}