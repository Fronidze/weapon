<?php

namespace common\modules\document\entities\enums;

enum TextAlignment: string
{
    case Top = 'top';
    case Bottom = 'bottom';
    case Left = 'left';
    case Right = 'right';
    case Center = 'center';
    case Justify = 'justify';
    case Normal = 'general';
}