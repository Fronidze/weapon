<?php

namespace common\modules\document\entities\enums;

use PhpOffice\PhpSpreadsheet\Style\Color;

enum FontColor: string
{
    case Black = '000000';
    case LightBlack = '333333';
    case Grey = 'CCCCCC';
}