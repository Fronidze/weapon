<?php

namespace common\modules\document\entities\enums;

enum FontFamily: string
{
    case TimesNewRoman = 'Times New Roman';
    case Arial = 'Arial';
}