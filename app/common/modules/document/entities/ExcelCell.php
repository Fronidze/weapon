<?php

namespace common\modules\document\entities;

use common\modules\document\entities\enums\FontColor;
use common\modules\document\entities\enums\FontFamily;
use common\modules\document\entities\enums\FontWeight;
use common\modules\document\entities\enums\TextAlignment;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ExcelCell
{
    public function __construct(
        protected string    $coordinateFrom,
        protected Worksheet $sheet,
    )
    {
    }

    public function merge(string $coordinateTo): static
    {
        $this->sheet->mergeCells("{$this->coordinateFrom}:{$coordinateTo}");
        return $this;
    }

    public function font(
        FontFamily $name = FontFamily::Arial,
        int        $size = 16,
        FontColor  $color = FontColor::Black,
        FontWeight $weight = FontWeight::Normal,
    ): static
    {
        $font = $this->sheet->getStyle($this->coordinateFrom)->getFont();
        $font->setName($name->value)->setSize($size);
        $font->getColor()->setRGB($color->value);
        match ($weight) {
            FontWeight::Bold => $font->setBold(true),
            FontWeight::Italic => $font->setItalic(true),
            FontWeight::Normal => null,
        };

        return $this;
    }

    public function value(string $value, string $type = DataType::TYPE_STRING): static
    {
        $this->sheet
            ->getCell($this->coordinateFrom)
            ->setValueExplicit($value, $type);

        return $this;
    }

    public function alignment(
        TextAlignment $horizontal = TextAlignment::Normal,
        TextAlignment $vertical = TextAlignment::Normal,
        bool    $isWrap = true,
        int     $indent = 0
    ): static
    {
        $this->sheet->getCell($this->coordinateFrom)->getStyle()
            ->getAlignment()
            ->setWrapText($isWrap)
            ->setIndent($indent)
            ->setHorizontal($horizontal->value)
            ->setVertical($vertical->value);

        return $this;
    }

    public function height(int $height): static
    {
        $coordinates = str_split($this->coordinateFrom);
        $this->sheet->getRowDimension($coordinates[1])
            ->setRowHeight($height);

        return $this;
    }

    public function width(int $width): static
    {
        $coordinates = str_split($this->coordinateFrom);
        $this->sheet->getColumnDimension($coordinates[0])
            ->setWidth($width, 'pt');

        return $this;
    }
}