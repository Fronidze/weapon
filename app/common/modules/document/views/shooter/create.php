<?php

use common\modules\document\requests\OrganizationShootingOrderRequest;
use yii\web\View;

/**
 * @var $this View
 * @var $request OrganizationShootingOrderRequest
 */

$this->title = 'Приказ на проведение стрелью';

\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Приказы', ['index'])
    ->setSection('Новый приказ');

?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Создание нового приказа на проведение стрелью</h5>
            </div>
            <div class="ibox-content">
                <?= $this->render('_form', compact('request')) ?>
            </div>
        </div>
    </div>
</div>



