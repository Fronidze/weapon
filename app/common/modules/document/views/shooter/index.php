<?php

use common\models\OrganizationShootingOrders;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $orders OrganizationShootingOrders[]
 */

$this->title = 'Приказы на стрельбы';

\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection($this->title);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Список приказов на стрельбы</h2>
                <p>тут немного текста про что это за документы и куда зачем</p>
                <div class="input-group">
                    <?php
                    echo Html::a('Создать новый приказ  ', ['create'], ['class' => ['btn btn-primary']])
                    ?>
                </div>
                <div class="clients-list">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <?php foreach ($orders as $order):?>
                                <tr>
                                    <td>
                                        <small>Приказ № <span>448552.1/2</span></small>
                                        <br>
                                        <small class="text-muted">
                                            от
                                            <?php
                                            echo \Yii::$app->formatter->asDate($order->created_at)
                                            ?>
                                        </small>
                                    </td>
                                    <td>
                                        <small>Руководитель стрельб:
                                            <span class="text-navy">
                                                <?php
                                                $employee = $order->headEmployee;
                                                /** @var \common\weapon\entity\formatter\CustomFormatter $formatter */
                                                $formatter = \Yii::$app->formatter;
                                                echo $formatter->asEmployeeName($employee, 'short')
                                                ?>
                                            </span>
                                        </small>
                                        <br>
                                        <small>Директор клуба:
                                            <span class="text-navy">
                                                <?php
                                                $employee = $order->directorEmployee;
                                                /** @var \common\weapon\entity\formatter\CustomFormatter $formatter */
                                                $formatter = \Yii::$app->formatter;
                                                echo $formatter->asEmployeeName($employee, 'short')
                                                ?>
                                            </span>
                                        </small>
                                    </td>
                                    <td>
                                        <small>Количество стрельб:<span class="text-navy"> 25 шт.</span></small>
                                    </td>
                                    <td class="text-right">
                                        <?php
                                        $file_id = $order->file_id;
                                        if ($file_id !== null) {
                                            $icon = Html::tag('i', null, ['class' => 'fa fa-arrow-down']);
                                            echo Html::a($icon, ['/file/download', 'id' => $file_id], ['class' => 'btn btn-xs btn-white']);
                                        } else {
                                            echo Html::tag('small', 'Генерируеться', ['class' => 'text-muted']);
                                        }
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
