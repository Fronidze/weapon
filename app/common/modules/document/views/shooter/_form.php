<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\document\requests\OrganizationShootingOrderRequest
 */

use yii\helpers\Html,
    yii\helpers\ArrayHelper;

?>

<?php
echo Html::beginForm();
echo Html::activeHiddenInput($request, 'id');
?>

<div class="row form-group">
    <div class="col-lg-6">
        <?php
        echo Html::activeLabel($request, 'organization_title');
        echo Html::activeTextInput($request, 'organization_title', ['class' => 'form-control']);
        echo Html::error($request, 'organization_title', ['class' => 'text-danger']);
        ?>
    </div>
    <div class="col-lg-6">
        <?php
        echo Html::activeLabel($request, 'created_at');
        echo Html::activeTextInput($request, 'created_at', [
            'class' => 'form-control',
            'data' => ['datepicker' => true]
        ]);
        echo Html::error($request, 'created_at', ['class' => 'text-danger']);
        ?>
    </div>
</div>

<div class="row form-group">
    <div class="col-lg-6">
        <?php
        echo Html::activeLabel($request, 'head_employee_id');
        echo Html::activeDropDownList($request, 'head_employee_id', $request->listEmployee(), [
            'class' => 'form-control',
            'data' => ['select' => true],
            'prompt' => '-- выберите руководителя стрельб --'
        ]);
        echo Html::error($request, 'head_employee_id', ['class' => 'text-danger']);
        ?>
    </div>
    <div class="col-lg-6">
        <?php
        echo Html::activeLabel($request, 'director_employee_id');
        echo Html::activeDropDownList($request, 'director_employee_id', $request->listEmployee(), [
            'class' => 'form-control',
            'data' => ['select' => true],
            'prompt' => '-- выберите руководителя клуба --'
        ]);
        echo Html::error($request, 'director_employee_id', ['class' => 'text-danger']);
        ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-12 text-left" style="margin-bottom: 0;">
        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']]) ?>
        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>

<?= Html::endForm(); ?>
