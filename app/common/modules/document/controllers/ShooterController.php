<?php

namespace common\modules\document\controllers;

use backend\controllers\BaseAccessController;
use common\modules\document\requests\OrganizationShootingOrderRequest;
use common\weapon\entity\enum\Permission;
use common\weapon\repository\orders\OrganizationShootingRepository;
use common\weapon\service\company\SettingService;
use common\weapon\service\orders\OrganizationShootingService;
use common\widgets\Alert;

class ShooterController extends BaseAccessController
{

    private SettingService $settingsService;
    private OrganizationShootingService $orderService;
    private OrganizationShootingRepository $repository;

    public function init()
    {
        $this->settingsService = new SettingService();
        $this->orderService = new OrganizationShootingService();
        $this->repository = new OrganizationShootingRepository();

        parent::init();
    }

    public function rules(): array
    {
        return [
            [
                'allow' => 'true',
                'roles' => [Permission::Document->getRoleName()]
            ]
        ];
    }

    public function actionIndex(): string
    {
        $orders = $this->repository->list();
        return $this->render('index', compact('orders'));
    }

    public function actionCreate()
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {

            $this->orderService->checkExists();

            $settings = $this->settingsService->current();
            $request = new OrganizationShootingOrderRequest([
                'organization_title' => $settings->title,
                'director_employee_id' => $settings->director_id,
                'created_at' => (new \DateTime())->format('Y-m-d'),
            ]);

            if ($request->loadPostRequest() === true) {
                $this->orderService->create($request);

                Alert::success("Приказ был успешно создан");
                $transaction->commit();
                return $this->redirect(['index']);
            }

            return $this->render('create', compact('request'));
        } catch (\Exception $exception) {
            $transaction->rollBack();
            Alert::error($exception->getMessage());
            return $this->redirect(['index']);
        } catch (\Throwable $throwable) {
            $transaction->rollBack();
            Alert::throwable($throwable);
            return $this->redirect(['index']);
        }
    }
}