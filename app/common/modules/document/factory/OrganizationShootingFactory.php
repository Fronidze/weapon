<?php

namespace common\modules\document\factory;

use common\models\OrganizationShootingOrders;
use common\modules\document\entities\enums\FontWeight;
use common\modules\document\entities\enums\TextAlignment;
use common\modules\document\reports\ShootReport;
use common\weapon\service\orders\OrganizationShootingService;
use common\weapon\service\storage\FileService;
use PhpOffice\PhpSpreadsheet\Writer\Pdf\Mpdf;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use yii\helpers\FileHelper;

class OrganizationShootingFactory
{
    private ShootReport $helper;
    private FileService $fileService;

    public function __construct(
        public OrganizationShootingOrders $orders,
    )
    {
        $this->fileService = new FileService();
        $this->helper = new ShootReport();
    }

    public function generate(string $filename)
    {

        $this->makeHeader();

        $filename .= '.xlsx';
        $path = \Yii::getAlias("@storage/orders/shooting/{$this->orders->id}/");
        FileHelper::createDirectory($path);

        $writer = new Xlsx($this->helper->getSpreadsheet());
        $writer->save($path.$filename);

        $file = $this->fileService->createFileRecord(
            $filename,
            "orders/shooting/{$this->orders->id}",
            'excel',
            filesize($path.$filename)
        );

        (new OrganizationShootingService())
            ->attachFile($this->orders, $file);
    }

    protected function makeHeader()
    {
        $this->helper->getNewCell('A1')->merge('F1')
            ->value($this->orders->organization_title)
            ->alignment(vertical: TextAlignment::Center)
            ->font(weight: FontWeight::Bold);

        $this->helper->getNewCell('A3')->merge('F3')
            ->value($this->orders->created_at)
            ->alignment(vertical: TextAlignment::Center)
            ->font(weight: FontWeight::Bold);
    }
}