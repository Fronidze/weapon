<?php

namespace common\modules\document\reports;

use common\modules\document\entities\ExcelCell;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class ShootReport
{

    protected int $defaultRowHeight = 20;
    protected int $defaultColumnWidth = 80;
    protected string $defaultMeasureUnit = 'pt';

    public function __construct(
        protected ?Spreadsheet $spreadsheet = null,
        protected ?Worksheet $worksheet = null,
    )
    {
        $this->worksheet = $this->prepareWorksheet();
    }

    public function getSpreadsheet(): Spreadsheet
    {
        return $this->spreadsheet;
    }

    protected function prepareWorksheet(): Worksheet
    {
        if ($this->spreadsheet === null) {
            $this->spreadsheet = new Spreadsheet();
        }

        $worksheet = $this->spreadsheet->getActiveSheet();
        $worksheet->getDefaultColumnDimension()->setWidth(
            $this->getColumnWidth(),
            $this->getMeasureUnit()
        );

        $worksheet->getDefaultRowDimension()->setRowHeight($this->getRowHeight());

        $this->spreadsheet->getDefaultStyle()
            ->getAlignment()
            ->setWrapText(true);

        $borders = $this->spreadsheet->getDefaultStyle()->getBorders();
        $borders->getTop()->setBorderStyle(Border::BORDER_THIN)->getColor()->setRGB('333333');
        $borders->getBottom()->setBorderStyle(Border::BORDER_THIN)->getColor()->setRGB('333333');
        $borders->getLeft()->setBorderStyle(Border::BORDER_THIN)->getColor()->setRGB('333333');
        $borders->getRight()->setBorderStyle(Border::BORDER_THIN)->getColor()->setRGB('333333');

        $this->configure($worksheet);
        return $worksheet;
    }

    protected function getPersonalColumnWidth(): array
    {
        return [
            'a' => 50,
            'b' => 150,
            'c' => 180,
            'e' => 100,
        ];
    }

    protected function configure(Worksheet $worksheet)
    {
        foreach ($this->getPersonalColumnWidth() as $stringIndex => $width) {
            $worksheet->getColumnDimension(strtoupper($stringIndex))
                ->setWidth($width, $this->getMeasureUnit());
        }
    }

    public function getRowHeight(): int
    {
        return $this->defaultRowHeight;
    }

    public function getColumnWidth(): int
    {
        return $this->defaultColumnWidth;
    }

    public function getMeasureUnit(): string
    {
        return $this->defaultMeasureUnit;
    }

    public function getNewCell(string $coordinate): ExcelCell
    {
        return new ExcelCell($coordinate, $this->worksheet);
    }
}