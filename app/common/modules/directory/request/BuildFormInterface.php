<?php

namespace common\modules\directory\request;

interface BuildFormInterface
{
    public function fieldConfig(): array;
}