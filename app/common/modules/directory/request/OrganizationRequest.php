<?php

namespace common\modules\directory\request;

use backend\request\BaseModel;
use common\attributes\ModelLabelAttribute;
use common\attributes\NumberValidateAttribute;
use common\attributes\RequiredValidateAttribute;
use common\attributes\StringValidateAttribute;
use common\attributes\UuidValidateAttribute;
use common\models\Organization;

class OrganizationRequest extends BaseModel
{
    #[StringValidateAttribute, UuidValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Индетификатор')]
    public string|null $id = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Короткое название')]
    public string|null $short_name = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Полное название')]
    public string|null $full_name = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Адрес')]
    public string|null $address = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Учредитель')]
    public string|null $manager = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('ИНН')]
    public string|null $inn = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('КПП')]
    public string|null $kpp = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('ОГРН')]
    public string|null $ogrn = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Статус')]
    public string|null $state = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Администратор')]
    public string|null $account_id = null;

    #[NumberValidateAttribute]
    #[ModelLabelAttribute('Дата создания')]
    public int|null $created_at = null;

    #[NumberValidateAttribute]
    #[ModelLabelAttribute('Дата обновления')]
    public int|null $updated_at = null;

    public static function make(): static
    {
        $record = Organization::find()
            ->where(['=', 'account_id', \Yii::$app->user->getId()])
            ->limit(1)
            ->one();

        if ($record instanceof  Organization === false) {
            throw new \RuntimeException("Организация не найдена");
        }

        return new static([
            'id' => $record->id,
            'short_name' => $record->short_name,
            'full_name' => $record->full_name,
            'address' => $record->address,
            'manager' => $record->manager,
            'inn' => $record->inn,
            'kpp' => $record->kpp,
            'ogrn' => $record->ogrn,
            'state' => $record->state,
            'account_id' => $record->account_id,
            'created_at' => $record->created_at,
            'updated_at' => $record->updated_at,
        ]);
    }

}