<?php

namespace common\modules\directory\request;

use common\models\WeaponViews;
use common\modules\directory\models\DirectoryWeaponSubtypes;
use common\modules\directory\models\DirectoryWeaponTypes;
use common\modules\directory\models\DirectoryWeaponViews;
use Ramsey\Uuid\Uuid;
use yii\base\Model;
use yii\db\Query;
use yii\helpers\ArrayHelper;

class WeaponViewsRequest extends Model
{
    public ?string $id = null;
    public ?string $view_id = null;
    public ?string $type_id = null;
    public ?string $subtype_id = null;

    public function rules(): array
    {
        return [
            ['type_id', 'default', 'value' => null],
            ['id', 'default', 'value' => Uuid::uuid4()->toString()],
            [['id', 'view_id', 'type_id', 'subtype_id'], 'string'],
            [['view_id', 'subtype_id'], 'required'],
        ];
    }
    public static function find(string $id): static
    {
        /** @var WeaponViews $record */
        $record = WeaponViews::find()->where(['=', 'id', $id])->one();
        return new static([
            'id' => $record->id,
            'view_id' => $record->view_id,
            'type_id' => $record->type_id,
            'subtype_id' => $record->subtype_id
        ]);
    }
    public function attributeLabels(): array
    {
        return [
            'view_id' => 'Вид вооружения',
            'type_id' => 'Тип вооружения',
            'subtype_id' => 'Подтип вооружения',
        ];
    }
    public function listViews(): array
    {
        $query = (new Query())
            ->from(DirectoryWeaponViews::tableName())
            ->orderBy('title')
            ->all();

        return ArrayHelper::map($query, 'id', 'title');
    }
    public function listTypes(): array
    {
        $query = (new Query())
            ->from(DirectoryWeaponTypes::tableName())
            ->orderBy('title')
            ->all();

        return ArrayHelper::map($query, 'id', 'title');
    }
    public function listSubtypes(): array
    {
        $query = (new Query())
            ->from(DirectoryWeaponSubtypes::tableName())
            ->orderBy('title')
            ->all();

        return ArrayHelper::map($query, 'id', 'title');
    }
}