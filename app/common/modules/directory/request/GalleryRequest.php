<?php

namespace common\modules\directory\request;

use backend\request\BaseModel;
use common\attributes\ModelLabelAttribute;
use common\attributes\NumberValidateAttribute;
use common\attributes\RequiredValidateAttribute;
use common\attributes\StringValidateAttribute;
use common\attributes\UuidValidateAttribute;
use common\weapon\repository\GalleryRepository;

class GalleryRequest extends BaseModel
{
    #[StringValidateAttribute, UuidValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Индентификатор')]
    public string|null $id = null;

    #[StringValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Название Галлереи')]
    public string|null $title = null;

    #[NumberValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Доступное количество посетителей')]
    public int $available_quantity = 4;

    #[NumberValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Продолжительность посещения')]
    public int $duration = 45;

    #[NumberValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Час начала')]
    public int|null $open_at = 0;

    #[NumberValidateAttribute, RequiredValidateAttribute]
    #[ModelLabelAttribute('Час окончания')]
    public int|null $close_at = 23;

    public static function make(string $id): static
    {
        $gallery = (new GalleryRepository())->findByIdentifier($id);
        $object = new static();
        foreach ($gallery->getAttributes() as $attribute => $value) {
            if ($object->hasProperty($attribute) === false) {
                continue;
            }
            $object->{$attribute} = $value;
        }

        return $object;
    }

}