<?php

namespace common\modules\directory\request;

use common\modules\directory\models\Storages;
use Ramsey\Uuid\Uuid;
use yii\base\Model;

class StorageRequest extends Model
{
    public ?string $id = null;
    public ?string $title = null;
    public ?string $number = null;

    public function rules(): array
    {
        return [
            ['id', 'default', 'value' => Uuid::uuid4()->toString()],
            [['id', 'title', 'number'], 'string'],
            [['title', 'number'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Название',
            'number' => 'Номер склада'
        ];
    }

    public static function find(string $id): static
    {
        /** @var Storages $record */
        $record = Storages::find()->where(['=', 'id', $id])->one();
        return new static([
            'id' => $record->id,
            'title' => $record->title,
            'number' => $record->number,
        ]);
    }
}