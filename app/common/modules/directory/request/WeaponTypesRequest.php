<?php

namespace common\modules\directory\request;

use common\models\WeaponTypes;
use Ramsey\Uuid\Uuid;
use yii\base\Model;

class WeaponTypesRequest extends Model
{
    public ?string $id = null;
    public ?string $title = null;

    public function rules(): array
    {
        return [
            ['id', 'default', 'value' => Uuid::uuid4()->toString()],
            [['id', 'title'], 'string'],
            [['title'], 'required'],
        ];
    }

    public static function find(string $id): static
    {
        /** @var WeaponTypes $record */
        $record = WeaponTypes::find()->where(['=', 'id', $id])->one();
        return new static([
            'id' => $record->id,
            'title' => $record->title,
        ]);
    }

    public function attributeLabels(): array
    {
        return [
            'title' => 'Название типа вооружения',
        ];
    }
}