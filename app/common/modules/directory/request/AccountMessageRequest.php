<?php

namespace common\modules\directory\request;

use yii\base\Model;

class AccountMessageRequest extends Model
{

    public string|null $sender = null;
    public string|null $recipient = null;
    public string|null $message = null;

    public function rules(): array
    {
        return [
            [['sender'], 'default', 'value' => \Yii::$app->user->getId()],
            [['sender', 'recipient', 'message'], 'string'],
            [['sender', 'recipient', 'message'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'sender' => 'Отправитель',
            'recipient' => 'Получатель',
            'message' => 'Сообщение',
        ];
    }

}