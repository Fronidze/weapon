<?php

namespace common\modules\directory\controllers;

use backend\controllers\BaseAccessController;
use common\modules\directory\request\GalleryRequest;
use common\modules\directory\service\GalleryService;
use common\weapon\entity\enum\Permission;
use common\weapon\repository\GalleryRepository;
use common\widgets\Alert;
use yii\web\Response;

class GalleryController extends BaseAccessController
{

    private GalleryRepository $repository;
    private GalleryService $service;

    public function rules(): array
    {
        return [
            [
                'allow' => true,
                'roles' => [
                    Permission::Directory->getRoleName()
                ]
            ]
        ];
    }

    public function init()
    {
        $this->repository = new GalleryRepository();
        $this->service = new GalleryService();

        parent::init();
    }

    public function actionIndex(): string
    {
        $galleries = $this->repository->list();
        return $this->render('index', compact('galleries'));
    }

    public function actionCreate(): Response|string
    {
        $request = new GalleryRequest();
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($request->loadPostRequest() === true) {
                $this->service->create($request);
                Alert::success('Новая галлерея успешно создана');
                $transaction->commit();
                return $this->redirect(['index']);
            }
        } catch (\Exception $exception) {
            Alert::error($exception->getMessage());
            $transaction->rollBack();
        } catch (\Throwable $throwable) {
            Alert::throwable($throwable);
            $transaction->rollBack();
        }
        return $this->render('create', compact('request'));
    }

    public function actionUpdate(string $id)
    {
        $request = GalleryRequest::make($id);
        if ($request->loadPostRequest() === true) {
            $this->service->update($id, $request);
            Alert::success("Обновление галлереи прошло успешно");
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('request'));
    }

}