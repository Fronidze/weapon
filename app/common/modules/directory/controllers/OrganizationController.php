<?php

namespace common\modules\directory\controllers;

use backend\controllers\BaseAccessController;
use common\modules\directory\request\GalleryRequest;
use common\modules\directory\request\OrganizationRequest;
use common\modules\directory\service\OrganizationService;
use common\weapon\entity\enum\Permission;
use common\widgets\Alert;

class OrganizationController extends BaseAccessController
{

    private OrganizationService $organizationService;

    public function init()
    {
        $this->organizationService = new OrganizationService();

        parent::init();
    }

    public function rules(): array
    {
        return [
            [
                'allow' => true,
                'roles' => [
                    Permission::Directory->getRoleName()
                ]
            ]
        ];
    }

    public function actionIndex()
    {
        $request = OrganizationRequest::make();
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            if ($request->loadPostRequest() === true) {
                $organization = $this->organizationService->create($request);
                Alert::success('Настройки успешно сохранены');
                $transaction->commit();
                return $this->redirect(['index']);
            }
        } catch (\Exception $exception) {
            Alert::error($exception->getMessage());
            $transaction->rollBack();
        } catch (\Throwable $throwable) {
            Alert::throwable($throwable);
            $transaction->rollBack();
        }

        return $this->render('index', compact('request'));
    }
}