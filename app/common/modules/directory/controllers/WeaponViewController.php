<?php

namespace common\modules\directory\controllers;

use backend\controllers\BaseAccessController;
use common\models\WeaponViews;
use common\modules\directory\request\WeaponViewsRequest;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use common\weapon\exception\ValidateException;
use yii\web\Response;

class WeaponViewController extends BaseAccessController
{
    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [Permission::Directory->getRoleName()]]
        ];
    }

    public function actionIndex(): string
    {
        $weaponTypes = WeaponViews::find()
            ->with('view', 'type', 'subtype')
            ->orderBy(['updated_at' => SORT_DESC])
            ->all();

        return $this->render('index', [
            'weaponTypes' => $weaponTypes
        ]);
    }

    public function actionCreate(): Response|string
    {
       $request = new WeaponViewsRequest();
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $record = new WeaponViews($request->getAttributes());
            if ($record->validate() === false) {
                throw new ValidateException($record);
            }

            $record->save(false);
            return $this->redirect(['index']);
        }

        return $this->render('create', ['request' => $request]);
    }
    public function actionUpdate(string $id): Response|string
    {
        $request = WeaponViewsRequest::find($id);
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            /** @var WeaponViews $record */
            $record = WeaponViews::find()->where(['=', 'id', $id])->one();
            if ($record !== null) {
                $attributes = $request->getAttributes();
                unset($attributes['id']);
                $record->setAttributes($attributes);
                if ($record->validate() === false) {
                    throw new ValidateException($record);
                }
                $record->updated_at = (new \DateTime())->format(DATE_ATOM);
                $record->update(false);
            }
            return $this->redirect(['index']);
        }
        return $this->render('update', ['request' => $request]);
    }

    public function actionRemove(string $id): Response
    {
        WeaponViews::deleteAll(['=', 'id', $id]);
        return $this->redirect(['index']);
    }

}