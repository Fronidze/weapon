<?php

namespace common\modules\directory\controllers;

use backend\controllers\BaseAccessController;
use common\modules\directory\models\Ammunition;
use common\modules\directory\request\AmmunitionRequest;
use common\modules\directory\service\AmmunitionService;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use yii\web\Response;

class AmmunitionController extends BaseAccessController
{

    private AmmunitionService $ammunitionService;

    public function init()
    {
        $this->ammunitionService = new AmmunitionService();
        parent::init();
    }

    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [Permission::Storage->getRoleName()]]
        ];
    }

    public function actionIndex(): string
    {
        $ammunition = Ammunition::find()
            ->with('caliber', 'licence', 'storage')
            ->orderBy(['updated_at' => SORT_DESC])
            ->all();

        return $this->render('index', compact('ammunition'));
    }

    public function actionCreate(): Response|string
    {
        $request = new AmmunitionRequest();
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $this->ammunitionService->create($request);
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('request'));
    }
}