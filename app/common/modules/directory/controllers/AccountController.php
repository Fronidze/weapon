<?php

namespace common\modules\directory\controllers;

use backend\controllers\BaseAccessController;
use common\models\Accounts;
use common\modules\directory\request\AccountMessageRequest;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use common\weapon\entity\socket\Message;

class AccountController extends BaseAccessController
{

    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [Permission::Directory->getRoleName()]]
        ];
    }

    public function actionIndex(): string
    {
        $accounts = Accounts::find()
            ->with(['employee'])
            ->orderBy(['created_at' => SORT_DESC])
            ->all();

        return $this->render('index', compact('accounts'));
    }

    public function actionDetail(string $id)
    {
        $request = new AccountMessageRequest(['recipient' => $id]);
        if ($request->load(\Yii::$app->request->post()) && $request->validate() === true) {
            $message = new Message(
                $request->message,
                'message',
                $request->sender,
                $request->recipient
            );

            \Yii::$app->get('redis')
                ->publish('websocket', $message->toJson());
            return $this->redirect(['index']);
        }
        return $this->render('detail', compact('request'));
    }
}