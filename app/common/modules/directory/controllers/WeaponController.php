<?php

namespace common\modules\directory\controllers;

use backend\controllers\BaseAccessController;
use common\models\Weapons;
use common\modules\directory\request\WeaponRequest;
use common\modules\directory\service\WeaponService;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;

class WeaponController extends BaseAccessController
{

    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [Permission::Storage->getRoleName()]]
        ];
    }

    public function actionIndex(): string
    {
        $weapons = Weapons::find()
            ->orderBy(['updated_at' => SORT_DESC])
            ->all();

        return $this->render('index', compact('weapons'));
    }

    public function actionCreate()
    {
        $service = new WeaponService();
        $request = new WeaponRequest();

        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $service->create($request);
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('request'));
    }
}