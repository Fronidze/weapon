<?php

namespace common\modules\directory\controllers;


use backend\controllers\BaseAccessController;
use common\models\WeaponTypes;
use common\modules\directory\request\WeaponTypesRequest;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use common\weapon\exception\ValidateException;
use yii\web\Controller;
use yii\web\Response;

class WeaponTypeController extends BaseAccessController
{

    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [Permission::Directory->getRoleName()]]
        ];
    }

    public function actionIndex(): string
    {
        $weaponTypes = WeaponTypes::find()
            ->orderBy(['updated_at' => SORT_DESC])
            ->all();

        return $this->render('index', [
            'weaponTypes' => $weaponTypes
        ]);
    }

    public function actionCreate(): Response|string
    {
        $request = new WeaponTypesRequest();
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $record = new WeaponTypes($request->getAttributes());
            if ($record->validate() === false) {
                throw new ValidateException($record);
            }
            $record->save(false);
            return $this->redirect(['index']);
        }

        return $this->render('create', ['request' => $request]);
    }

    public function actionUpdate(string $id): Response|string
    {
        $request = WeaponTypesRequest::find($id);
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            /** @var WeaponTypes $record */
            $record = WeaponTypes::find()->where(['=', 'id', $id])->one();
            if ($record === null) {
                $this->redirect(['index']);
            }
            $record->title = $request->title;
            $record->updated_at = (new \DateTime())->format(DATE_ATOM);
            if ($record->validate() === false) {
                throw new ValidateException($record);
            }

            $record->update(false);
            $this->redirect(['index']);
        }
        return $this->render('update', ['request' => $request]);
    }

    public function actionRemove(string $id): Response
    {
        WeaponTypes::deleteAll(['=', 'id', $id]);
        return $this->redirect(['index']);
    }

}