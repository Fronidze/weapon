<?php

namespace common\modules\directory\controllers;

use backend\controllers\BaseAccessController;
use common\modules\directory\models\Storages;
use common\modules\directory\request\StorageRequest;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use common\weapon\exception\ValidateException;

class StorageController extends BaseAccessController
{

    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [Permission::Storage->getRoleName()]]
        ];
    }

    public function actionIndex(): string
    {
        $storages = Storages::find()
            ->orderBy(['updated_at' => SORT_DESC])
            ->all();

        return $this->render('index', compact('storages'));
    }

    public function actionCreate()
    {
        $request = new StorageRequest();
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            $record = new Storages($request->getAttributes());
            if($record->validate() === false) {
                throw new ValidateException($record);
            }

            $record->save(false);
            return $this->redirect(['index']);
        }

        return $this->render('create', compact('request'));
    }

    public function actionUpdate(string $id)
    {
        $request = StorageRequest::find($id);
        if ($request->load(\Yii::$app->request->post()) && $request->validate()) {
            /** @var Storages $record */
            $record = Storages::find()->where([])->one();
            if ($record === null) {
                return $this->redirect(['index']);
            }
            $record->title = $request->title;
            $record->number = $request->number;
            $record->updated_at = (new \DateTime())->format(DATE_ATOM);

            if ($record->validate() === false) {
                throw new ValidateException($record);
            }

            $record->update(false);
            return $this->redirect(['index']);
        }
        return $this->render('update', compact('request'));
    }

    public function actionRemove(string $id)
    {
        Storages::deleteAll(['=', 'id', $id]);
        return $this->redirect(['index']);
    }
}