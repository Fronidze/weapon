<?php

namespace common\modules\directory\controllers;

use backend\controllers\BaseAccessController;
use common\weapon\entity\enum\Permission;
use common\weapon\entity\enum\Role;
use yii\web\Response;

class MainController extends BaseAccessController
{
    public function rules(): array
    {
        return [
            ['allow' => true, 'roles' => [Permission::Directory->getRoleName()]]
        ];
    }
    public function actionIndex(): Response
    {
        return $this->redirect(['weapon-type/index']);
    }
}