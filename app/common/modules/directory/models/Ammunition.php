<?php

namespace common\modules\directory\models;

use common\models\Calibers;
use common\models\Files;
use Yii;

/**
 * This is the model class for table "ammunition".
 *
 * @property string $id
 * @property string $caliber_id
 * @property string $number
 * @property int $count
 * @property string $storage_id
 * @property string $licence_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Calibers $caliber
 * @property Files $licence
 * @property Storages $storage
 */
class Ammunition extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ammunition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'caliber_id', 'number', 'count', 'storage_id', 'licence_id'], 'required'],
            [['id', 'caliber_id', 'storage_id', 'licence_id'], 'string'],
            [['count'], 'default', 'value' => null],
            [['count'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['number'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['caliber_id'], 'exist', 'skipOnError' => true, 'targetClass' => Calibers::className(), 'targetAttribute' => ['caliber_id' => 'id']],
            [['licence_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::className(), 'targetAttribute' => ['licence_id' => 'id']],
            [['storage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Storages::className(), 'targetAttribute' => ['storage_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'caliber_id' => Yii::t('app', 'Caliber ID'),
            'number' => Yii::t('app', 'Number'),
            'count' => Yii::t('app', 'Count'),
            'storage_id' => Yii::t('app', 'Storage ID'),
            'licence_id' => Yii::t('app', 'Licence ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Caliber]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCaliber()
    {
        return $this->hasOne(Calibers::className(), ['id' => 'caliber_id']);
    }

    /**
     * Gets query for [[Licence]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLicence()
    {
        return $this->hasOne(Files::className(), ['id' => 'licence_id']);
    }

    /**
     * Gets query for [[Storage]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storages::className(), ['id' => 'storage_id']);
    }
}
