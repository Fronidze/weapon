<?php

namespace common\modules\directory\models;

use Yii;

/**
 * This is the model class for table "galleries".
 *
 * @property string $id
 * @property string $title
 * @property int|null $available_quantity
 * @property int|null $duration
 * @property int|null $open_at
 * @property int|null $close_at
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property GallerySchedule[] $gallerySchedules
 */
class Galleries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'galleries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id'], 'string'],
            [['available_quantity', 'duration', 'open_at', 'close_at'], 'default', 'value' => null],
            [['available_quantity', 'duration', 'open_at', 'close_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'available_quantity' => Yii::t('app', 'Available Quantity'),
            'duration' => Yii::t('app', 'Duration'),
            'open_at' => Yii::t('app', 'Open At'),
            'close_at' => Yii::t('app', 'Close At'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[GallerySchedules]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGallerySchedules()
    {
        return $this->hasMany(GallerySchedule::className(), ['gallery_id' => 'id']);
    }
}
