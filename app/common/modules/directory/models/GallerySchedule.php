<?php

namespace common\modules\directory\models;

use Yii;

/**
 * This is the model class for table "gallery_schedule".
 *
 * @property string $id
 * @property string|null $event_id
 * @property string $gallery_id
 * @property string $status
 * @property string $event_at
 * @property string $begin_at
 * @property string $end_at
 *
 * @property Galleries $gallery
 */
class GallerySchedule extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gallery_schedule';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'gallery_id', 'event_at', 'begin_at', 'end_at'], 'required'],
            [['id', 'event_id', 'gallery_id'], 'string'],
            [['event_at', 'begin_at', 'end_at'], 'safe'],
            [['status'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['gallery_id'], 'exist', 'skipOnError' => true, 'targetClass' => Galleries::className(), 'targetAttribute' => ['gallery_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'event_id' => Yii::t('app', 'Event ID'),
            'gallery_id' => Yii::t('app', 'Gallery ID'),
            'status' => Yii::t('app', 'Status'),
            'event_at' => Yii::t('app', 'Event At'),
            'begin_at' => Yii::t('app', 'Begin At'),
            'end_at' => Yii::t('app', 'End At'),
        ];
    }

    /**
     * Gets query for [[Gallery]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGallery()
    {
        return $this->hasOne(Galleries::className(), ['id' => 'gallery_id']);
    }
}
