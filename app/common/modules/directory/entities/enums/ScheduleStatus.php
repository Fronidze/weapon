<?php

namespace common\modules\directory\entities\enums;

use yii\helpers\Html;

enum ScheduleStatus: string
{
    case Free = 'free';
    case Booked = 'booked';
    case Busy = 'busy';

    public function statusLabel(): string
    {
        return match ($this) {
            self::Free => 'свободно',
            self::Booked => 'бронь',
            self::Busy => 'занято'
        };
    }

    public function renderLabel(): string
    {
        $labelText = $this->statusLabel();
        $labelClass = match ($this) {
            self::Free => 'label label-primary',
            self::Booked => 'label label-warning',
            self::Busy => 'label label-danger'
        };

        return Html::tag('span', $labelText, ['class' => $labelClass]);
    }
}