<?php

namespace common\modules\directory\validation;

use yii\validators\Validator;

class TimestampValidator extends Validator
{
    public string $format = DATE_ATOM;

    public function validateAttribute($model, $attribute)
    {
        try {
            $datetime = new \DateTime($model->{$attribute});
            $model->{$attribute} = $datetime->format($this->format);
        } catch (\Throwable $exception) {
            $model->addError($attribute, $exception->getMessage());
        }
    }
}