<?php

namespace common\modules\directory\validation;

use common\weapon\service\storage\FileService;
use yii\validators\Validator;
use yii\web\UploadedFile;

class StorageValidator extends Validator
{
    public $skipOnEmpty = false;

    public function validateAttribute($model, $attribute): void
    {
        $uploader = UploadedFile::getInstance($model, $attribute);
        if ($uploader === null) {
            $label = $model->getAttributeLabel($attribute);
            $model->addError($attribute, "{$label} обязателен для заполнения.");
        }
    }
}