<?php

namespace common\modules\directory\service;

use common\modules\directory\models\Galleries;
use common\modules\directory\request\GalleryRequest;
use common\weapon\repository\GalleryRepository;
use JetBrains\PhpStorm\Pure;

class GalleryService
{

    private GalleryRepository $repository;
    private GalleryScheduleService $scheduleService;

    #[Pure]
    public function __construct()
    {
        $this->repository = new GalleryRepository();
        $this->scheduleService = new GalleryScheduleService();
    }

    public function create(GalleryRequest $request): Galleries
    {
        $gallery = new Galleries($request->getAttributes());
        $gallery = $this->repository->save($gallery);

        $this->scheduleService->removeTimes($gallery);
        $datetime = new \DateTime();
        for ($day = 1; $day <= 3; $day++) {
            $this->scheduleService->createScheduleByGallery($gallery, $datetime);
            $datetime->modify('+ 1 days');
        }

        return $gallery;
    }

    public function update(string $id, GalleryRequest $request): Galleries
    {
        $gallery = $this->repository->findByIdentifier($id);
        $attributes = $request->getAttributes();
        unset($attributes['id']);

        $gallery->setAttributes($attributes);
        return $this->repository->save($gallery);
    }

}