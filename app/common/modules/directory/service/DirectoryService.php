<?php

namespace common\modules\directory\service;

use common\models\Calibers;
use common\modules\directory\models\Storages;

class DirectoryService
{

    public function getListCalibers(): array
    {
        $result = [];
        $records = Calibers::find()->all();
        foreach ($records as $caliber) {
            $caliberId = $caliber['id'];
            $result[$caliberId] = $caliber['title'];
            if ($caliber['alternative_title'] !== null) {
                $result[$caliberId] .= " ({$caliber['alternative_title']})";
            }
        }
        return $result;
    }

    public function getListStorages(): array
    {
        $result = [];
        $records = Storages::find()->orderBy(['updated_at' => SORT_DESC])->all();
        /** @var Storages $record */
        foreach ($records as $record) {
            $result[$record->id] = "{$record->title} ({$record->number})";
        }

        return $result;
    }

}