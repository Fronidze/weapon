<?php

namespace common\modules\directory\service;

use common\modules\directory\models\Ammunition;
use common\modules\directory\request\AmmunitionRequest;
use common\weapon\exception\ValidateException;
use common\weapon\service\storage\FileService;

class AmmunitionService
{

    public function create(AmmunitionRequest $request)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {

             $files = (new FileService())
                ->saveFile($request, 'licence_id', "ammunition/{$request->id}/licence");

            $request->licence_id = $files?->id;

            $record = new Ammunition($request->getAttributes());
            if ($record->validate() === false) {
                throw new ValidateException($record);
            }

            $record->save(false);
            $record->refresh();

            $transaction->commit();
            return $record;
        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw new $exception;
        }
    }

}