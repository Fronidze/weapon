<?php

namespace common\modules\directory\service;

use common\models\Organization;
use common\modules\directory\dto\OrganizationDto;
use common\modules\directory\request\OrganizationRequest;
use common\weapon\repository\OrganizationRepository;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use yii\helpers\ArrayHelper;

class OrganizationService
{
    private Client $client;
    private OrganizationRepository $organizationRepository;

    public function __construct()
    {
        $this->organizationRepository = new OrganizationRepository();
        $this->client = new Client([
            'base_uri' => 'https://suggestions.dadata.ru',
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'Authorization' => 'Token a6659e0ac14127c0bd91922bf611d09fcecb24d0'
            ],
        ]);
    }

    public function findByInn(string $inn): OrganizationDto
    {
        try {
            $response = $this->client
                ->request('post', '/suggestions/api/4_1/rs/findById/party', ['body' => json_encode(['query' => $inn])]);
            
            $content = $response->getBody()->getContents();
            return $this->parseResponseInfo(json_decode($content, true));
        } catch (ClientException $exception) {
            $content = $exception->getResponse()->getBody()->getContents();
            throw new \RuntimeException($content);
        } catch (\Throwable $exception) {
            throw new \RuntimeException($exception->getMessage());
        }
    }

    private function parseResponseInfo(array $information): OrganizationDto
    {
        $suggestions = ArrayHelper::getValue($information, 'suggestions.0.data', []);
        if (empty($suggestions) === true) {
            throw new \RuntimeException("С данным ИНН организация не найдена.");
        }

        return new OrganizationDto([
            'inn' => ArrayHelper::getValue($suggestions, 'inn'),
            'kpp' => ArrayHelper::getValue($suggestions, 'kpp'),
            'ogrn' => ArrayHelper::getValue($suggestions, 'ogrn'),
            'manager' => ArrayHelper::getValue($suggestions, 'management.name'),
            'shortName' => ArrayHelper::getValue($suggestions, 'name.short'),
            'fullName' => ArrayHelper::getValue($suggestions, 'name.full'),
            'address' => ArrayHelper::getValue($suggestions, 'address.value'),
            'state' => ArrayHelper::getValue($suggestions, 'state.status'),
        ]);
    }

    public function create(OrganizationRequest $request): Organization
    {
        $record = new Organization();
        $record->setAttributes($request->getAttributes());
        return $this->organizationRepository->save($record);
    }

}