<?php

namespace common\modules\directory\service;

use common\models\WeaponCalibers;
use common\models\Weapons;
use common\modules\directory\request\WeaponRequest;
use common\weapon\exception\ValidateException;
use yii\helpers\ArrayHelper;

class WeaponService
{

    public function create(WeaponRequest $request): Weapons
    {
        $transaction = \Yii::$app->db->beginTransaction();
        try {
            $request->saveLicenceFile();
            $request->modifyDatetime();
            $attributes = $request->getAttributes();
            $calibers = ArrayHelper::remove($attributes, 'caliber_id');

            $record = new Weapons($attributes);
            if ($record->validate() === false) {
                throw new ValidateException($record);
            }

            $record->save(false);
            $record->refresh();

            foreach ($calibers as $caliber) {
                \Yii::$app->db->createCommand()
                    ->insert(WeaponCalibers::tableName(), ['weapon_id' => $record->id, 'caliber_id' => $caliber])
                    ->execute();
            }

            $transaction->commit();
            return $record;

        } catch (\Throwable $exception) {
            $transaction->rollBack();
            throw new $exception;
        }
    }

}