<?php

namespace common\modules\directory\service;

use common\modules\directory\models\Galleries;
use common\modules\directory\models\GallerySchedule;
use common\weapon\repository\GalleryScheduleRepository;
use Ramsey\Uuid\Uuid;

class GalleryScheduleService
{

    private GalleryScheduleRepository $repository;

    public function __construct()
    {
        $this->repository = new GalleryScheduleRepository();
    }

    public function removeTimes(Galleries $gallery)
    {
        $scheduleRecords = $this->repository->list($gallery->id);
        foreach ($scheduleRecords as $scheduleRecord) {
            $this->repository->remove($scheduleRecord->id);
        }
    }

    public function createScheduleByGallery(Galleries $gallery, \DateTime|null $dateTime = null)
    {
        $dateTime = $dateTime ?? new \DateTime();
        $startHour = $gallery->open_at;
        while ($startHour <= $gallery->close_at) {

            $schedule = new GallerySchedule();
            $schedule->id = Uuid::uuid4()->toString();
            $schedule->gallery_id = $gallery->id;
            $schedule->event_at = $dateTime->format('Y-m-d');

            $startTime = clone $dateTime;
            $startTime->setTime($startHour, 00);
            $schedule->begin_at = $startTime->format("H:i:s P");

            $endTime = clone $startTime;
            $endTime->modify("+ {$gallery->duration} minutes");
            $schedule->end_at = $endTime->format("H:i:s P");

            $this->repository->save($schedule);
            $startHour += 1;
        }
    }
}