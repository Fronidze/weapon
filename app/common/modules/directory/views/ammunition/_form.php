<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\AmmunitionRequest
 */

use yii\helpers\Html;

?>

<?= Html::beginForm(options: ['enctype' => 'multipart/form-data']); ?>
<?= Html::activeHiddenInput($request, 'id');?>


<div class="row">
    <div class="form-group col-lg-6">
        <?= Html::activeLabel($request, 'caliber_id');?>
        <?= Html::activeDropDownList($request, 'caliber_id', $request->listCalibers(), [
            'class' => 'form-control',
            'prompt' => '-- выберите калибр --',
            'data' => ['select' => true],
        ]);?>
        <?= Html::error($request, 'caliber_id');?>
    </div>
    <div class="form-group col-lg-6">
        <?= Html::activeLabel($request, 'number');?>
        <?= Html::activeTextInput($request, 'number', ['class' => 'form-control']);?>
        <?= Html::error($request, 'number');?>
    </div>
</div>
<div class="row">
    <div class="form-group col-lg-6">
        <?= Html::activeLabel($request, 'count');?>
        <?= Html::activeInput('number', $request, 'count', ['class' => 'form-control']);?>
        <?= Html::error($request, 'count');?>
    </div>
    <div class="form-group col-lg-6">
        <?= Html::activeLabel($request, 'storage_id');?>
        <?= Html::activeDropDownList($request, 'storage_id', $request->listStorages(), [
            'class' => 'form-control',
            'prompt' => '-- выберите склад --',
            'data' => ['select' => true],
        ]);?>
        <?= Html::error($request, 'storage_id');?>
    </div>
</div>
<div class="row">
    <div class="form-group col-lg-6">
        <div class="input-group">
            <div class="custom-file">
                <?= Html::activeFileInput($request, 'licence_id', ['class' => 'custom-file-input'])?>
                <?= Html::activeLabel($request, 'licence_id', ['class' => 'custom-file-label'])?>
            </div>
            <?= Html::error($request, 'licence_id', ['class' => ['text-danger']]);?>
        </div>
    </div>
</div>


<div class="row">
    <div class="form-group col-lg-12 text-right">
        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']])?>
        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>
<?= Html::endForm(); ?>
