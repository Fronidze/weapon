<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\AmmunitionRequest
 */

\backend\assets\Select2Assets::register($this);
\backend\assets\AirDatepickerAssets::register($this);

$this->title = 'Добавление амуниции';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Список амуниции', ['index'])
    ->setSection($this->title);

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Добавление амуниции</h5>
                </div>
                <div class="ibox-content">
                    <?= $this->render('_form', ['request' => $request])?>
                </div>
            </div>
        </div>
    </div>
</div>
