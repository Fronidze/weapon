<?php

/**
 * @var $this \yii\web\View
 * @var $ammunition Ammunition[]
 */

use common\modules\directory\models\Ammunition;
use yii\helpers\Html,
    common\weapon\helper\Breadcrumbs,
    yii\helpers\ArrayHelper;

$this->title = 'Амуниция';
Breadcrumbs::instance($this)
    ->setSection($this->title);

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Доступная амуниция</h5>
                </div>
                <div class="ibox-content">
                    <div class="table__header__wrapper">
                        <?= Html::a('Добавить амуницию', ['create'], ['class' => ['btn', 'btn-primary']]) ?>
                    </div>
                    <table class="table table-hover table-flex">
                        <tr class="row">
                            <th class="col-lg-3">Калибр</th>
                            <th class="col-lg-2">Номер партии</th>
                            <th class="col-lg-2">Кол-во</th>
                            <th class="col-lg-2">Склад</th>
                            <th class="col-lg-2">Лицензия на покупку</th>
                            <th class="col-lg-1 text-center"></th>
                        </tr>
                        <?php
                        /** @var Ammunition $record */
                        foreach ($ammunition as $record): ?>
                            <tr class="row">
                                <td class="col-lg-3">
                                    Патрон калибра: <?= ArrayHelper::getValue($record, 'caliber.title');?>
                                </td>
                                <td class="col-lg-2">
                                    <?= ArrayHelper::getValue($record, 'number'); ?>
                                </td>
                                <td class="col-lg-2">
                                    <?= ArrayHelper::getValue($record, 'count'); ?> шт.
                                </td>
                                <td class="col-lg-2">
                                    <?= ArrayHelper::getValue($record, 'storage.title'); ?>
                                </td>
                                <td class="col-lg-2">
                                    <?php
                                    $icon = Html::tag('i', null, ['class' => 'fa fa-download']);
                                    echo Html::a($icon, ['/file/download', 'id' => $record->licence_id], [
                                        'class' => 'text-success',
                                        'target' => '_blank',
                                    ]);
                                    ?>
                                </td>
                                <td class="col-lg-1 text-center"></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
