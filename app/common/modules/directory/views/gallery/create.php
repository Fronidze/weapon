<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\GalleryRequest
 */

use common\weapon\helper\Breadcrumbs;

$this->title = 'Добавление новой галлереи';

Breadcrumbs::instance($this)
    ->setSection('Список галлерей', ['index'])
    ->setSection('Новая галлерея');

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Детальная страница пользователя</h5>
            </div>
            <div class="ibox-content">
                <?= $this->render('_form', ['request' => $request]) ?>
            </div>
        </div>
    </div>
</div>
