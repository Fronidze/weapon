<?php

/**
 * @var $this \yii\web\View
 * @var $galleries Galleries[]
 */

use common\modules\directory\models\Galleries;
use yii\helpers\Html;

$this->title = 'Справочник Галлерей';

\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection($this->title);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Список доступных галерей в системе</h2>
                <p>тут придумать любой текст что можно было бы помочь пользователю понять что тут делать то</p>
                <div class="input-group">
                    <?php
                    echo Html::a('Новая галлерея', ['create'], ['class' => ['btn btn-primary']]);
                    ?>
                </div>
                <div class="clients-list">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <?php foreach ($galleries as $gallery):?>
                            <tr>
                                <td>
                                    <small><?= $gallery->title; ?></small>
                                </td>
                                <td>
                                    <small>Вместительность:
                                        <span class="text-navy"><?= $gallery->available_quantity?> персоны</span>
                                    </small>
                                </td>
                                <td>
                                    <small>
                                        <span class="text-navy"><?= $gallery->duration ?></span> минут
                                    </small>
                                </td>
                                <td>
                                    <small>Первый сеанс: <span class="text-navy"><?= $gallery->open_at?>:00</span></small>
                                </td>
                                <td>
                                    <small>Последний сеанс: <span class="text-navy"><?= $gallery->close_at?>:00</span></small>
                                </td>
                                <td class="text-right" style="width: 200px">
                                    <?php $icon = Html::tag('i', null, ['class' => ['fa fa-pencil']])?>
                                    <?= Html::a($icon, ['update', 'id' => $gallery->id], ['class' => ['btn btn-xs btn-white']])?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
