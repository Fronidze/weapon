<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\GalleryRequest
 */

use yii\helpers\Html;

echo Html::beginForm();
echo Html::activeHiddenInput($request, 'id');

?>

<div class="row">
    <div class="col-lg-6 form-group">
        <?php
        echo Html::activeLabel($request, 'title');
        echo Html::activeTextInput($request, 'title', ['class' => 'form-control']);
        echo Html::error($request, 'title', ['class' => 'text-danger']);
        ?>
    </div>
    <div class="col-lg-6 form-group">
        <?php
        echo Html::activeLabel($request, 'available_quantity');
        echo Html::activeTextInput($request, 'available_quantity', ['class' => 'form-control']);
        echo Html::error($request, 'available_quantity', ['class' => 'text-danger']);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 form-group">
        <?php
        echo Html::activeLabel($request, 'duration');
        echo Html::activeTextInput($request, 'duration', ['class' => 'form-control']);
        echo Html::error($request, 'duration', ['class' => 'text-danger']);
        ?>
    </div>
    <div class="col-lg-4 form-group">
        <?php
        echo Html::activeLabel($request, 'open_at');
        echo Html::activeTextInput($request, 'open_at', ['class' => 'form-control']);
        echo Html::error($request, 'open_at', ['class' => 'text-danger']);
        ?>
    </div>
    <div class="col-lg-4 form-group">
        <?php
        echo Html::activeLabel($request, 'close_at');
        echo Html::activeTextInput($request, 'close_at', ['class' => 'form-control']);
        echo Html::error($request, 'close_at', ['class' => 'text-danger']);
        ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-12 text-left">
        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']])?>
        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>
<?= Html::endForm(); ?>
