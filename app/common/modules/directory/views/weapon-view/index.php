<?php

/**
 * @var $this \yii\web\View
 * @var $weaponTypes \common\models\WeaponTypes[]
 */

use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$this->title = 'Виды вооружения';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Справочники', ['directory/main/index'])
    ->setSection($this->title);

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Виды вооружения</h5>
                </div>
                <div class="ibox-content">
                    <div class="table__header__wrapper">
                        <?= Html::a('Новый вид', ['create'], ['class' => ['btn', 'btn-primary']]) ?>
                    </div>
                    <table class="table table-hover table-flex">
                        <tr class="row">
                            <th class="col-lg-1 text-center">#</th>
                            <th class="col-lg-3">Вид</th>
                            <th class="col-lg-3">Тип</th>
                            <th class="col-lg-3">Подтип</th>
                            <th class="col-lg-2 text-center"></th>
                        </tr>
                        <?php foreach ($weaponTypes as $index => $type): ?>
                            <tr class="row">
                                <td class="col-lg-1 text-center"><?= $index + 1 ?></td>
                                <td class="col-lg-3"><?= ArrayHelper::getValue($type, 'view.title') ?></td>
                                <td class="col-lg-3"><?= ArrayHelper::getValue($type, 'type.title') ?></td>
                                <td class="col-lg-3"><?= ArrayHelper::getValue($type, 'subtype.title') ?></td>
                                <td class="col-lg-2 text-right">
                                    <div class="btn-group">

                                        <?php
                                        $icon = Html::tag('i', null, ['class' => ['fa', 'fa-pencil']]);
                                        echo Html::a($icon, ['update', 'id' => $type->id], ['class' => ['btn', 'btn-sm', 'btn-white']]);
                                        ?>

                                        <?php
                                        $icon = Html::tag('i', null, ['class' => ['fa', 'fa-trash']]);
                                        echo Html::a($icon, ['remove', 'id' => $type->id], ['class' => ['btn', 'btn-sm', 'btn-danger']]);
                                        ?>

                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
