<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\WeaponViewsRequest
 */

use yii\helpers\Html;

\backend\assets\Select2Assets::register($this);

$this->title = 'Новый вид вооружения';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Справочники', ['directory/main/index'])
    ->setSection('Виды вооружения', ['index'])
    ->setSection($this->title);

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Создание нового вида вооружения</h5>
                </div>
                <div class="ibox-content">
                    <?= $this->render('_form', ['request' => $request])?>
                </div>
            </div>
        </div>
    </div>
</div>
