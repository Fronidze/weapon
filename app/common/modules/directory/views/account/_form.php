<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\AccountMessageRequest
 */

use yii\helpers\Html;

?>

<?= Html::beginForm(options: ['enctype' => 'multipart/form-data']); ?>
<?= Html::activeHiddenInput($request, 'sender');?>
<?= Html::activeHiddenInput($request, 'recipient');?>


<div class="row">
    <div class="form-group col-lg-12">
        <?= Html::activeTextarea(
            $request, 'message',
            [
                'class' => 'form-control',
                'rows' => 4,
            ]
        )?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-12 text-right">
        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']])?>
        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>
<?= Html::endForm(); ?>
