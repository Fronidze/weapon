<?php

/**
 * @var $this \yii\web\View
 * @var $accounts \common\models\Accounts[]
 */

use common\weapon\entity\enum\Status;
use yii\helpers\Html;

$this->title = 'Учетные записи';

\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection($this->title);

?>


<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Список всех учетных записей</h2>
                <p>All clients need to be verified before you can send email and set a project.</p>
                <div class="clients-list">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <?php foreach ($accounts as $account): ?>
                                <tr>
                                    <td class="client-status text-center">
                                        <span class="label label-primary"><?= Status::from($account->status)->value ?></span>
                                    </td>
                                    <td class="contact-type">
                                        <?php if ($account->employee === null): ?>
                                            <?php
                                            $icon = Html::tag('i', null, ['class' => 'fa fa-plus']);
                                            echo Html::a($icon, ['/employee/create-for-account', 'id' => $account->id], ['class' => ['text-navy']]);
                                            ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <span class="badge badge-warning">
                                            <?php
                                            $roles = \Yii::$app->authManager->getRolesByUser($account->id);
                                            echo current(array_keys($roles));
                                            ?>
                                        </span>
                                        <?= $account->email ?><br>
                                        <small class="text-muted"><?= $account->id ?></small>
                                    </td>
                                    <td class="contact-type"><i class="fa fa-envelope"> </i></td>
                                    <td> <?= $account->email ?></td>
                                    <td>
                                        <?= \Yii::$app->formatter->asDate($account->created_at); ?>
                                    </td>
                                    <td>
                                        <?php
                                        $icon = Html::tag('i', null, ['class' => 'fa fa-pencil']);
                                        echo Html::a($icon, ['detail', 'id' => $account->id], ['class' => 'btn btn-sm btn-white'])
                                        ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


