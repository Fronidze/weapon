<?php

/**
 * @var $this View
 * @var $request AccountMessageRequest
 */

use common\modules\directory\request\AccountMessageRequest;
use yii\web\View;

$this->title = 'Детальная страница';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Учетные записи', ['index'])
    ->setSection($this->title);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h5>Детальная страница пользователя</h5>
            </div>
            <div class="ibox-content">
                <?= $this->render('_form', ['request' => $request]) ?>
            </div>
        </div>
    </div>
</div>