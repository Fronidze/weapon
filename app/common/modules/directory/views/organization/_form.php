<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\OrganizationRequest
 */

use yii\helpers\Html;
?>

<?= Html::beginForm(options: ['enctype' => 'multipart/form-data']); ?>
<?= Html::activeHiddenInput($request, 'id');?>


<div class="row">
    <div class="form-group col-lg-4">
        <?= Html::activeLabel($request, 'inn');?>
        <?= Html::activeTextInput($request, 'inn', ['class' => 'form-control']);?>
        <?= Html::error($request, 'inn');?>
    </div>
    <div class="form-group col-lg-4">
        <?= Html::activeLabel($request, 'kpp');?>
        <?= Html::activeTextInput($request, 'kpp', ['class' => 'form-control']);?>
        <?= Html::error($request, 'kpp');?>
    </div>
    <div class="form-group col-lg-4">
        <?= Html::activeLabel($request, 'ogrn');?>
        <?= Html::activeTextInput($request, 'ogrn', ['class' => 'form-control']);?>
        <?= Html::error($request, 'ogrn');?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-6">
        <?= Html::activeLabel($request, 'short_name');?>
        <?= Html::activeTextInput($request, 'short_name', ['class' => 'form-control']);?>
        <?= Html::error($request, 'short_name');?>
    </div>
    <div class="form-group col-lg-6">
        <?= Html::activeLabel($request, 'full_name');?>
        <?= Html::activeTextInput($request, 'full_name', ['class' => 'form-control']);?>
        <?= Html::error($request, 'full_name');?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-6">
        <?= Html::activeLabel($request, 'address');?>
        <?= Html::activeTextInput($request, 'address', ['class' => 'form-control']);?>
        <?= Html::error($request, 'address');?>
    </div>
    <div class="form-group col-lg-6">
        <?= Html::activeLabel($request, 'manager');?>
        <?= Html::activeTextInput($request, 'manager', ['class' => 'form-control']);?>
        <?= Html::error($request, 'manager');?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-6">
        <?= Html::activeLabel($request, 'state');?>
        <?= Html::activeTextInput($request, 'state', ['class' => 'form-control']);?>
        <?= Html::error($request, 'state');?>
    </div>
    <div class="form-group col-lg-6">
        <?= Html::activeLabel($request, 'account_id');?>
        <?= Html::activeTextInput($request, 'account_id', ['class' => 'form-control']);?>
        <?= Html::error($request, 'account_id');?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-12 text-right">
        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']])?>
        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>
<?= Html::endForm(); ?>
