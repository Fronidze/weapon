<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\OrganizationRequest
 */

use common\modules\directory\models\Galleries;
use common\weapon\helper\Breadcrumbs;
use yii\helpers\Html;

$this->title = 'Данные организации';
Breadcrumbs::instance($this)
    ->setSection($this->title);

?>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-content">
                <h2>Информация об организации</h2>
                <p>тут придумать любой текст что можно было бы помочь пользователю понять что тут делать то</p>
            </div>
        </div>
    </div>


    <div class="col-lg-12">
        <div class="ibox ">
            <div class="ibox-title">
                <h2>Данные организации</h2>
            </div>
            <div class="ibox-content">
                <?= $this->render('_form', ['request' => $request]) ?>
            </div>
        </div>
    </div>

</div>
