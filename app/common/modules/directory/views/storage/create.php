<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\StorageRequest
 */


$this->title = 'Новый склад';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Список складов', ['index'])
    ->setSection($this->title);

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Создание нового складского помещения</h5>
                </div>
                <div class="ibox-content">
                    <?= $this->render('_form', ['request' => $request])?>
                </div>
            </div>
        </div>
    </div>
</div>
