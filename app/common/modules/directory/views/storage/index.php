<?php

/**
 * @var $this \yii\web\View
 * @var $storages \common\modules\directory\models\Storages[]
 */

use yii\helpers\Html,
    yii\helpers\ArrayHelper;

$this->title = 'Список складов';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection($this->title);

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Складские помещения</h5>
                </div>
                <div class="ibox-content">
                    <div class="table__header__wrapper">
                        <?= Html::a('Новый склад', ['create'], ['class' => ['btn', 'btn-primary']]) ?>
                    </div>
                    <table class="table table-hover table-flex">
                        <tr class="row">
                            <th class="col-lg-1 text-center">#</th>
                            <th class="col-lg-4">Название</th>
                            <th class="col-lg-4">Номер</th>
                            <th class="col-lg-3 text-center"></th>
                        </tr>
                        <?php foreach ($storages as $index => $storage): ?>
                            <tr class="row">
                                <td class="col-lg-1 text-center"><?= $index + 1 ?></td>
                                <td class="col-lg-4"><?= ArrayHelper::getValue($storage, 'title') ?></td>
                                <td class="col-lg-4"><?= ArrayHelper::getValue($storage, 'number') ?></td>
                                <td class="col-lg-3 text-right">
                                    <div class="btn-group">

                                        <?php
                                        $icon = Html::tag('i', null, ['class' => ['fa', 'fa-pencil']]);
                                        echo Html::a($icon, ['update', 'id' => $storage->id], ['class' => ['btn', 'btn-sm', 'btn-white']]);
                                        ?>

                                        <?php
                                        $icon = Html::tag('i', null, ['class' => ['fa', 'fa-trash']]);
                                        echo Html::a($icon, ['remove', 'id' => $storage->id], ['class' => ['btn', 'btn-sm', 'btn-danger']]);
                                        ?>

                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
