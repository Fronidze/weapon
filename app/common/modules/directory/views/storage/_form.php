<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\StorageRequest
 */

use yii\helpers\Html;

?>

<?= Html::beginForm(); ?>
<?= Html::activeHiddenInput($request, 'id');?>

<div class="row">
    <div class="form-group col-lg-12">
        <?php
        echo Html::activeLabel($request, 'title');
        echo Html::activeTextInput($request, 'title', ['class' => ['form-control']]);
        echo Html::error($request, 'title', ['class' => ['text-danger']]);
        ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-12">
        <?php
        echo Html::activeLabel($request, 'number');
        echo Html::activeTextInput($request, 'number', ['class' => ['form-control']]);
        echo Html::error($request, 'number', ['class' => ['text-danger']]);
        ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-12 text-right">
        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']])?>
        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>
<?= Html::endForm(); ?>
