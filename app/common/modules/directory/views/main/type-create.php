<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\WeaponViewsRequest
 */

use yii\helpers\Html;

\backend\assets\Select2Assets::register($this);

$this->title = 'Новый тип вооружения';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Справочники', ['main/list'])
    ->setSection('Тип вооружения', ['main/index'])
    ->setSection($this->title);

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Создание нового типа вооружения</h5>
                </div>
                <div class="ibox-content">
                    <?= Html::beginForm(); ?>
                    <div class="row">
                        <div class="form-group col-lg-4">
                            <?php
                            echo Html::activeLabel($request, 'view_id');
                            echo Html::activeDropDownList(
                                $request,
                                'view_id',
                                $request->listViews(),
                                ['class' => 'form-control', 'prompt' => '--выберите вид--', 'data' => ['select' => true]]
                            );
                            echo Html::error($request, 'view_id', ['class' => ['text-danger']]);
                            ?>
                        </div>
                        <div class="form-group col-lg-4">
                            <?php
                            echo Html::activeLabel($request, 'type_id');
                            echo Html::activeDropDownList(
                                $request,
                                'type_id',
                                $request->listTypes(),
                                ['class' => 'form-control', 'prompt' => '--выберите тип--', 'data' => ['select' => true]]
                            );
                            echo Html::error($request, 'type_id', ['class' => ['text-danger']]);
                            ?>
                        </div>
                        <div class="form-group col-lg-4">
                            <?php
                            echo Html::activeLabel($request, 'subtype_id');
                            echo Html::activeDropDownList(
                                $request,
                                'subtype_id',
                                $request->listSubtypes(),
                                ['class' => 'form-control', 'prompt' => '--выберите подтип--', 'data' => ['select' => true]]
                            );
                            echo Html::error($request, 'subtype_id', ['class' => ['text-danger']]);
                            ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-lg-12 text-right">
                            <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']])?>
                            <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
                        </div>
                    </div>
                    <?= Html::endForm(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
