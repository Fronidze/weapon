<?php

/**
 * @var $this \yii\web\View
 * @var $weaponTypes \common\models\WeaponTypes
 */

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Типы вооружения';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Справочники', ['directory/main/index'])
    ->setSection($this->title);

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Типы вооружения</h5>
                </div>
                <div class="ibox-content">
                    <div class="table__header__wrapper">
                        <?= Html::a('Новый тип', ['create'], ['class' => ['btn', 'btn-primary']]) ?>
                    </div>
                    <table class="table table-hover table-flex">
                        <tr class="row">
                            <th class="col-lg-1 text-center">#</th>
                            <th class="col-lg-9">Название</th>
                            <th class="col-lg-2 text-center"></th>
                        </tr>
                        <?php foreach ($weaponTypes as $index => $type): ?>
                            <tr class="row">
                                <td class="col-lg-1 text-center"><?= $index + 1 ?></td>
                                <td class="col-lg-9"><?= ArrayHelper::getValue($type, 'title') ?></td>
                                <td class="col-lg-2 text-right">
                                    <div class="btn-group">

                                        <?php
                                        $icon = Html::tag('i', null, ['class' => ['fa', 'fa-pencil']]);
                                        echo Html::a($icon, ['update', 'id' => $type->id], ['class' => ['btn', 'btn-sm', 'btn-white']]);
                                        ?>

                                        <?php
                                        $icon = Html::tag('i', null, ['class' => ['fa', 'fa-trash']]);
                                        echo Html::a($icon, ['remove', 'id' => $type->id], ['class' => ['btn', 'btn-sm', 'btn-danger']]);
                                        ?>

                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
