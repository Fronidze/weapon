<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\WeaponTypesRequest
 */

use yii\helpers\Html;

$this->title = 'Новый тип вооружения';
\common\weapon\helper\Breadcrumbs::instance($this)
    ->setSection('Справочники', ['directory/main/index'])
    ->setSection('Тип вооружения', ['index'])
    ->setSection($this->title);

?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Создание нового типа вооружения</h5>
                </div>
                <div class="ibox-content">
                    <?= $this->render('_form', ['request' => $request])?>
                </div>
            </div>
        </div>
    </div>
</div>
