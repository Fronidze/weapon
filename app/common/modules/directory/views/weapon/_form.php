<?php

/**
 * @var $this \yii\web\View
 * @var $request \common\modules\directory\request\WeaponRequest
 */

use yii\helpers\Html;

?>

<?= Html::beginForm(options: ['enctype' => 'multipart/form-data']); ?>
<?= Html::activeHiddenInput($request, 'id');?>

 <div class="row">
     <div class="form-group col-lg-6">
         <?php
         echo Html::activeLabel($request, 'view_id');
         echo Html::activeDropDownList(
             $request,
             'view_id',
             $request->listViews(),
             ['class' => 'form-control', 'prompt' => '--выберите вид--', 'data' => ['select' => true]]
         );
         echo Html::error($request, 'view_id', ['class' => ['text-danger']]);
         ?>
     </div>
     <div class="form-group col-lg-6">
         <?php
         echo Html::activeLabel($request, 'type_id');
         echo Html::activeDropDownList(
             $request,
             'type_id',
             $request->listTypes(),
             ['class' => 'form-control', 'prompt' => '--выберите тип--', 'data' => ['select' => true]]
         );
         echo Html::error($request, 'type_id', ['class' => ['text-danger']]);
         ?>
     </div>
 </div>

<div class="row">
    <div class="form-group col-lg-6">
        <?php
        echo Html::activeLabel($request, 'model');
        echo Html::activeTextInput(
            $request,
            'model',
            ['class' => 'form-control']
        );
        echo Html::error($request, 'model', ['class' => ['text-danger']]);
        ?>
    </div>
    <div class="form-group col-lg-6">
        <?php
        echo Html::activeLabel($request, 'caliber_id');
        echo Html::activeDropDownList(
            $request,
            'caliber_id',
            $request->listCaliber(),
            ['class' => 'form-control', 'multiple' => true, 'prompt' => '--выберите калибр--', 'data' => ['select' => true]]
        );
        echo Html::error($request, 'caliber_id', ['class' => ['text-danger']]);
        ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-6">
        <?php
        echo Html::activeLabel($request, 'number');
        echo Html::activeTextInput(
            $request,
            'number',
            ['class' => 'form-control']
        );
        echo Html::error($request, 'number', ['class' => ['text-danger']]);
        ?>
    </div>
    <div class="form-group col-lg-6">
        <?php
        echo Html::activeLabel($request, 'manufacture_at');
        echo Html::activeTextInput(
            $request,
            'manufacture_at',
            ['class' => 'form-control']
        );
        echo Html::error($request, 'manufacture_at', ['class' => ['text-danger']]);
        ?>
    </div>
</div>

<div class="row">
    <div class="col-lg-6 form-group">
        <?php
        echo Html::activeLabel($request, 'purchase_at');
        echo Html::activeTextInput(
            $request,
            'purchase_at',
            ['class' => 'form-control', 'data' => ['datepicker' => true]]
        );
        echo Html::error($request, 'purchase_at', ['class' => ['text-danger']]);
        ?>
    </div>
    <div class="col-lg-6 form-group">
        <?php
        echo Html::activeLabel($request, 'state');
        echo Html::activeDropDownList(
            $request,
            'state',
            $request->listState(),
            ['class' => 'form-control', 'prompt' => '--состояние--', 'data' => ['select' => true]]
        );
        echo Html::error($request, 'state', ['class' => ['text-danger']]);
        ?>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-6">
        <?php
        echo Html::activeLabel($request, 'storage_id');
        echo Html::activeDropDownList(
            $request,
            'storage_id',
            $request->listStorage(),
            ['class' => 'form-control', 'prompt' => '--выберите склад--', 'data' => ['select' => true]]
        );
        echo Html::error($request, 'storage_id', ['class' => ['text-danger']]);
        ?>
    </div>
    <div class="form-group col-lg-6">
        <div class="input-group">
            <div class="custom-file">
                <?= Html::activeFileInput($request, 'licence_id', ['class' => 'custom-file-input'])?>
                <?= Html::activeLabel($request, 'licence_id', ['class' => 'custom-file-label'])?>
            </div>
            <?= Html::error($request, 'licence_id', ['class' => ['text-danger']]);?>
        </div>
    </div>
</div>

<div class="row">
    <div class="form-group col-lg-12 text-right">
        <?= Html::a('Назад', ['index'], ['class' => ['btn', 'btn-default']])?>
        <?= Html::submitButton('Сохранить', ['class' => ['btn', 'btn-primary']]) ?>
    </div>
</div>
<?= Html::endForm(); ?>
