<?php

/**
 * @var $this \yii\web\View
 * @var $weapons \common\models\Weapons[]
 */

use common\weapon\entity\enum\WeaponState;
use common\weapon\helper\Breadcrumbs,
    yii\helpers\Html;
use yii\helpers\ArrayHelper;

$this->title = 'Список вооружения';
Breadcrumbs::instance($this)
    ->setSection($this->title);
?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Типы вооружения</h5>
                </div>
                <div class="ibox-content">
                    <div class="table__header__wrapper">
                        <?= Html::a('Добавить вооружение', ['create'], ['class' => ['btn', 'btn-primary']]) ?>
                    </div>
                    <table class="table table-hover table-flex">
                        <tr class="row">
                            <th class="col-lg-4">Модель</th>
                            <th class="col-lg-1 text-center">Калибр</th>
                            <th class="col-lg-1 text-center">Серия, номер</th>
                            <th class="col-lg-1 text-center">Год</th>
                            <th class="col-lg-1 text-center">Документ</th>
                            <th class="col-lg-1 text-center">Куплен</th>
                            <th class="col-lg-1 text-center">Состояние</th>
                            <th class="col-lg-1 text-center">Место</th>
                            <th class="col-lg-1 text-center"></th>
                        </tr>
                        <?php foreach ($weapons as $weapon): ?>
                            <tr class="row">
                                <td class="col-lg-4">
                                    <?= $weapon->model; ?>
                                    <br>
                                    <small class="text-muted">
                                        <?= ArrayHelper::getValue($weapon, 'view.view.title') ?>
                                        <?= ArrayHelper::getValue($weapon, 'view.type.title') ?>
                                        <?= ArrayHelper::getValue($weapon, 'view.subtype.title') ?>
                                    </small>
                                </td>
                                <td class="col-lg-1 text-center">
                                    <?php
                                    $linkCalibers = ArrayHelper::getValue($weapon, 'weaponCalibers', []);
                                    /** @var \common\models\WeaponCalibers $linkCaliber */
                                    foreach ($linkCalibers as $linkCaliber): ?>
                                        <p style="margin-bottom: 5px;">
                                            <span class="label label-primary"><?= ArrayHelper::getValue($linkCaliber, 'caliber.title') ?></span>
                                        </p>
                                    <?php endforeach; ?>
                                </td>
                                <td class="col-lg-1 text-center"><?= ArrayHelper::getValue($weapon, 'number') ?></td>
                                <td class="col-lg-1 text-center"><?= ArrayHelper::getValue($weapon, 'manufacture_at') ?></td>
                                <td class="col-lg-1 text-center">
                                    <?php
                                    $icon = Html::tag('i', null, ['class' => 'fa fa-download']);
                                    echo Html::a($icon, ['/file/download', 'id' => $weapon->licence_id], [
                                        'class' => 'text-success',
                                        'target' => '_blank',
                                    ]);
                                    ?>
                                </td>
                                <td class="col-lg-1 text-center">
                                    <?= \Yii::$app->formatter->asDate($weapon->purchase_at); ?>
                                </td>
                                <td class="col-lg-1 text-center">
                                    <?= WeaponState::from($weapon->state)->getLabel() ?>
                                </td>
                                <td class="col-lg-1 text-center">
                                    <?= ArrayHelper::getValue($weapon, 'storage.title') ?>
                                    <small class="text-muted">
                                        (<?= ArrayHelper::getValue($weapon, 'storage.number') ?>)
                                    </small>
                                </td>
                                <td class="col-lg-1 text-center">
                                    <?php
                                    $icon = Html::tag('i', null, ['class' => 'fa fa-exchange']);
                                    echo Html::a($icon, null, ['class' => 'btn btn-white btn-bitbucket']);
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>