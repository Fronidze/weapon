<?php

namespace common\modules\directory\dto;

use common\modules\directory\request\OrganizationRequest;
use yii\base\Model;

class OrganizationDto extends Model
{
    public ?string $inn;
    public ?string $kpp;
    public ?string $ogrn;
    public ?string $manager;
    public ?string $shortName;
    public ?string $fullName;
    public ?string $address;
    public ?string $state;
    public ?string $accountId;

    public function setAccountId(string $account_id)
    {
        $this->accountId = $account_id;
    }

    public function isActive(): bool
    {
        return $this->state === 'ACTIVE';
    }

    public function buildRequest(): OrganizationRequest
    {
        $request = new OrganizationRequest([
            'inn' => $this->inn,
            'kpp' => $this->kpp,
            'ogrn' => $this->ogrn,
            'manager' => $this->manager,
            'address' => $this->address,
            'full_name' => $this->fullName,
            'short_name' => $this->shortName,
            'account_id' => $this->accountId,
            'state' => $this->state
        ]);

        if ($request->validate() === false) {
            throw new \RuntimeException("Ошибка валидации OrganizationDto");
        }

        return $request;
    }
}