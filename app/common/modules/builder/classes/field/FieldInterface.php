<?php

namespace common\modules\builder\classes\field;

interface FieldInterface
{
    public function render(): string;
}