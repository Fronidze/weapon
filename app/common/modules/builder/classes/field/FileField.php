<?php

namespace common\modules\builder\classes\field;

use yii\base\Model;
use yii\helpers\Html;

class FileField implements FieldInterface
{
    private string $wrapper = 'div';

    public function __construct(
        protected Model  $request,
        protected string $inputName,
        protected array  $option = []
    )
    {
    }

    public function render(): string
    {
        $elements[] = Html::beginTag('div', ['class' => 'custom-file']);
        $elements[] = Html::activeFileInput($this->request, $this->inputName, ['class' => 'custom-file-input']);
        $elements[] = Html::activeLabel($this->request, $this->inputName, ['class' => 'custom-file-label']);
        $elements[] = Html::endTag('div');

        return Html::tag($this->wrapper, implode('', $elements), ['class' => 'input-group']).
            Html::tag('div', null, ['class' => ['hr-line-dashed']]);
    }
}