<?php

namespace common\modules\builder\classes\field;

use yii\base\Model;
use yii\helpers\Html;

class DateField implements FieldInterface
{
    private string $wrapper = 'div';

    public function __construct(
        protected Model  $request,
        protected string $inputName,
        protected array  $option = []
    )
    {
    }

    public function render(): string
    {
        $elements[] = Html::activeLabel($this->request, $this->inputName);
        $elements[] = Html::activeInput('text', $this->request, $this->inputName, ['class' => 'form-control', 'data' => ['datepicker' => true]]);
        $elements[] = Html::error($this->request, $this->inputName, ['class' => 'form-text']);
        return Html::tag($this->wrapper, implode('', $elements), $this->option)
            . Html::tag('div', null, ['class' => ['hr-line-dashed']]);
    }
}