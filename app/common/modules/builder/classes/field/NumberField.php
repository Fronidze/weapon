<?php

namespace common\modules\builder\classes\field;

use yii\base\Model;
use yii\helpers\Html;

class NumberField implements FieldInterface
{
    private string $wrapper = 'div';

    public function __construct(
        protected Model  $request,
        protected string $inputName,
        protected array  $option = []
    )
    {
    }

    public function render(): string
    {
        $elements[] = Html::activeLabel($this->request, $this->inputName);
        $elements[] = Html::activeInput('number', $this->request, $this->inputName, ['class' => 'form-control']);
        $elements[] = Html::error($this->request, $this->inputName, ['class' => 'form-text']);
        return Html::tag($this->wrapper, implode('', $elements), $this->option)
            . Html::tag('div', null, ['class' => ['hr-line-dashed']]);
    }
}