<?php

namespace common\jobs\account;

use yii\base\BaseObject;
use yii\queue\JobInterface;
use yii\queue\Queue;

class RoleAssignJob extends BaseObject implements JobInterface
{
    public string|null $email = null;
    public string|null $role = null;

    public function execute($queue)
    {
        \Yii::$app->mailer->compose('account/role-assign', [
            'email' => $this->email,
            'role' => $this->role
        ])
            ->setSubject('Изменение у пользователя')
            ->setTo($this->email)
            ->send();
    }
}