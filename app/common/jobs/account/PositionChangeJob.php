<?php

namespace common\jobs\account;

use yii\base\BaseObject;
use yii\queue\JobInterface;

class PositionChangeJob extends BaseObject implements JobInterface
{
    public string|null $email = null;
    public string|null $position = null;
    public function execute($queue)
    {
        \Yii::$app->mailer->compose('account/position-change', [
            'email' => $this->email,
            'position' => $this->position
        ])
            ->setSubject('Изменение у пользователя')
            ->setTo($this->email)
            ->send();
    }
}