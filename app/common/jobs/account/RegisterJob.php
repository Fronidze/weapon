<?php

namespace common\jobs\account;

use yii\base\BaseObject;
use yii\queue\JobInterface;

class RegisterJob extends BaseObject implements JobInterface
{

    public string|null $login = null;
    public string|null $password = null;

    public function execute($queue)
    {
        \Yii::$app->mailer->compose('account/register', [
            'login' => $this->login,
            'password' => $this->password
        ])
            ->setSubject('Регистрация на сайте')
            ->setTo($this->login)
            ->send();
    }
}