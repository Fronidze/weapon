<?php

namespace common\jobs\orders;

use common\models\OrganizationShootingOrders;
use common\modules\document\factory\OrganizationShootingFactory;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class OrganizationShootingJobs extends BaseObject implements JobInterface
{
    public OrganizationShootingOrders|null $order = null;

    public function execute($queue)
    {
        $factory = new OrganizationShootingFactory($this->order);
        $datetime = new \DateTime($this->order->created_at);
        $factory->generate($datetime->format('d.m.Y'));
    }
}