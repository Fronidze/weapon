<?php

namespace common\models;

use common\modules\directory\models\Storages;
use Yii;

/**
 * This is the model class for table "weapons".
 *
 * @property string $id
 * @property string $view_id
 * @property string $type_id
 * @property string $model
 * @property string $number
 * @property int $manufacture_at
 * @property string $licence_id
 * @property string $purchase_at
 * @property string $state
 * @property string $storage_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Files $licence
 * @property Storages $storage
 * @property WeaponTypes $type
 * @property WeaponViews $view
 * @property WeaponCalibers[] $weaponCalibers
 */
class Weapons extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weapons';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'view_id', 'type_id', 'model', 'number', 'manufacture_at', 'licence_id', 'purchase_at', 'storage_id'], 'required'],
            [['id', 'view_id', 'type_id', 'licence_id', 'storage_id'], 'string'],
            [['manufacture_at'], 'default', 'value' => null],
            [['manufacture_at'], 'integer'],
            [['purchase_at', 'created_at', 'updated_at'], 'safe'],
            [['model', 'number', 'state'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['licence_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::className(), 'targetAttribute' => ['licence_id' => 'id']],
            [['storage_id'], 'exist', 'skipOnError' => true, 'targetClass' => Storages::className(), 'targetAttribute' => ['storage_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => WeaponTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['view_id'], 'exist', 'skipOnError' => true, 'targetClass' => WeaponViews::className(), 'targetAttribute' => ['view_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'view_id' => Yii::t('app', 'View ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'model' => Yii::t('app', 'Model'),
            'number' => Yii::t('app', 'Number'),
            'manufacture_at' => Yii::t('app', 'Manufacture At'),
            'licence_id' => Yii::t('app', 'Licence ID'),
            'purchase_at' => Yii::t('app', 'Purchase At'),
            'state' => Yii::t('app', 'State'),
            'storage_id' => Yii::t('app', 'Storage ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Licence]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLicence()
    {
        return $this->hasOne(Files::className(), ['id' => 'licence_id']);
    }

    /**
     * Gets query for [[Storage]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStorage()
    {
        return $this->hasOne(Storages::className(), ['id' => 'storage_id']);
    }

    /**
     * Gets query for [[Type]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(WeaponTypes::className(), ['id' => 'type_id']);
    }

    /**
     * Gets query for [[View]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getView()
    {
        return $this->hasOne(WeaponViews::className(), ['id' => 'view_id']);
    }

    /**
     * Gets query for [[WeaponCalibers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWeaponCalibers()
    {
        return $this->hasMany(WeaponCalibers::className(), ['weapon_id' => 'id']);
    }
}
