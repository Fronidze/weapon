<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "weapon_calibers".
 *
 * @property string $weapon_id
 * @property string $caliber_id
 *
 * @property Calibers $caliber
 * @property Weapons $weapon
 */
class WeaponCalibers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weapon_calibers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['weapon_id', 'caliber_id'], 'required'],
            [['weapon_id', 'caliber_id'], 'string'],
            [['caliber_id'], 'exist', 'skipOnError' => true, 'targetClass' => Calibers::className(), 'targetAttribute' => ['caliber_id' => 'id']],
            [['weapon_id'], 'exist', 'skipOnError' => true, 'targetClass' => Weapons::className(), 'targetAttribute' => ['weapon_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'weapon_id' => Yii::t('app', 'Weapon ID'),
            'caliber_id' => Yii::t('app', 'Caliber ID'),
        ];
    }

    /**
     * Gets query for [[Caliber]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCaliber()
    {
        return $this->hasOne(Calibers::className(), ['id' => 'caliber_id']);
    }

    /**
     * Gets query for [[Weapon]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getWeapon()
    {
        return $this->hasOne(Weapons::className(), ['id' => 'weapon_id']);
    }
}
