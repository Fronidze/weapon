<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property string $id
 * @property string $title
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $material_responsible_id
 * @property string $accountant_id
 * @property string $director_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Employee $accountant
 * @property Employee $director
 * @property Employee $materialResponsible
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'title', 'address', 'email', 'phone', 'material_responsible_id', 'accountant_id', 'director_id'], 'required'],
            [['id', 'material_responsible_id', 'accountant_id', 'director_id'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title', 'address', 'email', 'phone'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['material_responsible_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['material_responsible_id' => 'id']],
            [['accountant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['accountant_id' => 'id']],
            [['director_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['director_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'address' => Yii::t('app', 'Address'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'material_responsible_id' => Yii::t('app', 'Material Responsible ID'),
            'accountant_id' => Yii::t('app', 'Accountant ID'),
            'director_id' => Yii::t('app', 'Director ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Accountant]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAccountant()
    {
        return $this->hasOne(Employee::className(), ['id' => 'accountant_id']);
    }

    /**
     * Gets query for [[Director]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirector()
    {
        return $this->hasOne(Employee::className(), ['id' => 'director_id']);
    }

    /**
     * Gets query for [[MaterialResponsible]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaterialResponsible()
    {
        return $this->hasOne(Employee::className(), ['id' => 'material_responsible_id']);
    }
}
