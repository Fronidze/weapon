<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "employee_history".
 *
 * @property string $id
 * @property string $action
 * @property string|null $source
 * @property string|null $target
 * @property string|null $description
 * @property string|null $happened_at
 */
class EmployeeHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'action'], 'required'],
            [['id', 'source', 'target', 'description'], 'string'],
            [['happened_at'], 'safe'],
            [['action'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'action' => Yii::t('app', 'Action'),
            'source' => Yii::t('app', 'Source'),
            'target' => Yii::t('app', 'Target'),
            'description' => Yii::t('app', 'Description'),
            'happened_at' => Yii::t('app', 'Happened At'),
        ];
    }
}
