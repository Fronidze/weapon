<?php

namespace common\models;

use common\modules\directory\models\DirectoryWeaponSubtypes;
use common\modules\directory\models\DirectoryWeaponTypes;
use common\modules\directory\models\DirectoryWeaponViews;
use Yii;

/**
 * This is the model class for table "weapon_views".
 *
 * @property string $id
 * @property string $view_id
 * @property string|null $type_id
 * @property string $subtype_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property DirectoryWeaponSubtypes $subtype
 * @property DirectoryWeaponTypes $type
 * @property DirectoryWeaponViews $view
 */
class WeaponViews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'weapon_views';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'view_id', 'subtype_id'], 'required'],
            [['id', 'view_id', 'type_id', 'subtype_id'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['id'], 'unique'],
            [['subtype_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirectoryWeaponSubtypes::className(), 'targetAttribute' => ['subtype_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirectoryWeaponTypes::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['view_id'], 'exist', 'skipOnError' => true, 'targetClass' => DirectoryWeaponViews::className(), 'targetAttribute' => ['view_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'view_id' => Yii::t('app', 'View ID'),
            'type_id' => Yii::t('app', 'Type ID'),
            'subtype_id' => Yii::t('app', 'Subtype ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Subtype]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSubtype()
    {
        return $this->hasOne(DirectoryWeaponSubtypes::className(), ['id' => 'subtype_id']);
    }

    /**
     * Gets query for [[Type]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(DirectoryWeaponTypes::className(), ['id' => 'type_id']);
    }

    /**
     * Gets query for [[View]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getView()
    {
        return $this->hasOne(DirectoryWeaponViews::className(), ['id' => 'view_id']);
    }
}
