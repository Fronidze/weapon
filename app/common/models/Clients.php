<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "clients".
 *
 * @property string $id
 * @property string $lastName
 * @property string $firstName
 * @property string|null $middleName
 * @property string $photo_id
 * @property string $gender
 * @property string|null $birthday_at
 * @property string $email
 * @property string $phone
 * @property string|null $notice
 * @property bool|null $isLefty
 * @property string $passport_id
 * @property string $shooterExperience
 * @property string $declaration_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Files $declaration
 * @property Files $passport
 * @property Files $photo
 */
class Clients extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'lastName', 'firstName', 'photo_id', 'gender', 'email', 'phone', 'passport_id', 'shooterExperience', 'declaration_id'], 'required'],
            [['id', 'photo_id', 'notice', 'passport_id', 'declaration_id'], 'string'],
            [['birthday_at', 'created_at', 'updated_at'], 'safe'],
            [['isLefty'], 'boolean'],
            [['lastName', 'firstName', 'middleName', 'gender', 'email', 'phone', 'shooterExperience'], 'string', 'max' => 255],
            [['id'], 'unique'],
            [['photo_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::className(), 'targetAttribute' => ['photo_id' => 'id']],
            [['passport_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::className(), 'targetAttribute' => ['passport_id' => 'id']],
            [['declaration_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::className(), 'targetAttribute' => ['declaration_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'lastName' => Yii::t('app', 'Last Name'),
            'firstName' => Yii::t('app', 'First Name'),
            'middleName' => Yii::t('app', 'Middle Name'),
            'photo_id' => Yii::t('app', 'Photo ID'),
            'gender' => Yii::t('app', 'Gender'),
            'birthday_at' => Yii::t('app', 'Birthday At'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),
            'notice' => Yii::t('app', 'Notice'),
            'isLefty' => Yii::t('app', 'Is Lefty'),
            'passport_id' => Yii::t('app', 'Passport ID'),
            'shooterExperience' => Yii::t('app', 'Shooter Experience'),
            'declaration_id' => Yii::t('app', 'Declaration ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Declaration]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDeclaration()
    {
        return $this->hasOne(Files::className(), ['id' => 'declaration_id']);
    }

    /**
     * Gets query for [[Passport]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPassport()
    {
        return $this->hasOne(Files::className(), ['id' => 'passport_id']);
    }

    /**
     * Gets query for [[Photo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPhoto()
    {
        return $this->hasOne(Files::className(), ['id' => 'photo_id']);
    }
}
