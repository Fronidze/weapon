<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "position_roles".
 *
 * @property string $positions_id
 * @property string $items_id
 *
 * @property AuthItem $items
 * @property Positions $positions
 */
class PositionRoles extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'position_roles';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['positions_id', 'items_id'], 'required'],
            [['positions_id'], 'string'],
            [['items_id'], 'string', 'max' => 65],
            [['items_id'], 'exist', 'skipOnError' => true, 'targetClass' => AuthItem::className(), 'targetAttribute' => ['items_id' => 'name']],
            [['positions_id'], 'exist', 'skipOnError' => true, 'targetClass' => Positions::className(), 'targetAttribute' => ['positions_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'positions_id' => Yii::t('app', 'Positions ID'),
            'items_id' => Yii::t('app', 'Items ID'),
        ];
    }

    /**
     * Gets query for [[Items]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasOne(AuthItem::className(), ['name' => 'items_id']);
    }

    /**
     * Gets query for [[Positions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPositions()
    {
        return $this->hasOne(Positions::className(), ['id' => 'positions_id']);
    }
}
