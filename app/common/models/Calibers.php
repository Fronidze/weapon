<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "calibers".
 *
 * @property string $id
 * @property string $title
 * @property string|null $alternative_title
 */
class Calibers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'calibers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id'], 'string'],
            [['title', 'alternative_title'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'alternative_title' => Yii::t('app', 'Alternative Title'),
        ];
    }
}
