<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "organization".
 *
 * @property string $id
 * @property string $short_name
 * @property string $full_name
 * @property string $address
 * @property string $manager
 * @property string $inn
 * @property string $kpp
 * @property string $ogrn
 * @property string $state
 * @property string $account_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Accounts $account
 */
class Organization extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organization';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'short_name', 'full_name', 'address', 'manager', 'inn', 'kpp', 'ogrn', 'state', 'account_id'], 'required'],
            [['id', 'account_id'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['short_name', 'full_name', 'address', 'manager', 'inn', 'kpp', 'ogrn', 'state'], 'string', 'max' => 255],
            [['kpp'], 'unique'],
            [['ogrn'], 'unique'],
            [['inn'], 'unique'],
            [['id'], 'unique'],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => Accounts::className(), 'targetAttribute' => ['account_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'short_name' => Yii::t('app', 'Short Name'),
            'full_name' => Yii::t('app', 'Full Name'),
            'address' => Yii::t('app', 'Address'),
            'manager' => Yii::t('app', 'Manager'),
            'inn' => Yii::t('app', 'Inn'),
            'kpp' => Yii::t('app', 'Kpp'),
            'ogrn' => Yii::t('app', 'Ogrn'),
            'state' => Yii::t('app', 'State'),
            'account_id' => Yii::t('app', 'Account ID'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[Account]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(Accounts::className(), ['id' => 'account_id']);
    }
}
