<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "accounts".
 *
 * @property string $id
 * @property string $email
 * @property string $status
 * @property string $password
 * @property string $authentication_key
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Employee $employee
 */
class Accounts extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName(): string
    {
        return 'accounts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules(): array
    {
        return [
            [['id', 'email', 'password', 'authentication_key'], 'required'],
            [['id'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['email', 'status', 'password', 'authentication_key'], 'string', 'max' => 255],
            [['email'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels(): array
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'email' => Yii::t('app', 'Email'),
            'status' => Yii::t('app', 'Status'),
            'password' => Yii::t('app', 'Password'),
            'authentication_key' => Yii::t('app', 'Authentication Key'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['account_id' => 'id']);
    }

    public static function findIdentity($id)
    {
        return static::find()
            ->where(['=', 'id', $id])
            ->one();
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAuthKey(): string
    {
        return $this->authentication_key;
    }

    public function validateAuthKey($authKey): bool
    {
        return $this->getAuthKey() === $authKey;
    }
}
