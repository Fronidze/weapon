<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "organization_shooting_orders".
 *
 * @property string $id
 * @property string $organization_title
 * @property string $head_employee_id
 * @property string $director_employee_id
 * @property string|null $file_id
 * @property string|null $created_at
 *
 * @property Employee $directorEmployee
 * @property Files $file
 * @property Employee $headEmployee
 */
class OrganizationShootingOrders extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'organization_shooting_orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'organization_title', 'head_employee_id', 'director_employee_id'], 'required'],
            [['id', 'head_employee_id', 'director_employee_id', 'file_id'], 'string'],
            [['created_at'], 'safe'],
            [['organization_title'], 'string', 'max' => 255],
            [['created_at'], 'unique'],
            [['id'], 'unique'],
            [['head_employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['head_employee_id' => 'id']],
            [['director_employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['director_employee_id' => 'id']],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => Files::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'organization_title' => Yii::t('app', 'Organization Title'),
            'head_employee_id' => Yii::t('app', 'Head Employee ID'),
            'director_employee_id' => Yii::t('app', 'Director Employee ID'),
            'file_id' => Yii::t('app', 'File ID'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    /**
     * Gets query for [[DirectorEmployee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDirectorEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'director_employee_id']);
    }

    /**
     * Gets query for [[File]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(Files::className(), ['id' => 'file_id']);
    }

    /**
     * Gets query for [[HeadEmployee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getHeadEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'head_employee_id']);
    }
}
