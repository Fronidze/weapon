<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "positions".
 *
 * @property string $id
 * @property string $title
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property PositionRoles[] $positionRoles
 */
class Positions extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'positions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'title'], 'required'],
            [['id'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * Gets query for [[PositionRoles]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPositionRoles()
    {
        return $this->hasMany(PositionRoles::className(), ['positions_id' => 'id']);
    }
}
