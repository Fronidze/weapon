<?php

namespace common\attributes;

interface GenerateValueInterface
{
    public function getValue(): mixed;
}