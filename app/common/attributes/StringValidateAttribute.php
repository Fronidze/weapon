<?php

namespace common\attributes;

#[\Attribute]
class StringValidateAttribute implements ValidateAttributeInterface
{
    public function __construct()
    {
    }

    public function getValidatorClass(): string
    {
        return 'string';
    }

    public function getValidatorParams(): array
    {
        return [];
    }

}