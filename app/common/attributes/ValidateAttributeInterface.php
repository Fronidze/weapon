<?php

namespace common\attributes;

interface ValidateAttributeInterface
{
    public function getValidatorClass(): string;
    public function getValidatorParams(): array;
}