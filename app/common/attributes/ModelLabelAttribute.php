<?php

namespace common\attributes;

#[\Attribute]
class ModelLabelAttribute implements LabelInterface
{
    public function __construct(
        private string $placeholder
    ){}

    public function getAttributeLabel(): string
    {
        return \Yii::t('app', $this->placeholder);
    }
}