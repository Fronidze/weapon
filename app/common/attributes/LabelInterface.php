<?php

namespace common\attributes;

interface LabelInterface
{
    public function getAttributeLabel(): string;
}