<?php

namespace common\attributes;

#[\Attribute]
class NumberValidateAttribute implements ValidateAttributeInterface
{
    public function __construct()
    {
    }

    public function getValidatorClass(): string
    {
        return 'number';
    }

    public function getValidatorParams(): array
    {
        return [];
    }

}