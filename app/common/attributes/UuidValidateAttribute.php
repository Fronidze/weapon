<?php

namespace common\attributes;

use Ramsey\Uuid\Uuid;

#[\Attribute]
class UuidValidateAttribute implements ValidateAttributeInterface, GenerateValueInterface
{
    private string $value;
    public function __construct()
    {
        $this->value = Uuid::uuid4()->toString();
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function getValidatorClass(): string
    {
        return 'default';
    }

    public function getValidatorParams(): array
    {
        return [];
    }
}