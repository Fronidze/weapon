<?php

namespace common\attributes;

#[\Attribute]
class RequiredValidateAttribute implements ValidateAttributeInterface
{
    public function __construct()
    {
    }

    public function getValidatorClass(): string
    {
        return 'required';
    }

    public function getValidatorParams(): array
    {
        return [];
    }
}