<?php

namespace common\attributes;

use Ramsey\Uuid\Uuid;

#[\Attribute]
class DefaultValidateAttribute implements ValidateAttributeInterface, GenerateValueInterface
{
    public function __construct(
        private mixed $value
    )
    {}

    public function getValue(): mixed
    {
        return $this->value;
    }

    public function getValidatorClass(): string
    {
        return 'default';
    }

    public function getValidatorParams(): array
    {
        return [];
    }
}