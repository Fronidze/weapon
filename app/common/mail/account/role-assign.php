<?php
/**
 * @var $this \yii\web\View
 * @var $message \yii\swiftmailer\Message
 * @var $email string
 * @var $role string
 */

use yii\helpers\Html;
?>

<table>

    <tr>
        <td style="font-size: 20px; font-weight: 400; padding-bottom: 20px;">
            Изменение у пользователя
        </td>
    </tr>

    <tr>
        <td>
            У пользователя <b><?= $email?></b>, была измеена роль в системе. <br>
            Новая роль: <b><?= $role?></b>
        </td>
    </tr>
</table>

