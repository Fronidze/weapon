<?php
/**
 * @var $this \yii\web\View
 * @var $message \yii\swiftmailer\Message
 * @var $email string
 * @var $position string
 */

use yii\helpers\Html;

?>

<table>

    <tr>
        <td style="font-size: 20px; font-weight: 400; padding-bottom: 20px;">
            Изменение у пользователя
        </td>
    </tr>

    <tr>
        <td>
            У пользователя <b><?= $email ?></b>, измеена должность. <br>
            Новая должность: <b><?= $position ?></b>
        </td>
    </tr>
</table>

