<?php
/**
 * @var $this \yii\web\View
 * @var $message \yii\swiftmailer\Message
 * @var $login string
 * @var $password string|null
 */

use yii\helpers\Html;
?>

<table>

    <tr>
        <td style="font-size: 20px; font-weight: 400; padding-bottom: 20px;">
            Регистрация на сайте
        </td>
    </tr>

    <tr>
        <td>
            Вы успешно создали учетную запись на сайте <a href="https://smarttir.ru" target="_blank">smarttir.ru</a>,
            учетная запись сразну активна, вы можете пользоваться сразу сервисом.
        </td>
    </tr>
    <tr>
        <td>
            <br>
            Данные для входа в систему: <br>
            <table>
                <tr>
                    <td style="padding: 2px;"><b>Логин: </b></td>
                    <td style="padding: 2px;"><?= Html::encode($login) ?></td>
                </tr>
                <?php if ($password !== null): ?>
                    <tr>
                        <td style="padding: 2px;"><b>Пароль: </b></td>
                        <td style="padding: 2px;"><?= Html::encode($password) ?></td>
                    </tr>
                <?php endif; ?>
            </table>
        </td>
    </tr>
</table>

