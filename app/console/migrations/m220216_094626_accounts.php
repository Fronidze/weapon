<?php

use yii\db\Migration;

/**
 * Class m220216_094626_accounts
 */
class m220216_094626_accounts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('accounts', [
            'id' => 'uuid not null',
            'email' => $this->string()->unique()->notNull(),
            'status' => "varchar(255) check (status in ('create', 'active', 'block', 'remove')) not null default 'create'",
            'password' => $this->string(255)->notNull(),
            'authentication_key' => $this->string(255)->notNull(),
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);
        $this->addPrimaryKey('pkAccounts', 'accounts', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('accounts');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220216_094626_accounts cannot be reverted.\n";

        return false;
    }
    */
}
