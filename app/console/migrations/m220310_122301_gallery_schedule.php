<?php

use yii\db\Migration;

/**
 * Class m220310_122301_gallery_schedule
 */
class m220310_122301_gallery_schedule extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('gallery_schedule', [
            'id' => 'uuid not null',
            'gallery_id' => 'uuid not null',
            'event_id' => 'uuid default null',
            'status' => "varchar(255) check (status in ('free', 'booked', 'busy')) not null default 'free'",
            'event_at' => 'date not null',
            'begin_at' => 'time with time zone not null',
            'end_at' => 'time with time zone not null',
        ]);
        $this->addPrimaryKey('pkGallerySchedule', 'gallery_schedule', 'id');
        $this->addForeignKey('fkGalleryScheduleGallery',
        'gallery_schedule', 'gallery_id',
        'galleries', 'id',
        'cascade', 'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkGalleryScheduleGallery', 'gallery_schedule');
        $this->dropTable('gallery_schedule');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220310_122301_gallery_schedule cannot be reverted.\n";

        return false;
    }
    */
}
