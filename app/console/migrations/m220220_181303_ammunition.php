<?php

use yii\db\Migration;

/**
 * Class m220220_181303_ammunition
 */
class m220220_181303_ammunition extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('ammunition', [
            'id' => 'uuid not null',
            'caliber_id' => 'uuid not null',
            'number' => $this->string()->notNull(),
            'count' => $this->integer()->notNull(),
            'storage_id' => 'uuid not null',
            'licence_id' => 'uuid not null',
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);

        $this->addPrimaryKey('pkAmmunition', 'ammunition', 'id');
        $this->addForeignKey('fkAmmunitionCaliber',
        'ammunition', 'caliber_id',
        'calibers', 'id',
        'cascade', 'cascade'
        );
        $this->addForeignKey('fkAmmunitionStorage',
            'ammunition', 'storage_id',
            'storages', 'id',
            'cascade', 'cascade'
        );
        $this->addForeignKey('fkAmmunitionFiles',
        'ammunition', 'licence_id',
        'files', 'id',
            'cascade', 'cascade'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkAmmunitionCaliber', 'ammunition');
        $this->dropForeignKey('fkAmmunitionStorage', 'ammunition');
        $this->dropForeignKey('fkAmmunitionFiles', 'ammunition');

        $this->dropTable('ammunition');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220220_181303_ammunition cannot be reverted.\n";

        return false;
    }
    */
}
