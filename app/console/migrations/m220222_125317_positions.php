<?php

use yii\db\Migration;

/**
 * Class m220222_125317_positions
 */
class m220222_125317_positions extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('positions', [
            'id' => 'uuid not null',
            'title' => $this->string()->notNull(),
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);
        $this->addPrimaryKey('pkPositions', 'positions', 'id');

        $this->createTable('position_roles', [
            'positions_id' => 'uuid not null',
            'items_id' => 'varchar(65) not null',
        ]);
        $this->addForeignKey('fkPositionsRolesPosition',
            'position_roles', 'positions_id',
            'positions','id',
            'cascade', 'cascade'
        );
        $this->addForeignKey('fkPositionsRolesItem',
            'position_roles', 'items_id',
            'auth_item','name',
            'cascade', 'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkPositionsRolesPosition', 'position_roles');
        $this->dropForeignKey('fkPositionsRolesItem', 'position_roles');

        $this->dropTable('position_roles');
        $this->dropTable('positions');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220222_125317_positions cannot be reverted.\n";

        return false;
    }
    */
}
