<?php

use yii\db\Migration;

/**
 * Class m220218_110014_storage
 */
class m220218_110014_storage extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('storages', [
            'id' => 'uuid not null',
            'title' => $this->string()->notNull(),
            'number' => $this->string()->notNull(),
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);
        $this->addPrimaryKey('pkStorage', 'storages', 'id');
        $this->addForeignKey('fkWeaponStorage',
            'weapons', 'storage_id',
            'storages', 'id',
            'cascade', 'cascade',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkWeaponStorage', 'weapons');
        $this->dropTable('storages');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220218_110014_storage cannot be reverted.\n";

        return false;
    }
    */
}
