<?php

use yii\db\Migration;

/**
 * Class m220301_094336_employee
 */
class m220301_094336_employee extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('employee', [
            'id' => 'uuid not null',
            'account_id' => 'uuid default null',
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'middle_name' => $this->string()->notNull(),
            'position_id' => 'uuid default null',
            'email' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'employment_at' => 'timestamp default now()',
            'status' => "varchar(255) check (status in ('draft', 'hired', 'in_holiday', 'fired')) not null default 'draft'",
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);

        $this->addPrimaryKey('pkEmployeeId', 'employee', 'id');
        $this->addForeignKey('fkEmployeeAccount',
            'employee', 'account_id',
            'accounts', 'id',
            'cascade', 'cascade',
        );
        $this->addForeignKey('fkEmployeePosition',
            'employee', 'position_id',
            'positions', 'id',
            'cascade', 'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkEmployeePosition', 'employee');
        $this->dropForeignKey('fkEmployeeAccount', 'employee');
        $this->dropTable('employee');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220301_094336_employee cannot be reverted.\n";

        return false;
    }
    */
}
