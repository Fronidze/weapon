<?php

use yii\db\Migration;

/**
 * Class m220218_091615_weapon_types
 */
class m220218_091615_weapon_types extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('weapon_types', [
            'id' => 'uuid not null',
            'title' => $this->string()->notNull(),
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);
        $this->addPrimaryKey('pkWeaponTypes', 'weapon_types', 'id');
        $this->addForeignKey('fkWeaponType',
            'weapons', 'type_id',
            'weapon_types', 'id',
            'cascade', 'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkWeaponType', 'weapons');
        $this->dropTable('weapon_types');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220218_091615_weapon_types cannot be reverted.\n";

        return false;
    }
    */
}
