<?php

use yii\db\Migration;

/**
 * Class m220309_090909_organization_shooting_orders
 */
class m220309_090909_organization_shooting_orders extends Migration
{

    public function safeUp()
    {
        $this->createTable('organization_shooting_orders', [
            'id' => 'uuid not null',
            'organization_title' => $this->string()->notNull(),
            'head_employee_id' => 'uuid not null',
            'director_employee_id' => 'uuid not null',
            'file_id' => 'uuid default null',
            'created_at' => 'timestamp with time zone default now()'
        ]);

        $this->addPrimaryKey('pkOsoHead', 'organization_shooting_orders', 'id');
        $this->createIndex('uqOsoCreatedAt', 'organization_shooting_orders', 'created_at', true);
        $this->addForeignKey(
            'fkOsoHead',
            'organization_shooting_orders', 'head_employee_id',
            'employee', 'id',
            'set null', 'cascade'
        );

        $this->addForeignKey(
            'fkOsoDirector',
            'organization_shooting_orders', 'director_employee_id',
            'employee', 'id',
            'set null', 'cascade'
        );

        $this->addForeignKey('fkOsoFiles',
        'organization_shooting_orders', 'file_id',
            'files', 'id',
            'set null', 'cascade'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fkOsoFiles', 'organization_shooting_orders');
        $this->dropForeignKey('fkOsoHead', 'organization_shooting_orders');
        $this->dropForeignKey('fkOsoDirector', 'organization_shooting_orders');
        $this->dropTable('organization_shooting_orders');
    }
}
