<?php

use yii\db\Migration;

/**
 * Class m220216_203329_weapons
 */
class m220216_203329_weapons extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('calibers', [
            'id' => 'uuid not null',
            'title' => $this->string()->notNull(),
            'alternative_title' => $this->string()->defaultValue(null),
        ]);
        $this->addPrimaryKey('pkCalibers', 'calibers', 'id');

        $this->createTable('directory_weapon_views', [
            'id' => 'uuid not null',
            'title' => $this->string()->notNull(),
        ]);
        $this->addPrimaryKey('pkDirectoryWeaponViews', 'directory_weapon_views', 'id');

        $this->createTable('directory_weapon_types', [
            'id' => 'uuid not null',
            'title' => $this->string()->notNull(),
        ]);
        $this->addPrimaryKey('pkDirectoryWeaponTypes', 'directory_weapon_types', 'id');

        $this->createTable('directory_weapon_subtypes', [
            'id' => 'uuid not null',
            'title' => $this->string()->notNull(),
        ]);
        $this->addPrimaryKey('pkDirectoryWeaponSubtypes', 'directory_weapon_subtypes', 'id');

        $this->createTable('weapon_views', [
            'id' => 'uuid not null',
            'view_id' => 'uuid not null',
            'type_id' => 'uuid default null',
            'subtype_id' => 'uuid not null',
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);
        $this->addPrimaryKey('pkWeaponViews', 'weapon_views', 'id');
        $this->addForeignKey('fkDwvWv', 'weapon_views', 'view_id', 'directory_weapon_views', 'id', 'cascade', 'cascade');
        $this->addForeignKey('fkDwtWv', 'weapon_views', 'type_id', 'directory_weapon_types', 'id','cascade', 'cascade');
        $this->addForeignKey('fkDwsWv', 'weapon_views', 'subtype_id', 'directory_weapon_subtypes', 'id','cascade', 'cascade');

        // TODO докинуть внешних ключей
        $this->createTable('weapons', [
            'id' => 'uuid not null',
            'view_id' => 'uuid not null',
            'type_id' => 'uuid not null',
            'model' => $this->string()->notNull(),
            'number' => $this->string()->notNull(),
            'manufacture_at' => $this->integer(4)->notNull(),
            'licence_id' => 'uuid not null',
            'purchase_at' => 'timestamp with time zone not null',
            'state' => "varchar(255) check (state in ('bought', 'storage', 'gallery', 'repair', 'disposal')) not null default 'bought'",
            'storage_id' => 'uuid not null',
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);
        $this->addPrimaryKey('pkWeapons', 'weapons', 'id');
        $this->addForeignKey('fkWeaponView',
            'weapons', 'view_id',
            'weapon_views', 'id',
            'cascade', 'cascade'
        );

        $this->createTable('weapon_calibers', [
            'weapon_id' => 'uuid not null',
            'caliber_id' => 'uuid not null',
        ]);
        $this->addForeignKey('fkWeaponCalibersWeapon',
            'weapon_calibers', 'weapon_id',
            'weapons', 'id',
            'cascade', 'cascade',
        );
        $this->addForeignKey('fkWeaponCalibersCaliber',
            'weapon_calibers', 'caliber_id',
            'calibers', 'id',
            'cascade', 'cascade',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkWeaponView', 'weapons');
        $this->dropForeignKey('fkWeaponCalibersWeapon', 'weapon_calibers');
        $this->dropForeignKey('fkWeaponCalibersCaliber', 'weapon_calibers');

        $this->dropTable('calibers');
        $this->dropTable('weapon_views');
        $this->dropTable('directory_weapon_subtypes');
        $this->dropTable('directory_weapon_types');
        $this->dropTable('directory_weapon_views');
        $this->dropTable('weapons');
        $this->dropTable('weapon_calibers');
    }
}
