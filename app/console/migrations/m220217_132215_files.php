<?php

use yii\db\Migration;

/**
 * Class m220217_132215_files
 */
class m220217_132215_files extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('files', [
            'id' => 'uuid not null',
            'name' => $this->string()->notNull(),
            'path' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
            'size' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('pkFiles', 'files', 'id');
        $this->addForeignKey('fkWeaponLicence',
            'weapons', 'licence_id',
            'files', 'id',
            'cascade', 'cascade',
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkWeaponLicence', 'weapons');
        $this->dropTable('files');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220217_132215_files cannot be reverted.\n";

        return false;
    }
    */
}
