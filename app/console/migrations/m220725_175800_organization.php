<?php

use yii\db\Migration;

/**
 * Class m220725_175800_organization
 */
class m220725_175800_organization extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('organization', [
            'id' => 'uuid not null',
            'short_name' => 'varchar(255) not null',
            'full_name' => 'varchar(255) not null',
            'address' => 'varchar(255) not null',
            'manager' => 'varchar(255) not null',
            'inn' => 'varchar(255) not null',
            'kpp' => 'varchar(255) not null',
            'ogrn' => 'varchar(255) not null',
            'state' => 'varchar(255) not null',
            'account_id' => 'uuid not null',
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);
        $this->addPrimaryKey('pkOrganization', 'organization', 'id');
        $this->createIndex('ixOrganizationInn', 'organization', 'inn',true);
        $this->createIndex('ixOraganizationKpp', 'organization', 'kpp',true);
        $this->createIndex('ixOraganizationOgrn', 'organization', 'ogrn',true);

        $this->addForeignKey('fkAccountOrganization',
        'organization', 'account_id',
            'accounts', 'id',
            'set null', 'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkAccountOrganization', 'organization');
        $this->dropTable('organization');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220725_175800_organization cannot be reverted.\n";

        return false;
    }
    */
}
