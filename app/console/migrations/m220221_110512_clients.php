<?php

use yii\db\Migration;

/**
 * Class m220221_110512_clients
 */
class m220221_110512_clients extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('clients', [
            'id' => 'uuid not null',
            'lastName' => $this->string()->notNull(),
            'firstName' => $this->string()->notNull(),
            'middleName' => $this->string()->defaultValue(null),
            'photo_id' => 'uuid not null',
            'gender' => $this->string()->notNull(),
            'birthday_at' => 'timestamp default now()',
            'email' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'notice' => $this->text()->defaultValue(null),
            'isLefty' => $this->boolean()->defaultValue(false),
            'passport_id' => 'uuid not null',
            'shooterExperience' => $this->string()->notNull(),
            'declaration_id' => 'uuid not null',
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);

        $this->addPrimaryKey('pkClient', 'clients', 'id');

        $this->addForeignKey('fkClientFilesPhoto',
            'clients', 'photo_id',
            'files', 'id',
            'cascade', 'cascade'
        );

        $this->addForeignKey('fkClientFilesPassport',
            'clients', 'passport_id',
            'files', 'id',
            'cascade', 'cascade'
        );

        $this->addForeignKey('fkClientFilesDeclaration',
            'clients', 'declaration_id',
            'files', 'id',
            'cascade', 'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkClientFilesPhoto', 'clients');
        $this->dropForeignKey('fkClientFilesPassport', 'clients');
        $this->dropForeignKey('fkClientFilesDeclaration', 'clients');
        $this->dropTable('clients');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220221_110512_clients cannot be reverted.\n";

        return false;
    }
    */
}
