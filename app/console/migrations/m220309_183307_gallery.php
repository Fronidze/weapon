<?php

use yii\db\Migration;

/**
 * Class m220309_183307_gallery
 */
class m220309_183307_gallery extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('galleries', [
            'id' => 'uuid not null',
            'title' => $this->string()->notNull(),
            'available_quantity' => $this->integer()->defaultValue(null),
            'duration' => $this->integer()->defaultValue(45),
            'open_at' => $this->integer()->defaultValue(null),
            'close_at' => $this->integer()->defaultValue(null),
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);

        $this->addPrimaryKey('pkGalleries', 'galleries', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('galleries');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220309_183307_gallery cannot be reverted.\n";

        return false;
    }
    */
}
