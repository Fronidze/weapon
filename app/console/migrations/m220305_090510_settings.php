<?php

use yii\db\Migration;

/**
 * Class m220305_090510_settings
 */
class m220305_090510_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('settings', [
            'id' => 'uuid not null',
            'title' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
            'phone' => $this->string()->notNull(),
            'material_responsible_id' => 'uuid not null',
            'accountant_id' => 'uuid not null',
            'director_id' => 'uuid not null',
            'created_at' => 'timestamp with time zone default now()',
            'updated_at' => 'timestamp with time zone default now()',
        ]);

        $this->addPrimaryKey('pkSettings', 'settings', 'id');

        $this->addForeignKey('fkSettingsMaterialResponsible',
        'settings', 'material_responsible_id',
        'employee', 'id',
        'set null', 'cascade'
        );

        $this->addForeignKey('fkSettingsAccountant',
            'settings', 'accountant_id',
            'employee', 'id',
            'set null', 'cascade'
        );

        $this->addForeignKey('fkSettingsDirector',
            'settings', 'director_id',
            'employee', 'id',
            'set null', 'cascade'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fkSettingsDirector', 'settings');
        $this->dropForeignKey('fkSettingsMaterialResponsible', 'settings');
        $this->dropForeignKey('fkSettingsAccountant', 'settings');
        $this->dropTable('settings');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220305_090510_settings cannot be reverted.\n";

        return false;
    }
    */
}
