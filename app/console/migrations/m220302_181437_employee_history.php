<?php

use yii\db\Migration;

/**
 * Class m220302_181437_user_hisroty
 */
class m220302_181437_employee_history extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('employee_history', [
            'id' => 'uuid not null',
            'action' => $this->string()->notNull(),
            'source' => 'uuid default null',
            'target' => 'uuid default null',
            'description' => $this->text()->defaultValue(null),
            'happened_at' => 'timestamp with time zone default now()'
        ]);
        $this->addPrimaryKey('pkEmployeeHistory', 'employee_history', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('employee_history');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m220302_181437_user_hisroty cannot be reverted.\n";

        return false;
    }
    */
}
