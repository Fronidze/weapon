<?php

namespace console\controllers;

use common\models\Calibers;
use common\modules\directory\models\DirectoryWeaponSubtypes;
use common\modules\directory\models\DirectoryWeaponTypes;
use common\modules\directory\models\DirectoryWeaponViews;
use common\weapon\entity\rules\DeveloperRule;
use Ramsey\Uuid\Uuid;
use yii\console\Controller;
use yii\helpers\Console;
use yii\rbac\DbManager;
use yii\rbac\Permission;
use yii\rbac\Role;
use common\weapon\entity\enum\Role as RoleEnum;
use common\weapon\entity\enum\Permission as PermissionEnum;
use yii\rbac\Rule;

class SeedController extends Controller
{
    protected function message(string $text, bool $isError = false)
    {
        $messageColor = $isError ? Console::FG_RED : Console::FG_GREEN;
        $this->stdout($text . PHP_EOL, $messageColor);
    }

    protected function truncate(string $table): void
    {
        \Yii::$app->db->createCommand("truncate {$table} cascade")->execute();
    }

    public function actionDirectory()
    {
        $views = [
            'гражданское',
            'служебное'
        ];
        $this->truncate(DirectoryWeaponViews::tableName());
        foreach ($views as $view) {
            \Yii::$app->db->createCommand()
                ->insert(DirectoryWeaponViews::tableName(), ['id' => Uuid::uuid4()->toString(), 'title' => $view])
                ->execute();
        }

        $types = [
            'оружие самообороны',
            'спортивное оружие',
            'охотничье оружие',
            'оружие, используемое в культурных и образовательных целях',
        ];
        $this->truncate(DirectoryWeaponTypes::tableName());
        foreach ($types as $type) {
            \Yii::$app->db->createCommand()
                ->insert(DirectoryWeaponTypes::tableName(), ['id' => Uuid::uuid4()->toString(), 'title' => $type])
                ->execute();
        }

        $subtypes = [
            'огнестрельное гладкоствольное длинноствольное',
            'огнестрельное ограниченного поражения',
            'огнестрельное с нарезным стволом',
            'огнестрельное гладкоствольное',
            'огнестрельное длинноствольное с нарезным стволом',
            'огнестрельное гладкоствольное длинноствольное',
            'огнестрельное комбинированное длинноствольное',
            'списанное',
            'огнестрельное гладкоствольное оружие',
            'огнестрельное нарезное короткоствольное оружие',
            'огнестрельное гладкоствольное длинноствольное оружие',
            'огнестрельное оружие ограниченного поражения',
        ];
        $this->truncate(DirectoryWeaponSubtypes::tableName());
        foreach ($subtypes as $subtype) {
            \Yii::$app->db->createCommand()
                ->insert(DirectoryWeaponSubtypes::tableName(), ['id' => Uuid::uuid4()->toString(), 'title' => $subtype])
                ->execute();
        }
    }

    public function actionCalibers()
    {
        $this->truncate(Calibers::tableName());
        $calibers = [
            ['title' => '7,62х51', 'alternative_title' => '.308 Win'],
            ['title' => '5,6х15,6', 'alternative_title' => '.22 LR'],
            ['title' => '8,6х70', 'alternative_title' => '.338 LM'],
            ['title' => '9х19', 'alternative_title' => null],
            ['title' => '5,56х45', 'alternative_title' => '.223 Rem'],
            ['title' => '10х22', 'alternative_title' => '.40 S&W'],
            ['title' => '9х18', 'alternative_title' => null],
            ['title' => '12,7х32,6', 'alternative_title' => '.50 AE'],
            ['title' => '11,43х23', 'alternative_title' => '.45 ACP'],
            ['title' => '9х17', 'alternative_title' => '.380 ACP'],
        ];
        foreach ($calibers as $caliber) {
            \Yii::$app->db->createCommand()
                ->insert(Calibers::tableName(), [
                    'id' => Uuid::uuid4()->toString(),
                    'title' => $caliber['title'],
                    'alternative_title' => $caliber['alternative_title'],
                ])
                ->execute();
        }
    }

    public function actionRbacInit()
    {
        $manager = \Yii::$app->authManager;
        $manager->removeAll();

        $this->makePermission(PermissionEnum::Client);
        $this->makePermission(PermissionEnum::Reception);
        $this->makePermission(PermissionEnum::Directory);
        $this->makePermission(PermissionEnum::Document);
        $this->makePermission(PermissionEnum::Storage);
        $this->makePermission(PermissionEnum::Weapon);

        $this->makeRole(RoleEnum::Client, [PermissionEnum::Client]);
        $this->makeRole(RoleEnum::Employee);
        $this->makeRole(RoleEnum::Admin, [
            PermissionEnum::Client,
            PermissionEnum::Reception,
            PermissionEnum::Directory,
            PermissionEnum::Document,
            PermissionEnum::Storage,
            PermissionEnum::Weapon
        ]);

    }

    private function makeRole(RoleEnum $role, array $childRoles = [], ?string $description = null)
    {
        /** @var DbManager $authManager */
        $authManager = \Yii::$app->authManager;

        $roleName = $role->getRoleName();
        $role = new Role();
        $role->name = $roleName;
        $role->description = $description;

        $authManager->add($role);
        $this->message("Роль [{$roleName}] добавлена");

        foreach ($childRoles as $childRole) {
            if ($childRole instanceof RoleEnum) {
                $childRoleName = $childRole->getRoleName();
                $childRoleItem = $authManager->getRole($childRoleName);
                if ($childRoleItem === null) {
                    $this->message("Роль [{$childRoleName}] не найдена", true);
                    continue;
                }

                $authManager->addChild($role, $childRoleItem);
                $this->message(" - Роль [{$role->name}] наследует роль [{$childRoleItem->name}]");
                continue;
            }

            if ($childRole instanceof PermissionEnum) {
                $childPermissionName = $childRole->getRoleName();
                $childPermissionItem = $authManager->getPermission($childPermissionName);
                if ($childPermissionItem === null) {
                    $this->message("Разрешение [{$childPermissionName}] не найдено", true);
                    continue;
                }

                $authManager->addChild($role, $childPermissionItem);
                $this->message(" - Роль [{$role->name}] наследует разрешение [{$childPermissionItem->name}]");
                continue;
            }

            $this->message("Переданно не коректная роль в массив дочерних ролей", true);
        }
    }

    private function makePermission(PermissionEnum $permission, array $childItems = [], ?Rule $rule = null)
    {
        $manager = \Yii::$app->authManager;

        $permissionItem = new Permission();
        $permissionItem->name = $permission->getRoleName();
        if ($rule !== null) {
            $permissionItem->ruleName = $rule->name;
        }


        $manager->add($permissionItem);
        $this->message("Разрешение [{$permission->getRoleName()}] добавлено");
        if ($rule !== null) {
            $this->message(" - Правило [{$rule->name}] добавлена в разрешение [{$permission->getRoleName()}]");
        }

        foreach ($childItems as $childItem) {

            if ($childItem instanceof PermissionEnum) {

                $childPermissionItem = $manager->getPermission($childItem->getRoleName());
                if ($childPermissionItem === null) {
                    $this->message("Разрешение [{$childItem->getRoleName()}] не найдено", true);
                    continue;
                }

                $manager->addChild($permissionItem, $childPermissionItem);
                $this->message(" - Разрешение [{$permissionItem->name}] наследует разрешение [{$childPermissionItem->name}]");
                continue;
            }

            $this->message("Переданно не коректная роль в массив дочерних ролей", true);
        }
    }

}