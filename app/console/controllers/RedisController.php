<?php

namespace console\controllers;

use common\models\Accounts;
use common\weapon\entity\socket\Message;
use yii\console\Controller;
use yii\redis\Connection;

class RedisController extends Controller
{
    private ?Connection $redis = null;
    private string $channelName = 'websocket';

    public function init()
    {
        $this->redis = \Yii::$app->get('redis');
        parent::init();
    }

    public function actionCommand(string $command)
    {
        $message = new Message($command, 'command');
        $this->redis->publish(
            $this->channelName,
            $message->toJson()
        );
    }

    public function actionMessage(string $message, ?string $email = null)
    {
        $uuid = null;
        if ($email !== null) {
            $account = Accounts::find()
                ->where(['=', 'email', $email])
                ->one();

            if ($account instanceof Accounts) {
                $uuid = $account->id;
            }
        }

        $message = new Message($message, 'message', recipient: $uuid);
        $this->redis->publish($this->channelName, $message->toJson());
    }

}