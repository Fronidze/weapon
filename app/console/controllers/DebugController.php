<?php

namespace console\controllers;

use common\jobs\account\RegisterJob;
use common\modules\directory\models\Galleries;
use common\modules\directory\service\GalleryScheduleService;
use common\modules\directory\service\GalleryService;
use common\modules\document\entities\enums\FontWeight;
use common\modules\document\entities\enums\TextAlignment;
use common\modules\document\factory\OrganizationShootingFactory;
use common\modules\document\reports\ShootReport;
use common\weapon\repository\AccountRepository;
use common\weapon\repository\GalleryRepository;
use common\weapon\repository\orders\OrganizationShootingRepository;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use yii\console\Controller;
use yii\console\ExitCode;

class DebugController extends Controller
{

    public function actionRun()
    {
        /** @var Galleries[] $galleries */
        $galleries = (new GalleryRepository())->list();
        $gallery = current($galleries);

        $serviceSchedule = new GalleryScheduleService();
        $serviceSchedule->createScheduleByGallery($gallery);

    }

    public function actionExcel(): int
    {
        $report = new ShootReport();
        $report->getNewCell('A1')->merge('L1')
            ->value('ЖУРНАЛ')
            ->font(size: 12, weight: FontWeight::Bold)
            ->height(20)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('A2')->merge('L2')
            ->value('учета стрельб')
            ->font(size: 12, weight: FontWeight::Bold)
            ->height(20)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('A3')->merge('L3')
            ->value('ООО «ТРИ КОТА»')
            ->font(size: 12, weight: FontWeight::Bold)
            ->height(20)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('A4')->merge('L4')
            ->value('Ленская область, г. Анин, ул. Ленина, д. 50')
            ->font(size: 12, weight: FontWeight::Bold)
            ->height(20)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('A5')->merge('L5')
            ->value('(наименование и адрес тира, стрелково-стендового комплекса, стрельбища, наименование организации)')
            ->font(size: 9)
            ->height(20)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('A6')->merge('B6')
            ->value('Начало: ')
            ->font(size: 11, weight: FontWeight::Bold);

        $report->getNewCell('A7')->merge('B7')
            ->value('Окончание: ')
            ->font(size: 11, weight: FontWeight::Bold);

        $report->getNewCell('A10')->merge('A11')
            ->value('№ п/п')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('B10')->merge('B11')
            ->value('Дата, время и место проведения стрельбы')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('C10')->merge('C11')
            ->value('Наименование организации, с работниками которой проводятся стрельбы, номер и дата договора о предоставлении тира, ССК, стрельбища для стрельбы')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('D10')->merge('D11')
            ->value('Фамилия, имя, отчество стреляющего')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);


        $report->getNewCell('E10')->merge('E11')
            ->value('Основания и цель проведения стрельбы (номер и дата приказа о назначении администрации стрельбы)')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('F10')->merge('G10')
            ->value('Администрация стрельбы (фамилия, имя, отчество)')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('F11')->value('руководитель стрельбы')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('G11')->value('раздатчик патронов')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('H10')->merge('K10')
            ->value('Сведения о стрельбе')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('H11')->value('кол-во стреляющих')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);
        $report->getNewCell('I11')->value('кол-во оружия и патронов (перед началом проведения стрельбы)')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);
        $report->getNewCell('J11')->value('кол-во оружия и патронов (по окончании стрельбы)')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);
        $report->getNewCell('K11')->value('расписка руководителя стрельбы')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $report->getNewCell('L10')->merge('L11')
            ->value('Примечание')
            ->font(size: 12)
            ->alignment(horizontal: TextAlignment::Center, vertical: TextAlignment::Center);

        $columns = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'];
        foreach ($columns as $index => $columnLabel) {
            $report->getNewCell("{$columnLabel}12")->value($index + 1)
                ->font(size: 12, weight: FontWeight::Bold)
                ->alignment(TextAlignment::Center, TextAlignment::Center);
        }

        $clients = [];
        $row = [
            1,
            ['25-09-2020', '10:00', 'Галерея №1'],
            'Клиент клуба',
            ['Маркелов', 'Алексей', 'Юрьевич'],
            ['Приказ №1', '25-09-2020'],
            ['Иванов', 'Олег', 'Иванович'],
            ['Иванов', 'Олег', 'Иванович'],
            1,
            [2, 300],
            [2, 20],
            null,
            null
        ];

        for ($i = 0; $i < 10; $i++) {
            array_push($clients, $row);
        }

        $lastIndex = 12;
        foreach ($clients as $information) {
            $recordRow = $lastIndex;
            $maxRecordRow = $recordRow;
            foreach ($information as $columnIndex => $value) {
                $index = Coordinate::stringFromColumnIndex($columnIndex + 1);

                if (is_array($value) === true) {
                    $valueRow = $recordRow;
                    foreach ($value as $arrayValue) {
                        $valueRow += 1;
                        $report->getNewCell("{$index}{$valueRow}")
                            ->value($arrayValue);

                    }

                    if ($maxRecordRow < $valueRow) {
                        $maxRecordRow = $valueRow;
                    }

                    continue;
                }

                $rowIndex = $recordRow + 1;
                $report->getNewCell("{$index}{$rowIndex}")
                    ->value($value ?? '');

            }
            $lastIndex = $maxRecordRow + 1;
        }
//        $filename = \Yii::getAlias('@storage/test.pdf');
//        $writer = new Mpdf($report->getSpreadsheet());

        $filename = \Yii::getAlias('@storage/test.xlsx');
        $writer = new Xlsx($report->getSpreadsheet());

        $writer->save($filename);
        return ExitCode::OK;
    }
}