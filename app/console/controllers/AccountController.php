<?php

namespace console\controllers;

use common\models\Accounts;
use common\weapon\repository\AccountRepository;
use common\weapon\service\profile\AccountService;
use yii\console\Controller;
use yii\console\ExitCode;
use yii\helpers\Console;
use yii\web\Response;

class AccountController extends Controller
{

    protected function message(string $text, bool $isError = false)
    {
        $messageColor = $isError ? Console::FG_RED : Console::FG_GREEN;
        $this->stdout($text . PHP_EOL, $messageColor);
    }

    public function actionRole(string $email, string $role)
    {
        try {
            (new AccountService())->changeRole($email, $role);
            $this->message("Роль успешно изменена");
            return ExitCode::OK;
        } catch (\Throwable $exception) {
            $this->message($exception->getMessage(), true);
            $this->message($exception->getTraceAsString(), true);
            return ExitCode::UNSPECIFIED_ERROR;
        }
    }

}