const WebSocket = require('ws')
const redis = require('redis')
const url = require('url')

const redisClient = redis.createClient({
    url: 'redis://redis:6379',
    password: "405968"
});


(async () => {
    await redisClient.connect();
    await redisClient.subscribe('websocket', (jsonMessage) => {
        let message = JSON.parse(jsonMessage);
        let messageObject = new SocketMessage(
            message.body,
            message.type,
            message.sender || null,
            message.recipient || null,
        )

        if (messageObject.recipient === null) {
            webSocket.clients.forEach(socket => {
                socket.send(messageObject.json());
            });
        }

        if (messageObject.recipient !== null) {
            webSocket.clients.forEach(socket => {
                let socketId = socket.id;
                if (socketId === messageObject.recipient) {
                    socket.send(messageObject.json());
                }
            });
        }
    });
})();

const webSocket = new WebSocket.Server({port: 8080})
webSocket.on('connection', (socket, request) => {
    let token = url.parse(request.url, true).query.token;
    if (token !== undefined) {
        socket.id = token;
        return;
    }

    socket.close();
});

class SocketMessage {
    constructor(
        message,
        type = 'message',
        sender = null,
        recipient = null,
    ) {
        this.message = message
        this.type = type
        this.sender = sender;
        this.recipient = recipient;

    }

    json() {
        return JSON.stringify({
            type: this.type,
            sender: this.sender,
            recipient: this.recipient,
            body: this.message
        });
    }
}